class Game_Unit
  def init_lines_matrix(matrix)
    @lines_matrix = matrix
    @line_set = nil
  end

  def line_set
    @line_set ||= init_line_set
  end

  def get_line_set
    @line_set
  end

  def smooth_target(index)
    member = members[index]
    (member && member.alive? && !member.transparent?) ? member : first_visible
  end

  def first_visible
    flatten_matrix = lines_matrix.flatten
    alive_members.reject(&:transparent?).sort do |b1, b2|
      flatten_matrix.index(b1.index) <=> flatten_matrix.index(b2.index)
    end.first
  end

  private

  def init_line_set
    LineSet.new(half).tap do |ls|
      lines_matrix.each.with_index do |arr, line_index|
        arr.each { |i| ls.lines[line_index].add members[i] if members[i] }
      end
    end
  end

  def lines_matrix
    @lines_matrix || [[0, 1], [2], [3]]
  end

  def half
    if set = $game_troop.get_line_set || $game_party.get_line_set
      set.half == 1 ? 2 : 1
    else
      (1..2).to_a.sample
    end
  end
end