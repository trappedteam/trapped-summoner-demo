class Game_Actor

  def rpg_object
    actor
  end

  def use_sprite?
    true
  end

  def sprite_name
    actor.sprite_name
  end

  def perform_damage_effect
    @sprite_effect_type = :blink
    Sound.play_actor_damage
  end

  def skill_atype_ok?(skill)
    atype_id1 = skill.required_atype_id1
    atype_id2 = skill.required_atype_id2
    return true if atype_id1 == 0 && atype_id2 == 0
    return true if atype_id1 > 0 && atype_equipped?(atype_id1)
    return true if atype_id2 > 0 && atype_equipped?(atype_id2)
    false
  end

  def atype_equipped?(atype_id)
    armors.any? {|armor| armor.atype_id == atype_id }
  end

  def check_damage_value(user, item)
    value = item.damage.eval(user, self, $game_variables)
    value *= item_element_rate(user, item)
    value *= pdr if item.physical?
    value *= mdr if item.magical?
    value *= rec if item.damage.recover?
    apply_guard(value)
  end
end
