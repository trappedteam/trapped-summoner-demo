class RPG::UsableItem::Effect
  attr_accessor :direct

  def direct?
    defined?(@direct) ? !!@direct : true
  end
end