module MeleePatch
  def melee?
    tags.include?(:melee) || !range?
  end

  def range?
    tags.include? :range
  end
end

module PiercingPatch
  def piercing?
    tags.include? :piercing
  end
end

module RequiredAtypes
  attr_writer :required_atype_id1, :required_atype_id2

  def required_atype_id1
    @required_atype_id1.to_i
  end

  def required_atype_id2
    @required_atype_id2.to_i
  end
end

module ItemStates
  def states
    effects.select { |effect| effect.code == 21 }.map do  |effect|
      $data_states[effect.data_id]
    end
  end
end

module AnimationsPatch
  def animations
    @animations ||= {}
  end
end

[RPG::Skill, RPG::Item].each do |klass|
  [MeleePatch, RequiredAtypes, PiercingPatch, ItemStates, AnimationsPatch].each do |patch|
    klass.__send__ :include, patch
  end
end

RPG::Class.__send__ :include, MeleePatch