class Game_Troop
  def current_strategy
    @current_strategy ||= Strategy.best
  end

  def make_actions
    current_strategy.set_roles
    super
  end

  def lines_matrix
    current_strategy.lines_matrix
  end

  def clear_strategy
    @current_strategy = nil
    @line_set = nil
  end

  def sort_by_priority!
    new_enemies = @enemies.sort do |l, r|
      if l.lines_priority[0] != r.lines_priority[0]
        l.lines_priority[0] <=> r.lines_priority[0]
      elsif l.lines_priority[1] != r.lines_priority[1]
        l.lines_priority[1] <=> r.lines_priority[1]
      else
        l.lines_priority[2] <=> r.lines_priority[2]
      end
    end

    unless new_enemies == @enemies
      @enemies = new_enemies
      @enemies.each.with_index { |e, index| e.index = index }
    end
  end
end