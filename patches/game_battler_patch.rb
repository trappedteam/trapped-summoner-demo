class Game_Battler
  EFFECT_TO_METHOD = {
    EFFECT_RECOVER_HP    => :item_effect_recover_hp,
    EFFECT_RECOVER_MP    => :item_effect_recover_mp,
    EFFECT_GAIN_TP       => :item_effect_gain_tp,
    EFFECT_ADD_STATE     => :item_effect_add_state,
    EFFECT_REMOVE_STATE  => :item_effect_remove_state,
    EFFECT_ADD_BUFF      => :item_effect_add_buff,
    EFFECT_ADD_DEBUFF    => :item_effect_add_debuff,
    EFFECT_REMOVE_BUFF   => :item_effect_remove_buff,
    EFFECT_REMOVE_DEBUFF => :item_effect_remove_debuff,
    EFFECT_SPECIAL       => :item_effect_special,
    EFFECT_GROW          => :item_effect_grow,
    EFFECT_LEARN_SKILL   => :item_effect_learn_skill,
    EFFECT_COMMON_EVENT  => :item_effect_common_event,
  }

  attr_writer :pose

  def effective_hp(kind, element_id = nil)
    kind.to_s =~ /magic/i ? hp * mdf : hp * self.def
  end

  def damage_messages
    @damage_messages ||= []
  end

  def can_hit?(target, args)
    TargetManager.new(self, target, args[:with]).can_hit?
  end

  def distance_to(target)
    Line::Distance.new(self, target).distance
  end

  def line_set
    friends_unit.line_set
  end

  def line
    line_set.line_with self
  end

  def transparent?
    states.include?(RPG::State.transparent) || dead?
  end

  def observable?
    states.include? RPG::State.observable
  end

  def visible?
    !transparent?
  end

  alias original_on_turn_end on_turn_end
  def on_turn_end
    original_on_turn_end
    set_nonaction_pose
  end

  alias original_on_action_end on_action_end
  def on_action_end
    original_on_action_end
    set_nonaction_pose
  end

  alias on_battle_start_original on_battle_start
  def on_battle_start
    Ticker.delay 30 do
      switch_pose :battle_start
    end
    on_battle_start_original
  end

  def item_effect_apply(user, item, effect)
    actor, subject = effect.direct? ? [self, user] : [user, self]

    if method_name = effect_to_method(effect.code)
      actor.__send__ method_name, subject, item, effect
    end
  end

  def effect_to_method(code)
    EFFECT_TO_METHOD.merge(@additional_effect_methods || {})[code]
  end

  def battlers_behind
    if friends_unit.line_set
      friends_unit.line_set.battlers_behind self
    else
      []
    end
  end

  def item_effect_add_state_normal(user, item, effect)
    chance = effect.value1
    chance *= state_rate(effect.data_id) if opposite?(user)
    chance *= luk_effect_rate(user)      if opposite?(user)
    if rand < chance
      add_state effect.data_id, user
      @result.success = true
    end
  end

  def add_state(state_id, caster = nil)
    if state_addable?(state_id)
      add_new_state(state_id, caster) unless state?(state_id)
      reset_state_counts(state_id)
      @result.added_states.push(state_id).uniq!
    end
  end

  def add_new_state(state_id, caster = nil)
    die if state_id == death_state_id
    @states.push(state_id)
    @states_casters[state_id] = caster
    on_restrict if restriction > 0
    sort_states
    refresh
  end

  alias original_refresh_for_poses refresh
  def refresh
    original_refresh_for_poses
    set_nonaction_pose
  end

  def make_damage_value(user, item)
    value = item.damage.eval(user, self, $game_variables)
    value *= item_element_rate(user, item)
    value *= pdr if item.physical?
    value *= mdr if item.magical?
    value *= rec if item.damage.recover?
    value = apply_critical(value) if @result.critical
    value = apply_variance(value, item.damage.variance)
    value = apply_guard(value)
    value, @result.transfered_damage, @result.transfered_damage_target = apply_watch(value)
    @result.make_damage(value.to_i, item)
  end

  def apply_watch(damage)
    if damage > 0 && states.include?(RPG::State.watched) && state_caster(RPG::State.watched).alive?
      [damage * 0.7, (damage * 0.3).round, state_caster(RPG::State.watched)]
    else
      [damage, nil, nil]
    end
  end

  def apply_guard(damage)
    damage / (damage > 0 && guard? ? 1.2 * grd : 1)
  end

  alias execute_damage_original execute_damage
  def execute_damage(user)
    execute_damage_original user
    if @result.hp_damage > 0
      animate_damage
    else
      set_nonaction_pose
    end
    if @result.transfered_damage && @result.transfered_damage_target
      @result.transfered_damage_target.hp -= @result.transfered_damage
      @result.transfered_damage_target.set_nonaction_pose if @result.transfered_damage_target.dead?
    end
  end

  def pose
    @pose || :iddle
  end

  def low_hp?
    hp < mhp * 0.3
  end

  def animate_damage
    switch_pose(:taking_damage) unless dead?
  end

  def recover_all
    super
    set_nonaction_pose
  end

  def set_nonaction_pose
    @pose = if dead?
      :dead
    elsif guard?
      :guard
    elsif low_hp?
      :dying
    else
      :iddle
    end
  end

  def switch_pose(pose)
    @pose = pose
    Ticker.delay 30 do
      set_nonaction_pose
    end
  end

  alias original_die_for_patch die 
  def die
    original_die_for_patch
    set_nonaction_pose
  end
end
