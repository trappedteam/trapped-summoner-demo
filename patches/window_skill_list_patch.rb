class Window_SkillList
  alias original_include? include?
  def include?(item)
    original_include?(item) && item.id != @actor.attack_skill_id
  end
end
