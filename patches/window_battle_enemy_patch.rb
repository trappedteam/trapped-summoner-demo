class Window_BattleEnemy
  attr_accessor :current_item

  def enemies
    $game_troop.alive_members
  end

  def enemy
    @index && enemies[@index]
  end

  def item_max
    enemies.size
  end

  def current_item_enabled?
    enemy && enable?(enemy)
  end

  def enable?(enemy)
    if current_enemy = BattleManager.actor
      current_enemy.can_hit? enemy, with: current_item
    else
      false
    end
  end

  def draw_item(index)
    enemy = enemies[index]
    change_color normal_color, enable?(enemy)
    draw_text item_rect_for_text(index), enemy.name
  end

  def select(index)
    cursor.mark enemies[index]
    cursor.show
    super
  end

  alias original_hide hide
  def hide
    cursor.hide
    original_hide
  end

  def cursor
    Spriteset_Battle.cursors[:red]
  end
end