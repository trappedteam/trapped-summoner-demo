class Window_BattleStatus
  def draw_basic_area(rect, actor, enabled = true)
    draw_actor_name(actor, rect.x + 0, rect.y, 100, enabled)
    draw_actor_icons(actor, rect.x + 104, rect.y, rect.width - 104)
  end
end