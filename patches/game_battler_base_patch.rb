class Game_BattlerBase
  def skill_atype_ok?(skill)
    true
  end

  alias skill_conditions_met_original skill_conditions_met?
  def skill_conditions_met?(skill)
    skill_conditions_met_original(skill) && skill_atype_ok?(skill)
  end

  alias clear_states_original clear_states
  def clear_states
    clear_states_original
    @states_casters = {}
  end

  alias erase_state_original erase_state
  def erase_state(state_id)
    erase_state_original state_id
    @states_casters.delete state_id
  end

  def state_caster(state_id)
    state_id = state_id.is_a?(Integer) ? state_id : state_id.id
    @states_casters[state_id]
  end

  def attack_skill_id
    skill = skills.find { |skill| skill.tag? :attack_skill }
    (skill && skill.id) || 1
  end
end