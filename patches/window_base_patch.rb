class Window_Base
  def draw_actor_name(actor, x, y, width = 112, enabled = true)
    change_color(hp_color(actor), enabled)
    draw_text(x, y, width, line_height, actor.name)
  end

  def update_tone
  end
end