class Game_Enemy
  attr_accessor :current_role
  attr_accessor :index

  %w(skills sprite_name lines_priority).each do |method_name|
    define_method method_name do |*args|
      enemy.public_send(method_name, *args)
    end
  end

  (Strategy::Role.roles.keys + %w(range melee magical martial)).each do |key|
    define_method "#{key}?" do
      tag? key
    end
  end

  def rpg_object
    enemy
  end

  def melee_attack?
    $data_skills[attack_skill_id].melee?
  end

  def make_actions
    super
    return if @actions.empty?
    @actions.each do |action|
      action.set_strategy_action($game_troop.current_strategy.action_for(self))
    end
  end
end
