class Scene_Battle

  alias original_terminate_for_patch terminate
  def terminate
    $game_troop.clear_strategy
    original_terminate_for_patch
  end

  alias original_battle_start_for_patch battle_start 
  def battle_start
    $game_troop.clear_strategy
    original_battle_start_for_patch
  end

  def command_attack
    BattleManager.actor.input.set_attack
    @enemy_window.current_item = $data_skills[BattleManager.actor.attack_skill_id]
    select_enemy_selection
  end

  alias original_on_skill_ok on_skill_ok
  def on_skill_ok
    @actor_window.current_item = @enemy_window.current_item = @skill_window.item
    @skill_window.hide
    original_on_skill_ok
  end

  alias original_on_item_ok on_item_ok
  def on_item_ok
    @actor_window.current_item = @enemy_window.current_item = @item_window.item
    original_on_item_ok
  end

  alias original_invoke_counter_attack invoke_counter_attack
  def invoke_counter_attack(target, item)
    target.switch_pose :counter_attack
    original_invoke_counter_attack target, item
  end

  alias original_invoke_magic_reflection invoke_magic_reflection
  def invoke_magic_reflection(target, item)
    target.switch_pose :magic_reflection
    original_invoke_magic_reflection target, item
  end

  def with_item_pose(item)
    if item.is_a?(RPG::Skill) && item.id == 2
      yield
    else
      @subject.pose = item.pose
      yield
      @subject.set_nonaction_pose
    end
  end

  def use_item
    item = @subject.current_action.item
    @log_window.display_use_item(@subject, item)
    @subject.use_item(item)
    refresh_status
    targets = @subject.current_action.make_targets.compact

    if item.animations.any?
      with_item_pose item do
        show_animation [@subject], item.animations[:caster] if item.animations[:caster]
      end
      show_animation targets, item.animations[:target] if item.animations[:target]
    else
      with_item_pose(item) { show_animation(targets, item.animation_id) }
    end
    targets.each {|target| item.repeats.times { invoke_item(target, item) } }
  end

  def show_animation(targets, animation_id)
    if animation_id < 0
      show_attack_animation(targets)
    else
      show_normal_animation(targets, animation_id)
    end
    wait_for_animation
  end

  def show_normal_animation(targets, animation_id, mirror = false)
    animation = $data_animations[animation_id]
    if animation
      targets.each do |target|
        target.animation_id = animation_id
        target.animation_mirror = mirror
      end
    end
    abs_wait_short
  end

  def invoke_item(target, item)
    if rand < target.item_cnt(@subject, item)
      invoke_counter_attack(target, item)
    elsif rand < target.item_mrf(@subject, item)
      invoke_magic_reflection(target, item)
    elsif item.for_opponent? && target.states.include?(RPG::State.shield_block) && rand(10) == 0
      target.switch_pose :block
      @log_window.display_shield_block target
    else
      apply_item_effects(apply_substitute(target, item), item)
    end
    @subject.last_target_index = target.index
  end

  def show_attack_animation(targets)
    Sound.play_enemy_attack
    abs_wait_short
  end

  alias original_process_action_end process_action_end
  def process_action_end
    original_process_action_end
    wait 60
  end
end
