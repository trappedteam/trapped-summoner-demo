class Object
  def try(*args, &block)
    public_send(*args, &block) if respond_to?(args[0])
  end
end
