class Game_Action
  def set_strategy_action(hash)
    set_skill hash[:skill].id
    self.target_index = hash[:target_index] if hash[:target_index]
  end

  def decide_random_target
    if item.for_dead_friend?
      target = until_reachable { friends_unit.random_dead_target }
    elsif item.for_friend?
      target = until_reachable { friends_unit.random_target }
    else
      target = until_reachable { opponents_unit.random_target }
    end
    if target
      @target_index = target.index
    else
      clear
    end
  end

  alias confusion_target_original confusion_target
  def confusion_target
    until_reachable { confusion_target_original }
  end

  def targets_for_opponents
    if item.for_random?
      Array.new(item.number_of_targets) { until_reachable { opponents_unit.random_target } }
    elsif item.for_one?
      num = 1 + (attack? ? subject.atk_times_add.to_i : 0)
      add_if_piercing do
        if @target_index < 0
          [until_reachable { opponents_unit.random_target }] * num
        else
          [if_reachable { opponents_unit.smooth_target(@target_index) }] * num
        end
      end
    else
      opponents_unit.alive_members
    end
  end

  def targets_for_friends
    if item.for_user?
      [subject]
    elsif item.for_dead_friend?
      if item.for_one?
        [if_reachable { friends_unit.smooth_dead_target(@target_index) }]
      else
        friends_unit.dead_members
      end
    elsif item.for_friend?
      if item.for_one?
        add_if_piercing { [if_reachable { friends_unit.smooth_target(@target_index) }] }
      else
        friends_unit.alive_members
      end
    end
  end

  alias item_target_candidates_original item_target_candidates
  def item_target_candidates
    item_target_candidates_original.select { |battler| subject.can_hit? target, with: item }
  end

  alias make_targets_original make_targets

  def make_targets
    make_targets_original.compact
  end

  private

  def add_if_piercing
    targets = yield
    if item.piercing?
      targets.compact.map { |target| [target] + target.battlers_behind }.flatten
    else
      targets
    end
  end

  def until_reachable
    count = 0
    while (target = yield) && (count < 100)
      count += 1
      break target if subject.can_hit? target, with: item
    end
  end

  def if_reachable
    target = yield
    target if subject.can_hit? target, with: item
  end
end
