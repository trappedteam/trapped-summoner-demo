class Window_BattleActor
  attr_accessor :current_item

  def friends
    $game_party.battle_members
  end

  def friend
    @index && friends[@index]
  end

  def item_max
    friends.size
  end

  def current_item_enabled?
    friend && enable?(friend)
  end

  def enable?(friend)
    if current_actor = BattleManager.actor
      current_actor.can_hit? friend, with: current_item
    else
      false
    end
  end

  def select(index)
    cursor.mark $game_party.members[index]
    cursor.show
    super
  end

  alias original_hide hide
  def hide
    cursor.hide
    original_hide
  end

  def draw_item(index)
    friend = friends[index]
    draw_basic_area basic_area_rect(index), friend, enable?(friend)
    draw_gauge_area gauge_area_rect(index), friend
  end

  def cursor
    Spriteset_Battle.cursors[:blue]
  end
end