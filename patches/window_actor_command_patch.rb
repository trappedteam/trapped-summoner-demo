class Window_ActorCommand
  alias original_setup setup
  def setup(actor)
  	cursor.mark actor
  	cursor.show
  	original_setup actor
  end

  alias original_close close
  def close
  	original_close
  	cursor.hide
  end

  private

  def cursor
  	Spriteset_Battle.cursors[:green]
  end
end