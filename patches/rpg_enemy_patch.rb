class RPG::Enemy
  attr_writer :skills
  attr_accessor :sprite_name

  def skills
    @skills ||= []
  end

  def lines_priority=(*lines)
  	@line_priority = lines.flatten
  end

  def lines_priority
  	@line_priority || [1, 2, 3]
  end
end
