module BattleManager
  instance_eval do
    alias original_process_victory process_victory
    def process_victory
      victory_pose $game_party.battle_members
      original_process_victory
    end

    alias original_process_defeat process_defeat
    def process_defeat
      victory_pose $game_troop.members
      original_process_defeat
    end

    private

    def victory_pose(battlers)
      battlers.each { |battler| battler.pose = :victory unless battler.dead? }
    end
  end
end
