module Taggable
  def self.included(klass)
    klass.class_eval { attr_writer :tags }
  end

  def tags
    @tags ||= []
  end

  def tag?(*syms)
    (to_tags(syms) - tags).empty?
  end
  alias tags? tag?

  def any_tag?(*syms)
    (tags & to_tags(syms)).any?
  end

  def to_tags(strings)
    strings.map(&:to_sym)
  end
end

[
  RPG::Enemy, RPG::Skill,
  RPG::Item, RPG::Class,
  RPG::Actor
].each { |klass| klass.__send__ :include, Taggable }

[Game_Actor, Game_Enemy].each do |klass|
  klass.class_eval do
    %w(tags tag? any_tag?).each do |method_name|
      define_method method_name do |*args|
        rpg_object.public_send(method_name, *args)
      end
    end
  end
end