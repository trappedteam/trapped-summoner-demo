class RPG::State
  class << self
    { transparent: 26, watched: 28, shield_block: 27, observable: 29 }.each do |state_name, data_id|
      define_method(state_name) { $data_states[data_id] }
    end
  end
end