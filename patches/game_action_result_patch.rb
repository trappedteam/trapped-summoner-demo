class Game_ActionResult
  attr_accessor :transfered_damage, :transfered_damage_target

  alias clear_damage_values_original clear_damage_values
  def clear_damage_values
    clear_damage_values_original
    @transfered_damage = 0
    @transfered_damage_target = nil
  end
end