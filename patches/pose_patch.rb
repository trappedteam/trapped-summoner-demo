class RPG::Skill
  attr_writer :pose

  def pose
  	@pose || default_pose
  end

  private

  def default_pose
  	if stype_id == 2
  	  :casting
  	elsif id == 1
  	  :attacking
    elsif id == 2
      :guard
  	else
  	  :skill
  	end
  end
end

class RPG::Item
  attr_writer :pose

  def pose
  	@pose || :casting
  end
end
