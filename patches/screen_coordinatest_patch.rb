[Game_Actor, Game_Enemy].each do |klass|
  klass.class_eval do
    def screen_y
      line_set.slot_with(self).y
    end

    def screen_x
      line_set.slot_with(self).x
    end

    def screen_z
      friends_unit.members.size - index.to_i   
    end
  end
end