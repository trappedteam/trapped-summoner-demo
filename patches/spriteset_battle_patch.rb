class Spriteset_Battle
  def self.cursors
    @cursors ||= {}
  end

  def cursors
    self.class.cursors
  end

  alias original_initialize initialize
  def initialize(*args)
    @cursors = {}
    create_cursor :red
    create_cursor :green
    create_cursor :blue
    original_initialize(*args)
  end

  def create_enemies
    @enemy_sprites = create_animated_sprites $game_troop
  end

  def create_actors
    @actor_sprites = create_animated_sprites $game_party
  end

  def update_actors
    if $game_party.members.size != @actor_sprites.size
      dispose_actors
      dispose_hp_bars
      create_hp_bars
      create_actors
    end
    @actor_sprites.each { |sprite| sprite.update }
  end

  alias original_dispose dispose
  def dispose
    dispose_cursor :red
    dispose_cursor :green
    dispose_cursor :blue
    original_dispose
  end

  alias original_update update
  def update
    update_cursor :red
    update_cursor :green
    update_cursor :blue
    original_update
  end

  private

  def create_cursor(color)
    cursors[color] = Cursor.new @viewport2, color
  end

  def update_cursor(color)
    cursors[color].update
  end

  def dispose_cursor(color)
    cursors[color].dispose
  end

  def battlers
    $game_troop.members + $game_party.members
  end

  def create_animated_sprites(game_unit)
    game_unit.members.reverse.map { |actor| Sprite_AnimatedBattler.new @viewport2, actor }
  end
end
