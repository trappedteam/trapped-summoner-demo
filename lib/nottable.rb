module Nottable
  def self.note!
  	$data_actors[1..-1].each(&:note!)
  	save_data $data_actors, 'Data/Actors.rvdata2'
  end

  def note!
    if /<ruby>(.*)<\/ruby>/m =~ note
      instance_eval $1
    end
  end
end
