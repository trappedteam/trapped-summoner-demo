class Spriteset_Battle
  alias initialize_for_states_bar initialize
  def initialize
    create_states_bars
    initialize_for_states_bar
  end

  alias dispose_for_states_bar dispose
  def dispose
    dispose_states_bars
    dispose_for_states_bar
  end

  alias update_for_states_bar update
  def update
    update_states_bars
    update_for_states_bar
  end

  private

  def update_states_bars
    if $game_party.members.size != @actor_sprites.size
      dispose_states_bars
      create_states_bars
    end
    @states_bars.each(&:update)
  end

  def dispose_states_bars
    @states_bars.each(&:dispose)
  end

  def create_states_bars
    @states_bars = states_bars_targets.map do |target|
      StatesBar.new @viewport2, target
    end
  end

  def states_bars_targets
    $game_party.alive_members + $game_troop.alive_members
  end
end