module Energy::Patch
end

require 'energy/patch/rpg_enemy_patch'
require 'energy/patch/rpg_actor_patch'
require 'energy/patch/rpg_skill_patch'
require 'energy/patch/game_battler_base_patch'
require 'energy/patch/game_actor_patch'
require 'energy/patch/game_enemy_patch'

require 'energy/patch/window_base_patch'
require 'energy/patch/window_skill_list_patch'

require 'energy/patch/vocab_patch'
require 'energy/patch/game_action_result_patch'

require 'energy/patch/game_battler_patch'
require 'energy/patch/window_battle_log_patch'
require 'energy/patch/window_battle_status_patch'

require 'energy/patch/rpg_item_patch'
require 'energy/patch/energy_recharge_rate_patch'