class Window_Base
  def energy_cost_color
    Color.new 255, 255, 0
  end

  def energy_gauge_color1
    tp_gauge_color1
  end

  def energy_gauge_color2
    tp_gauge_color2
  end

  def energy_color(actor)
    normal_color
  end

  def draw_actor_energy(actor, x, y, width = 124)
    draw_gauge x, y, width, actor.energy_rate, energy_gauge_color1, energy_gauge_color2
    change_color system_color
    draw_text x, y, 30, line_height, Vocab.energy_a
    change_color energy_color(actor)
    draw_text x + width - 42, y, 42, line_height, actor.energy.to_i, 2
  end
end