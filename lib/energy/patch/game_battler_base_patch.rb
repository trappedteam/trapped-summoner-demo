class Game_BattlerBase

  attr_writer :energy

  alias original_initliaze_for_energy initialize
  def initialize
    @energy = 100
    original_initliaze_for_energy
  end

  def skill_energy_cost(skill)
    skill.energy_cost
  end

  def skill_cost_payable?(skill)
    tp >= skill_tp_cost(skill) && mp >= skill_mp_cost(skill) && energy >= skill_energy_cost(skill)
  end

  def pay_skill_cost(skill)
    self.mp -= skill_mp_cost(skill)
    self.tp -= skill_tp_cost(skill)
    self.energy -= skill_energy_cost(skill)
  end

  def energy
    energy_user? ? @energy : 0
  end

  def energy_user?
    false
  end

  alias recover_all_for_energy recover_all 
  def recover_all
    recover_all_for_energy
    init_energy 
  end

  def energy_rate
    @energy.to_f / 100
  end
end