class Window_BattleLog
  alias display_damage_for_energy display_damage
  def display_damage(target, item)
    unless target.result.missed || target.result.evaded
      display_energy_damage target, item
    end

    display_damage_for_energy target, item
  end

  def display_energy_damage(target, item)
    return if target.dead? || target.result.energy_damage == 0
    Sound.play_recovery if target.result.energy_damage < 0
    add_text(target.result.energy_damage_text)
    wait
  end
end