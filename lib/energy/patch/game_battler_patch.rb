class Game_Battler
  EFFECT_GAIN_ENERGY = 401

  alias original_initialize_for_energy initialize
  def initialize
    @additional_effect_methods = { EFFECT_GAIN_ENERGY => :item_effect_gain_energy }
    original_initialize_for_energy
  end

  def item_effect_gain_energy(user, item, effect)
    value = effect.value1.to_i
    @result.energy_damage -= value
    @result.success = true if value != 0
    self.energy += value
  end

  alias item_user_effect_for_energy item_user_effect
  def item_user_effect(user, item)
    user.energy += item.energy_gain
    item_user_effect_for_energy user, item
  end

  alias regenerate_all_for_energy regenerate_all
  def regenerate_all
    regenerate_energy if alive?
    regenerate_all_for_energy
  end

  def regenerate_energy
    self.energy = [energy + energy_recharge_rate, 100].min if energy_user?
  end

  alias on_battle_start_for_energy on_battle_start 
  def on_battle_start
    init_energy 
    on_battle_start_for_energy
  end

  def init_energy
    self.energy = 100 if energy_user?
  end

  alias on_battle_end_for_energy on_battle_end 
  def on_battle_end
    init_energy
    on_battle_end_for_energy
  end
end