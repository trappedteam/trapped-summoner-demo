class RPG::Skill
  attr_writer :energy_cost, :energy_gain

  def energy_cost
    @energy_cost || 0
  end

  def energy_gain
    @energy_gain || 0
  end
end