class Game_Actor
  def energy_user?
    actor.energy_user?
  end

  def energy_recharge_rate
    actor.energy_recharge_rate
  end
end