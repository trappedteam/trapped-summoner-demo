class Game_Enemy
  def energy_user?
    enemy.energy_user?
  end

  def energy_recharge_rate
    enemy.energy_recharge_rate
  end
end