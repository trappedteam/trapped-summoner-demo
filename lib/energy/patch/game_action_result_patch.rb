class Game_ActionResult
  attr_accessor :energy_damage

  alias clear_damage_values_for_energy clear_damage_values
  def clear_damage_values
    @energy_damage = 0
    clear_damage_values_for_energy
  end

  def energy_damage_text
    if @energy_damage > 0
      fmt = @battler.actor? ? Vocab::ActorLoss : Vocab::EnemyLoss
      sprintf(fmt, @battler.name, Vocab.energy, @energy_damage)
    elsif @energy_damage < 0
      fmt = @battler.actor? ? Vocab::ActorGain : Vocab::EnemyGain
      sprintf(fmt, @battler.name, Vocab.energy, -@energy_damage)
    else
      ""
    end
  end
end