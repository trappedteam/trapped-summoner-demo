module Energy::Patch::RechargeRate
  DEFAULT_RATE = 10

  def energy_recharge_rate=(rate)
    @energy_recharge_rate = rate 
  end

  def energy_recharge_rate
    @energy_recharge_rate || DEFAULT_RATE
  end
end

[RPG::Actor, RPG::Enemy].each do |klass|
  klass.__send__ :include, Energy::Patch::RechargeRate
end