class RPG::Enemy
  attr_writer :energy_user
  
  def energy_user?
    !!@energy_user
  end
end