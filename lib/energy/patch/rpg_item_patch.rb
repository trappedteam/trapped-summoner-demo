class RPG::Item
  attr_writer :energy_gain

  def energy_gain
    @energy_gain || 0
  end
end