class Window_SkillList
  alias draw_skill_cost_for_energy draw_skill_cost
  def draw_skill_cost(rect, skill)
    if @actor.skill_energy_cost(skill) > 0
      change_color energy_cost_color, enable?(skill)
      draw_text rect, @actor.skill_energy_cost(skill), 2
    else
      draw_skill_cost_for_energy rect, skill
    end
  end
end