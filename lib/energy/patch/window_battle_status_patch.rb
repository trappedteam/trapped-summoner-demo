class Window_BattleStatus
  def draw_gauge_area_with_tp(rect, actor)
    draw_actor_hp actor, rect.x + 0, rect.y, 72
    draw_actor_mp actor, rect.x + 82, rect.y, 64
    if actor.energy_user?
      draw_actor_energy actor, rect.x + 156, rect.y, 64
    else
      draw_actor_tp actor, rect.x + 156, rect.y, 64
    end
  end
end