class Sprite_AnimatedBattler < Sprite_Battler
  ROWS = {
    iddle: 0,
    guard: 1, guarded: 1, deffence: 1, block: 1,
    dying: 2, woozy: 2, low_hp: 2,
    attacked: 3, taking_damage: 3,
    attack: 4, attacking: 4, counter_attack: 4,
    using_item: 5,item: 5,
    skill: 6,
    casting: 7, spell: 7, magic_reflection: 7,
    dash: 8, run: 8,
    run_away: 9,
    victory: 10,
    enter: 11, battle_start: 11,
    dead: 12, die: 12
  }

  MAX_ROWS = 14
  MAX_COLUMNS = 4

  def initialize(*args)
    @column = 0
    super
  end

  def new_bitmap
    Cache.battler @battler.sprite_name, @battler.battler_hue
  end

  def update_bitmap
    if bitmap != new_bitmap
      self.bitmap = new_bitmap
      set_sizes
      set_mirror
      init_visibility
    end
  end

  def update
    super
    if @battler
      @use_sprite = @battler.use_sprite?
      if @use_sprite
        update_bitmap
        set_rectangle
        update_origin
        update_position
        update_opacity
      end
      setup_new_effect
      setup_new_animation
      update_effect
    else
      self.bitmap = nil
      @effect_type = nil
    end
  end

  def update_opacity
    self.opacity = @battler.transparent? && @battler.alive? ? 100 : 255
  end

  def set_animation_origin
    offset = @height / 3
    if @animation.position == 3
      line_set = @battler.friends_unit.line_set
      @ani_ox = line_set.x
      @ani_oy = line_set.y - offset
    else
      @ani_ox = @battler.screen_x
      @ani_oy = @battler.screen_y - offset
      if @animation.position == 0
        @ani_oy -= offset
      elsif @animation.position == 2
        @ani_oy += offset
      end
    end
  end

  def revert_to_normal
    self.blend_type = 0
    self.color.set(0, 0, 0, 0)
    self.opacity = 255
  end

  private

  def set_sizes
    @width  = bitmap.width / MAX_COLUMNS
    @height = bitmap.height / MAX_ROWS
  end

  def set_rectangle
    new_y = row * @height
    new_x = column * @width
    src_rect.set new_x, new_y, @width, @height
  end

  def init_visibility
    self.opacity = 255
  end

  def set_mirror
    self.mirror = @battler.friends_unit.line_set.half == 1
  end

  def update_origin
    self.ox, self.oy = @width / 2, @height
  end

  def row
    @row = ROWS[@battler.pose].tap do |battler_row|
      @column = 0 unless battler_row == @row
    end
    @row
  end

  def speed
    row > 2 ? 0.22 : 0.075
  end

  def freeze?
    row > 2
  end

  def column
    value = @column
    if @column >= MAX_COLUMNS - 1
      @column = 0 unless freeze? 
    else
      @column += speed
    end
    value.to_i
  end
end
