class Strategy
  class << self
    def best
      strategies.max_by(&:success_index).new
    end

    def strategies
      @@strategies ||= []
    end

    def register
      strategies << self unless strategies.include? self
    end
  end

  def initialize
    @current_phase = :waiting
  end

  def lines_matrix
    matrix = [[], [], []]
    $game_troop.sort_by_priority!
    $game_troop.members.each_with_index do |enemy, index|
      wish_index = 0
      wish_index += 1 until matrix[enemy.lines_priority[wish_index] - 1].length < 2
      matrix[enemy.lines_priority[wish_index] - 1] << index
    end
    matrix
  end

  def current_phase
    switch_if_needed
    @current_phase
  end

  def actions(phase)
    damage_dealers.map { |enemy| action_for enemy, phase }
  end

  def cost_ok?(key, phase)
    actions(phase).all? do |action|
      skill, enemy = action[:wish_skill], action[:enemy]
      skill.nil? || (skill.public_send("#{key}_cost") * 3 <= enemy.public_send(key))
    end
  end

  def cost_low?(key, phase)
    actions(phase).all? do |action|
      skill, enemy = action[:wish_skill], action[:enemy]
      skill.nil? || (skill.public_send("#{key}_cost") > enemy.public_send(key))
    end
  end

  def action_for(enemy, phase = current_phase)
    Strategy::Role.find(enemy.current_role).action_for enemy, phase, target
  end

  def set_roles
    $game_troop.alive_members.each { |member| member.current_role = nil }
  end

  private

  def front_target
    weakest $game_party.line_set.front_battlers
  end

  def visible_taget
    weakest $game_party.line_set.visible_battlers
  end

  def weakest(actors)
    actors.min_by { |actor| actor.effective_hp attack_type }
  end

  def attack_type
    [:martial, :magical].max_by do |tag|
      damage_dealers.count { |dd| dd.tag? tag }
    end
  end

  def target
    $game_troop.alive_members.any?(&:melee?) ? front_target : visible_taget
  end

  def at_least(number, role)
    enemies_fit_for(role).sample(number).each do |enemy|
      enemy.current_role = role
    end
  end

  def rest_to(*roles)
    roles.each do |role|
      enemies_without_role.each do |enemy|
        enemy.current_role = enemy.tag?(role) ? role : role_key(enemy)
      end
    end
  end

  def role_key(enemy)
    roles.find { |role| enemy.tag? role }
  end

  def enemies_without_role
    $game_troop.alive_members.select { |member| member.current_role.nil? }
  end

  def enemies_fit_for(role)
    enemies_without_role.select { |enemy| enemy.tag? role }
  end
end
require 'strategy/concerns'
require 'strategy/rate'
require 'strategy/role'
require 'strategy/area'
require 'strategy/slice'
require 'strategy/cleave'
require 'strategy/solo'
require 'strategy/fist'
require 'strategy/needle'
