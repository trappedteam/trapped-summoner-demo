class EnemyDsl < DslManagerBase
  class << self
    def enemies
      unless @enemies.is_a? Array
        prepare!
        @enemies = []
        defenitions.each { |key, blocks| blocks.each { |block| instance_eval(&block) } }
      end
      @enemies
    end

    def enemy(name = nil, &block)
      EnemyBuilder.new.tap do |builder|
        builder.instance_eval(&block)
        all.each { |all_block| builder.instance_eval(&all_block) }
        builder.item.name = name if name
        enemies << builder.item
      end
    end

    def inject
      enemies.each { |enemy| $data_enemies[enemy.id] = enemy if enemy.id }
      save_data $data_enemies, 'Data/Enemies.rvdata2'
    end
  end
end

require 'enemy_dsl/base'
require 'enemy_dsl/enemy_builder'
require 'enemy_dsl/drop_item_builder'
require 'enemy_dsl/feature_builder'
