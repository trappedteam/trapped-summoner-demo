class StatesBar < Sprite_Base
  ICON_WIDTH = ICON_HEIGHT = 24
  Y_OFFSET = -80
  X_OFFSET = -30
  MAX_ICONS = 3
  ZOOM = 0.8

  def initialize(viewport, target, y_offset = Y_OFFSET)
    @target, @y_offset = target, y_offset
    @states, @buff_icons = [], []
    super viewport
    create_bitmap
    update
  end

  def update
    super
    update_bitmap
    update_position unless disposed?
  end

  def dispose
    self.bitmap.dispose
    super
  end

  private

  def update_bitmap
    if need_update?
      if @target.alive?
        self.visible = true
        self.bitmap.clear
        draw_icons
        @states = target_states
        @buff_icons = target_buff_icons
      else
        self.visible = false
      end
    end
  end

  def need_update?
    target_states != @states || target_buff_icons != @buff_icons
  end

  def target_states
    @target.states 
  end

  def target_buff_icons
    @target.buff_icons
  end

  def draw_icons
    (@target.state_icons + target_buff_icons).each.with_index do |icon_index, index| 
      draw_state_icon icon_index, index
    end
  end

  def draw_state_icon(icon_index, index)
    rect = Rect.new(
      icon_index % 16 * ICON_WIDTH,
      icon_index / 16 * ICON_HEIGHT,
      ICON_WIDTH, ICON_HEIGHT
    )
    bitmap.stretch_blt icon_rect(index), icons, rect
  end

  def icons
    Cache.system "Iconset"
  end

  def icon_rect(index)
    Rect.new ICON_WIDTH * index, 0, ICON_WIDTH, ICON_HEIGHT
  end

  def create_bitmap
    self.bitmap = Bitmap.new ICON_WIDTH * MAX_ICONS, ICON_HEIGHT
    self.zoom_x = self.zoom_y = ZOOM
  end

  def update_position
    self.x, self.y = @target.screen_x + X_OFFSET, @target.screen_y + @y_offset
    self.z = 50
  end
end

require 'states_bar/patch'