class LineSet
  attr_reader   :padding, :lines
  attr_accessor :half
  #halves:
  #1 2
  def initialize(half)
    check half

    @half = half
    @lines  = [Line::First.new(self), Line::Second.new(self), Line::Third.new(self)]
  end

  def slot_with(battler)
    slots.find { |slot| slot.battler == battler }
  end

  def line_with(battler)
    lines.find { |line| line.battlers.include? battler }
  end

  def index_of(battler)
    case line_with(battler)
    when Line::First
      0
    when Line::Second
      1
    when Line::Third
      2
    end
  end

  def slots
    lines.map(&:slots).flatten
  end

  def offset
    Graphics.height / 4
  end

  def front_battlers
    @lines.inject([]) do |result, line|
      return result if result.any?
      alive_and_visible line
    end
  end

  def visible_battlers
    result = @lines.each_with_object count: 0, battlers: [] do |line, object|
      battlers = alive_and_visible line
      if battlers.any? && object[:count] < 2
        object[:battlers] += battlers
        object[:count] += 1
      end
    end
    result[:battlers]
  end

  def battlers
    slots.map(&:battler).compact
  end

  def slot_index_of(battler)
    line_with(battler).slot_index_of(battler) if battlers.include? battler
  end

  def battlers_behind(battler)
    if index = slot_index_of(battler)
      lines[index_of(battler) + 1 .. 2].map { |line| line.slots[index].battler }.compact
    else
      []
    end
  end

  def x
    lines[1].slots[1].x
  end

  def y
    lines[1].slots[1].y
  end

  private

  def alive_and_visible(line)
    line.battlers.select { |battler| battler.visible? && battler.alive? }
  end

  def check(half)
    raise ArgumentError unless (1..2).to_a.include? half
  end
end