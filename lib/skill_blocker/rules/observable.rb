class SkillBlocker::Rules::Observable
  def blocks?(battler, skill)
    skill.effects.any? do |effect|
      effect.code == 21 && effect.data_id == RPG::State.transparent.id
    end && battler.observable?
  end

  SkillBlocker.rules << new
end