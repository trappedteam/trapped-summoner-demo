class Game_BattlerBase
  alias skill_conditions_met_for_skill_blocker skill_conditions_met?
  def skill_conditions_met?(skill)
    skill_conditions_met_for_skill_blocker(skill) && !SkillBlocker.blocks?(self, skill)
  end
end