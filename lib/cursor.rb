class Cursor < Sprite_Base
  WIDTH = 32
  HEIGHT = 32
  Y_OFFSET = -100
  X_OFFSET = -18
  MAX_PADDING = 20

  include AASM

  aasm do
    state :hidden,  initial: true
    state :rising
    state :falling

    event :rise do 
      transitions to: :rising
      after do 
        self.padding_correction = -0.5
      end
    end

    event :show do
      transitions to: :rising
      after do 
        self.visible = true
        self.padding_correction = -0.5
      end
    end

    event :fall do
      transitions to: :falling
      after do 
        self.padding_correction = 0.5
      end
    end

    event :hide do
      transitions to: :hidden
      after do
        self.visible = false
      end
    end
  end

  attr_accessor :padding_correction

  def initialize(viewport, color)
  	@color = color
  	super viewport
  	create_bitmap
    self.visible = false
  	self.z = 51
    self.opacity = 255
  	update
  end

  def dispose
  	self.bitmap.dispose
  	super
  end

  def mark(battler)
  	@original_y = @current_y = battler.screen_y + Y_OFFSET
    self.x, self.y = battler.screen_x + X_OFFSET, @original_y
  end

  def update
  	super
  	if visible?
  	  rise if falling? && @original_y <= y
  	  fall if rising?  && @original_y - MAX_PADDING >= y
  	  @current_y += padding_correction
  	  self.y = @current_y.to_i
  	end
  end

  def visible?
    !hidden?
  end

  private

  def create_bitmap
  	self.bitmap = Cache.system 'Arrows'
    src_rect.set *rect_coords, WIDTH, HEIGHT
  end


  def rect_coords
    case @color.to_s
    when 'green'
      [0, 0]
    when 'red'
      [6 * 32, 0]
    else
      [3 * 32, 0]
    end
  end
end