class Strategy::Rate
  def self.success_rate(&block)
    new.tap { |rate| rate.instance_eval(&block) }.rate
  end

  def initialize
    @prefer, @secondary = [], []
    @progressive = {}
  end

  def prefer(*roles)
    @prefer |= roles
  end

  alias firstly prefer

  def progressive(role, rules = {})
    @progressive[role] = rules
  end

  def secondary(*roles)
    @secondary |= roles
  end

  def rate
    hash = $game_troop.members.each_with_object rate: 0, enemies: [] do |enemy, hash|
      hash[:rate] +=  (enemy_rate(enemy.tags) + progressively(enemy.tags, hash[:enemies]))
      hash[:enemies] << enemy
    end
    hash[:rate]
  end

  private

  def progressively(tags, enemies)
    role, max = rule(tags)
    if role
      count = enemies.count { |enemy| enemy.tags.include? role }
      count > max.to_i ? 0 : count
    end.to_i * 2
  end

  def rule(tags)
    if (tags & @progressive.keys).any?
      role = tags.max { |tag| (@progressive[tag] && (@progressive[tag][:max] || Float::INFINITY)) || 0 }
      [role, @progressive[role][:max]]
    else
      [nil, nil]
    end
  end

  def enemy_rate(tags)
    if (@prefer & tags).any?
      2
    elsif (@secondary & tags).any?
      1
    end.to_i
  end
end