class Strategy::Slice < Strategy::Area
  def self.success_index
    Strategy::Rate.success_rate do
      prefer :area_ongoiner
      secondary :area_controller, :healer, :tank
    end
  end

  def current_phase
    :area_ongoing
  end

  def set_roles
    super
    at_least 2, :area_ongoiner
    at_least 2, :area_controller
    at_least 1, :healer
    at_least 1, :tank
    rest_to :area_ongoiner
  end

  def roles
    %w(
      area_ongoiner area_controller
      area_burster healer tank
      solo_burster solo_ongoiner solo_controller
    ).map(&:to_sym)
  end

  def damage_dealers
    $game_troop.alive_members.select { |member| member.tag? :area_ongoiner }
  end

  register
end
