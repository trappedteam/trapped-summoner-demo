class Strategy::Cleave < Strategy::Area
  def self.success_index
    Strategy::Rate.success_rate do
      prefer :area_burster
      progressive :area_burster, max: 2
      secondary :area_controller, :healer
    end
  end

  def set_roles
    super
    at_least 2, :area_burster
    at_least 1, :area_controller
    rest_to :area_burster
  end

  def damage_dealers
    $game_troop.alive_members.select { |member| member.tag? :area_burster }
  end

  private

  def tp_or_mp_gain_skill
    area_skills.select(&recharging).max_by(&damage)
  end

  def cheap_skill
    area_skills.select(&cheap).max_by(&damage)
  end

  def switch_if_needed
    if @current_phase == :waiting
      @current_phase = :area_burst if %w(tp mp energy).all? { |key| cost_ok? key, :area_burst }
    else
      @current_phase = :waiting if %w(tp mp energy).any? { |key| cost_low? key, :area_burst }
    end
  end

  def roles
    %w(
      area_burster area_controller
      area_ongoiner healer tank
      solo_burster solo_ongoiner solo_controller
    ).map(&:to_sym)
  end

  register
end
