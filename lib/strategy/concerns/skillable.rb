module Strategy::Concerns::Skillable
  def area_burst
    burst_skill area_skills
  end

  def solo_burst
    burst_skill solo_skills
  end

  def burst_skill(skills)
    skills.select { |skill| skill.tag? :high_damage }.max_by(&damage)
  end

  def area_ongoing
    ongoing_skill area_skills
  end

  def ongoing_skill(skills)
    skills.select { |skill| (skill.tags & [:normal_damage, :low_damage]).any? }.max_by(&damage)
  end

  def solo_ongoing
    ongoing_skill solo_skills
  end

  def area_controll
    area_skills.select { |skill| skill.tag? :controll }.select(&usefull_controll).sample
  end

  def defence
    $data_skills[@enemy.guard_skill_id]
  end

  def attack_skill
    $data_skills[@enemy.attack_skill_id]
  end

  def attack
    reachable { usable { [attack_skill] } }.first
  end

  def waiting
    tp_or_mp_gain_skill || cheap_skill || attack || defence
  end

  def usable
    yield.select { |skill|  @enemy.usable?(skill) || @wish_mode }
  end

  def reachable
    yield.select { |skill| @enemy.can_hit? target, with: skill }
  end

  def solo_skills
    reachable { usable { all_solo_skills + [attack_skill] } }
  end

  def all_solo_skills
    @enemy.skills.select(&solo)
  end

  def solo
    proc do |skill|
      skill.tag?(:solo) || !skill.tag?(:area) || !skill.for_all?
    end
  end

  def recharging
    proc do |skill|
      skill.any_tag?(:tp_gain, :mp_gain) || skill == attack_skill
    end
  end

  def cheap
    proc { |skill| skill.any_tag? :low_cost, :cheap, :free }
  end

  def area_skills
    usable do
      @enemy.skills.select { |skill| skill.tag?(:area) || skill.for_all? }
    end
  end
end