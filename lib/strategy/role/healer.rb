class Strategy::Role::Healer < Strategy::Role
  HEALTH_PERCENT = 75 / 100.0

  def skill
    area_heal || solo_heal || area_hot || 
      ally_without_hot { solo_hot } ||
      with_opositive_target { attack } || defence
  end

  def wish_skill
    skill
  end

  private

  def with_opositive_target
    if skill = yield
      @opositive_target = if @enemy.can_hit? @target, with: skill
        @target
      else
        $game_party.alive_members.find do |actor|
          @enemy.can_hit? actor, with: skill
        end
      end
      skill if @opositive_target
    end
  end

  def ally_without_hot
    if skill = yield
      @forced_target = alive_enemies.select do |member|
        !member.states.include? $data_states[14]
      end.max_by(&rest_hp)
      skill if @forced_target
    end
  end

  def solo_hot
    solo_skills.find { |skill| skill.tag? :hot }
  end

  def area_hot
    hot_needed? && area_skills.find { |s| s.tag?(:hot) }
  end

  def solo_heal
    need_heal.any? && solo_skills.select { |s| s.tag? :heal }.max_by(&heal_value)
  end

  def area_heal
    need_heal.count > 1 && area_skills.select { |s| s.tag? :heal }.max_by(&heal_value)
  end

  def target
    @forced_target || @opositive_target || weaker_enemy
  end

  def weaker_enemy
    alive_enemies.min_by(&rest_hp)
  end

  def rest_hp
    proc do |member|
      member.hp / member.mhp.to_f
    end
  end

  def need_heal
    alive_enemies.select { |member| member.hp < member.mhp * HEALTH_PERCENT }
  end

  def hot_needed?
    alive_enemies.any? { |member| !member.states.include? $data_states[14] }
  end

  def heal_value
    proc { |skill| eval skill.damage.formula }
  end

  def b
    weaker_enemy
  end

  register :healer
end
