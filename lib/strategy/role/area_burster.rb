class Strategy::Role::AreaBurster < Strategy::Role
  def hierarhy_skill
    area_burst || area_controll || area_ongoing  || attack || waiting  || defence
  end

  private

  def cheap_skill
    area_skills.select(&cheap).max_by(&damage)
  end

  def tp_or_mp_gain_skill
    area_skills.select(&recharging).max_by(&damage)
  end

  register :area_burster
end
