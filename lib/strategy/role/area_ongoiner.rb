class Strategy::Role::AreaOngoiner < Strategy::Role

  private

  def hierarhy_skill
    area_ongoing || area_controll || solo_ongoing  || attack || defence
  end

  register :area_ongoiner
end
