module Strategy::Role::Rogue
  def skill
    stealth || super
  end

  private

  def stealth
    @phase == :solo_burst && high_damage_stealth || normal_stealth
  end

  def high_damage_stealth
    when_stealth_required do
      burst_skill solo_skills.select { |skill| skill.tags? :stealth }
    end
  end

  def normal_stealth
    when_stealth_required do
      all_solo_skills.find { |skill| skill.tags? :stealth, :no_damage }
    end
  end

  def when_stealth_required
    yield if stealth_required?
  end

  def stealth_required?
    area_damage_absent? && !@enemy.observable? && !@enemy.transparent?
  end

  def area_damage_absent?
    $game_party.alive_members.none? do |actor|
      actor.any_tag? :area_ongoiner, :area_controller, :area_burster
    end
  end
end