class Strategy::Role::AreaController < Strategy::Role
  register :area_controller

  def area_ongoing
    area_controll || super
  end

  def area_burst
    area_controll || super
  end

  private

  def tp_or_mp_gain_skill
    hierarhy_skill
  end

  def cheap_skill
    hierarhy_skill
  end

  def hierarhy_skill
    area_controll || attack || defence
  end
end
