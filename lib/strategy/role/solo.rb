class Strategy::Role::Solo < Strategy::Role
  private

  def tp_or_mp_gain_skill
    solo_skills.select(&recharging).max_by(&damage)
  end

  def cheap_skill
    solo_skills.select(&cheap).max_by(&damage)
  end
end
