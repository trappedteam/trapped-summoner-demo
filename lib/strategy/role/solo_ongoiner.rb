class  Strategy::Role::SoloOngoiner < Strategy::Role::Solo
  def hierarhy_skill
    solo_ongoing || waiting || attack || defence
  end

  register :solo_ongoiner
end
