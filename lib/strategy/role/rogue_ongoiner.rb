class Strategy::Role::RogueOngoiner < Strategy::Role::SoloOngoiner
  include Strategy::Role::Rogue 

  def hierarhy_skill
    solo_ongoing || waiting || attack || perception || defence
  end

  register :rogue_ongoiner
end