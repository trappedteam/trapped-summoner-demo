class Strategy::Role::RogueBurster < Strategy::Role::SoloBurster
  include Strategy::Role::Rogue 

  def hierarhy_skill
    solo_burst  || solo_ongoing || waiting || attack || perception || defence
  end

  register :rogue_burster
end