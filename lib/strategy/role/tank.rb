class Strategy::Role::Tank < Strategy::Role
  attr_reader :target

  def initialize(enemy, phase, target)
    super
    @strategy_target = target
  end

  def skill
    @targret = nil
    with_self_target { self_protection_extreme } ||
      allies_protection ||
      ally_protection ||
      perception ||
      with_self_target { self_protection_long_time } || 
      with_opositive_target { self_protection_short_time || attack } ||
      defence
  end

  def wish_skill
    skill
  end

  private

  def self_protective_skills
    skills.select { |skill| skill.tags? :protective, :self }
  end

  def ally_protective_skills
    skills.select { |skill| skill.tags? :protective, :ally  }
  end

  def allies_protective_skills
    skills.select { |skill| skill.tags? :protective, :allies  }
  end

  def allies_protection
    allies_protective_skills.find do |skill|
      alive_enemies.any? { |enemy| stateable? skill, enemy }
    end
  end

  def ally_protection
    ally_protective_skills.find do |skill|
      if target = (alive_enemies - [@enemy]).find { |enemy| stateable? skill, enemy }
        @target = target
      end
    end
  end

  def self_protection_extreme
    if @enemy.hp < @enemy.mhp * 0.3
      self_protective_skills.find { |s| s.any_tag? :extreme, :extrimal } 
    end
  end

  def self_protection_long_time
    self_protective_skills.find do |skill|
      skill.tag?(:long_time) && stateable?(skill, @enemy)
    end
  end

  def self_protection_short_time
    self_protective_skills.find { |skill| skill.tag? :short_time }
  end

  def skills
    usable { @enemy.skills }
  end

  def stateable?(skill, target)
    (skill.states.empty? || (skill.states - target.states).any?) && @enemy.can_hit?(target, with: skill)
  end

  def with_self_target
    yield.tap { |result| result && @target = @enemy }
  end

  def with_opositive_target
    if skill = yield
      @target = if @enemy.can_hit? @strategy_target, with: skill
        @strategy_target
      else
        $game_party.alive_members.find { |member| @enemy.can_hit? member, with: skill }
      end
      skill if @target
    end
  end

  register :tank
end