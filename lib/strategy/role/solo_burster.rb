class Strategy::Role::SoloBurster < Strategy::Role::Solo
  def hierarhy_skill
    solo_burst  || solo_ongoing || waiting || attack || defence
  end

  register :solo_burster
end
