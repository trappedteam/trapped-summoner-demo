class Strategy::Area < Strategy
  def target
    $game_troop.alive_members.any?(&:melee_attack?) ? front_target : visible_taget
  end
end