class Strategy::Needle < Strategy::Solo
  def self.success_index
    Strategy::Rate.success_rate do
      prefer :solo_burster, :rogue_burster
      progressive :solo_burster, max: 3
      progressive :rogue_burster, max: 2
      secondary :controller, :healer, :tank
    end
  end

  def set_roles
    super
    at_least 3, :solo_burster
    at_least 2, :rogue_burster
    at_least 1, :healer
    at_least 1, :tank
    rest_to :solo_burster, :rogue_burster
  end

  private

  def damage_dealers
    $game_troop.alive_members.select do |member|
      member.any_tag? :solo_burster, :rogue_burster
    end
  end

  def switch_if_needed
    if @current_phase == :waiting
      @current_phase = :solo_burst if %w(tp energy mp).all? { |key| cost_ok? key, :solo_burst }
    else
      @current_phase = :waiting if %w(tp energy mp).any? { |key| cost_low? key, :solo_burst }
    end
  end

  def roles
    %w(
      solo_burster solo_ongoiner
      healer tank area_controller solo_controller
      area_burster area_ongoiner
    ).map(&:to_sym)
  end

  register
end