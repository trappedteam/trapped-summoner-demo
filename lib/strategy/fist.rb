class Strategy::Fist < Strategy::Solo
  def self.success_index
    Strategy::Rate.success_rate do
      prefer :solo_ongoiner, :rogue_ongoiner
      secondary :controller, :healer, :tank
    end
  end

  def set_roles
    super
    at_least 3, :solo_ongoiner
    at_least 2, :rogue_ongoiner
    at_least 1, :healer
    at_least 1, :tank
    rest_to :solo_ongoiner, :rogue_ongoiner
  end

  def current_phase
    :solo_ongoing
  end

  private

  def damage_dealers
    $game_troop.alive_members.select do |member|
      member.any_tag? :solo_ongoiner, :rogue_ongoiner
    end
  end

  def roles
    %w(
      solo_ongoiner solo_burster
      healer tank area_controller solo_controller
      area_ongoiner area_burster
    ).map(&:to_sym)
  end

  register
end