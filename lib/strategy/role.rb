class Strategy
  class Role
    include Strategy::Concerns::Skillable

    class << self
      def register(key)
        roles[key] = self unless roles[key]
      end

      def all
        roles.values
      end

      def roles
        @@roles ||= {}
      end

      def find(key)
        roles[key]
      end

      def action_for(enemy, phase, target = nil)
        new(enemy, phase, target).action
      end
    end

    attr_reader :target

    def initialize(enemy, phase, target)
      @target = target
      @enemy, @phase = enemy, phase
    end

    def action
      { skill: skill, target_index: target_index, enemy: @enemy, wish_skill: wish_skill }
    end

    def wish_skill
      @wish_mode = true
      try @phase
    ensure
      @wish_mode = false
    end

    def skill
      try(@phase) || hierarhy_skill
    end

    def perception
      if $game_party.alive_members.any?(&:transparent?)
        skills.find { |skill| skill.tag? :perception }
      end
    end

    private


    def alive_enemies
      $game_troop.alive_members
    end

    def damage
      proc do |skill|
        targets_for(skill).inject(0) { |sum, target| sum + target.check_damage_value(@enemy, skill) }
      end
    end

    def usefull_controll
      proc { |skill| !overlaping_state?(skill) }
    end

    def overlaping_state?(skill)
      if skill.for_all?
        $game_party.alive_members.count { |m| (m.states & skill.states).any? } /
          $game_party.alive_members.count.to_f > 0.5
      else
        (target.states & skill.states).any?
      end
    end

    def targets_for(skill)
      if skill.for_all?
        $game_party.alive_members
      else
        [target]
      end
    end

    def target_index
      target.friends_unit.members.index target if target
    end
  end
end

%w(
  area_burster area_ongoiner solo solo_burster solo_ongoiner healer
  tank solo_controller area_controller rogue rogue_burster rogue_ongoiner
).each do |role|
  require "strategy/role/#{role}"
end