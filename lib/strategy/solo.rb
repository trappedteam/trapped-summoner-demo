class Strategy::Solo < Strategy
  private

  def target
    damage_dealers.any?(&:melee?) ? front_target : visible_taget
  end
end