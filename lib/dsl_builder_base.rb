class DslBuilderBase
  attr_reader :item

  def self.define_methods_for(*method_hashes)
    method_hashes.each do |hash|
      hash.each do |value, method_names|
        [method_names].flatten.each do |method_name|
          define_method(method_name) { value }
        end
      end
    end
  end

  ELEMENTS = {
    -1 => :common, 0 => %w(none nothing), 1 => %w(physical martial),
    2 => %w(drain vampiric), 3 => 'fire', 4 => 'ice', 5 => %w(lighting electrical),
    6 => 'water', 7 => 'earth', 8 => %w(air wind), 9 => %w(holy holly), 10 => 'shadow'
  }

  WTYPES  = {
    1 => 'axe', 2 => %w(claw claws), 3 => 'spear', 4 => 'sword', 5 => %w(katana catana),
    6 => 'bow', 7 => %w(dagger dager), 8 => 'hammer', 9 => 'staff', 10 => %w(gun fire_shooter)
  }

  ATYPES = {
    1 => 'common', 3 => 'light', 4 => %w(massive heavy), 5 => 'small_shield', 6 => %w(big_shield shield)
  }

  PARAMS = {
    0 => 'mhp', 1 => 'mmp', 2 => 'atk', 3 => %w(dff defence defense),
    4 => 'mat', 5 => 'mdf', 6 => 'agi', 7 => 'luk'
  }

  XPARAMS = {
    0 => %w(acu acuracy), 1 => %w(eva evasion), 2 => %w(cri critical), 3 => %w(res resilence),
    4 => %w(sev spell_evasion), 5 => %w(sre spell_reflection), 6 => %w(cou counter_attack),
    7 => %w(hpr hp_regeneration), 8 => %w(mpr mp_regeneration), 9 => %w(tpr tp_regeneration)
  }

  SPARAMS = {
    0 => 'target_chance', 1 => %w(deff_effect_rate deffence_effect_rate), 2 => %w(rec_effect_rate recovery_effect_rate),
    3 => 'medical', 4 => 'spell_mp_cost', 5 => 'tp_rate', 6 => %w(deal_physical_damage deal_phys_damage deal_phys_dmg),
    7 => %w(deal_magical_damage del_mag_damage deal_mag_dmg), 8 => %w(cutting_age_damage cutting_age_dmg fall_damage fall_dmg),
    8 => 'exp_rate'
  }


  define_methods_for ELEMENTS, WTYPES, ATYPES, PARAMS, XPARAMS, SPARAMS

  def method_missing(*args, &block)
    method_name = :"#{args[0]}="

    if item.public_methods.include? method_name
      item.public_send(method_name, *args[1..-1], &block)
    else
      super
    end
  end

  def data(id)
    data_id id
  end

  def shared(key, &block)
    manager_class.shared(key, &block) if block_given?
    (manager_class.fragments[key] || []).each { |fragment| instance_eval(&fragment) }
  end

  def tag(*tags)
    item.tags += tags
  end
  alias tags tag

  def remove_tag(*tags)
    item.tags -= tags
  end
  alias remove_tags remove_tag
end
