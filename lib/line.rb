class Line
  ALPHA  = Math::PI / 8

  attr_reader :slots

  def initialize(line_set)
    @line_set = line_set
    @max_number_of_actors = 2
    @slots = (0..2).to_a.map { |index| Line::Slot.new(self, index) }
  end

  def add(battler)
    add_battler battler if battlers.size < @max_number_of_actors
  end

  def battlers
    @slots.map(&:battler).compact
  end

  def remove(battler)
    @slots.find { |slot| slot.battler == battler }.battler = nil
    if battlers.size > 0
      @slots[1].battler = battlers.first
      @slots[0].battler, @slots[2].battler = [nil] * 2
    end
  end

  def slot_with(battler)
    @slots.find { |s| s.battler == battler }
  end

  def slot_index_of(battler)
    @slots.index slot_with(battler)
  end

  def heights
    (1 .. 3).to_a.map { |i|  Graphics.height - offset - 40 * i }
  end

  private

  def in_correct_half
    number = yield
    Graphics.width / 2 + (@line_set.half == 1 ? -number : number)
  end

  def offset
    @line_set.offset
  end

  def add_battler(battler)
    if battlers.size == 0
      @slots[1].battler = battler
    else
      old_battler = @slots[1].battler
      @slots[0].battler, @slots[1].battler, @slots[2].battler = old_battler, nil, battler
    end
  end
end

require 'line/first'
require 'line/second'
require 'line/third'
require 'line/slot'
require 'line/distance'