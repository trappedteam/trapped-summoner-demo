class SkillDsl
  class DamageBuilder < Base
    TYPES = {
      0 => %w(buff debuff), 1 => :to_hp, 2 => :to_mp,
      3 => 'heal_hp', 4 => 'heal_mp', 5 => 'drain_hp', 6 => 'drain_mp'
    }

    define_methods_for TYPES

    def element(id)
      element_id id
    end

    def critical(value)
      @item.critical = value
    end

    def initialize(item = nil)
      @item = item || RPG::UsableItem::Damage.new
    end
  end
end
