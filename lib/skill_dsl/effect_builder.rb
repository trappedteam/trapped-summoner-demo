class SkillDsl
  class EffectBuilder < Base
    CODES = {
      11 => 'recover_hp', 12 => 'recover_mp', 13 => 'gain_tp', 21 => 'add_state',
      22 => 'remove_state', 31 => 'add_buff', 32 => 'add_debuff', 33 => 'remove_buff',
      34 => 'remove_debuff', 41 => 'special', 42 => 'raise_parameter', 43 => 'learn_skill',
      44 => 'common_event', 401 => 'gain_energy'
    }

    define_methods_for CODES

    def initialize(item = nil)
      @item = item || RPG::UsableItem::Effect.new
    end

    def effect(value)
      code value
    end
  end
end
