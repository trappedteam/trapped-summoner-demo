class SkillDsl
  class SkillBuilder  < Base
    HIT_TYPES = { 1 => 'physical', 0 => 'certain', 2 => 'magical' }
    SCOPES = {
      0 => %w(none empty), 1 => 'enemy', 2 => 'all_enemies', 3 => 'double_on_enemy',
      4 => 'random_enemy', 5 => 'two_random_enemies', 6 => 'three_random_enemies',
      7 => %w(ally aly friend), 8 => %w(all_allies all_alies all_friends),
      9 => %w(dead_friend dead_ally dead_aly), 10 => %w(all_dead_friends all_dead_allies all_dead_alies),
      11 => %w(me my_self myself)
    }
    OCCASIONS = { 0 => %w(all_the_time allways), 1 => 'only_in_battle', 2 => 'only_from_menu', 3 => 'never' }
    STYPES    = { 1 => %w(special spec), 2 => 'magic' }



    define_methods_for SCOPES, HIT_TYPES, OCCASIONS, STYPES

    def initialize
      @item = RPG::Skill.new
    end

    def animation(key, value)
      @item.animations[key] = value
    end

    def class_name(name = nil)
      name ? @class_name = name : (@class_name || 'general')
    end

    def damage(&block)
      @item.damage = DamageBuilder.new(@item.damage).tap { |builder| builder.instance_eval(&block) }.item
    end

    def effect(&block)
      @item.effects << EffectBuilder.new.tap { |builder| builder.instance_eval(&block) }.item
    end

    def expand_effect(&block)
      EffectBuilder.new(@item.effects.last).tap { |builder| builder.instance_eval(&block) }
    end

    def required_weapon(id)
      @item.required_wtype_id1.to_i > 0 ? required_wtype_id2(id) : required_wtype_id1(id)
    end

    def required_armor(id)
      @item.required_atype_id1.to_i > 0 ? required_atype_id2(id) : required_atype_id1(id)
    end

    def message(text)
      text = ' ' + text
      @item.message1.to_s.length > 0 ? message2(text) : message1(text)
    end

    def stype(id)
      stype_id id
    end

    def pose(value)
      @item.pose = value
    end
  end
end
