class Summoning::Summoner
  class << self
    def summon(matrix)
      instance = new matrix
      instance.summon
      instance.place
    end

    def release
      $game_party.members.each do |member|
        if member.summon?
          member.on_battle_end
          $game_party.remove_actor member.id
        end
      end
    end
  end

  def initialize(matrix)
    @matrix = matrix
  end

  def summon
    @matrix.flatten.compact.each.with_index do |actor, index|
      if actor.summon?
        actor.change_level $game_party.leader.level, false
        actor.recover_all
        $game_party.add_actor actor.id, index
      end
    end
  end

  def place
    $game_party.init_lines_matrix lines_matrix
  end

  def lines_matrix
    @matrix.map { |arr| arr.compact.map { |actor| $game_party.members.index actor } }
  end
end