#encoding=utf-8
module Summoning::Vocab
  def lines
    [first_line, second_line, third_line, to_battle]
  end

  def first_line
    'Линия плотного боя.'
  end

  def second_line
    <<-TEXT
      Воины первой линии
      прикрывают вторую линию
      от атак плотного боя.
    TEXT
  end

  def third_line
    <<-TEXT
      Воины первой и второй
      линии прикрывают
      третью линию от 
      прямых атак.
    TEXT
  end

  def to_battle
    'В бой'
  end

  extend self
end
