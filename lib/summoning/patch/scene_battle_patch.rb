class Scene_Battle
  alias create_all_windows_original_for_summoning create_all_windows
  def create_all_windows
    create_all_windows_original_for_summoning
    create_summoning_window
  end

  def create_summoning_window
    w, h = Graphics.width / 2, Graphics.height
    @summoning_lines_window = Summoning::Window::Lines.new 0, 0, w, h
    @summoning_list_window = Summoning::Window::List.new w, 0, w, h/ 2
    @summoning_info_window = Summoning::Window::Info.new w, h / 2, w, h / 2
    @summoning_lines_window.list = @summoning_list_window
    @summoning_lines_window.info = @summoning_info_window
    @summoning_list_window.lines = @summoning_lines_window
    @summoning_list_window.info = @summoning_info_window
  end

  def battle_start
    BattleManager.battle_start
    process_event
    start_summoning
  end

  def start_summoning
    @summoning_lines_window.on_finish do
      start_party_command_selection
    end
    @summoning_lines_window.setup
  end
end
