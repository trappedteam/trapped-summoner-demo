class Game_Party
  def add_actor(actor_id, index = nil)
    index = index || @actors.length
    
    unless @actors.include?(actor_id)
      existing = @actors[index]
      @actors[index] = actor_id 
      @actors.push existing
      @actors.compact!
    end

    $game_player.refresh
    $game_map.need_refresh = true
  end
end