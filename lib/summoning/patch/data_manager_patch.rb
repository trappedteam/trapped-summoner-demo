module DataManager
  instance_eval do
    alias make_save_contents_for_summoning make_save_contents
    def make_save_contents
      make_save_contents_for_summoning.tap do |contents|
        contents[:summons] = Summoning.ids
      end
    end

    alias extract_save_contents_for_summoning extract_save_contents
    def extract_save_contents(contents)
      extract_save_contents_for_summoning contents
      Summoning.ids = contents[:summons]
    end
  end
end
