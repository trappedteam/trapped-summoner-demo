module BattleManager
  instance_eval do
    def need_summon_release?
      @phase && (
        $game_party.members.empty? || $game_party.all_dead? ||
        $game_troop.all_dead? || aborting?
      )
    end

    def need_resurect?
      return false unless Summoning::SOFT_MODE
      summons, battlers = $game_party.alive_members.each_with_object([[], []]) do |member, result|
        (member.summon? ? result[0] : result[1]) << member
      end
      summons.any? && battlers.empty?  
    end

    def resurect_real_battlers
      $game_party.dead_members.each do |member|
        member.remove_state 1 if member.dead? && !member.summon?
      end
    end

    alias judge_win_loss_for_summoning judge_win_loss
    def judge_win_loss
      if need_summon_release?
        resurect_real_battlers if need_resurect?
        Summoning::Summoner.release 
      end
      judge_win_loss_for_summoning
    end
  end
end