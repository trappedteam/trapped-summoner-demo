class Summoning::Window::Info < Window_Base
  LINE_HEIGHT = 23

  def initialize(*args)
    super
    hide
  end

  def alert(text)
    contents.clear
    text.lines.each.with_index do |line, index|
      contents.draw_text(*sizes(index), line.strip)
    end
  end

  def sizes(index)
    [0, LINE_HEIGHT * index, contents.rect.width, LINE_HEIGHT * (index + 1)]
  end
end
