class Summoning::Window::Lines < Summoning::Window::Base
  configure col_max: 2, item_max: 7, item_width: 96, item_height: 96
  attr_accessor :list, :scene

  def initialize(x, y, width, height)
    @matrix = Array.new(3) { Array.new(2) { nil } }
    @heroes, @summons = $game_party.members, Summoning.summons.uniq(&:name)
    super
  end

  def on_finish(&block)
    @on_finish = block
  end

  def set(actor)
    @matrix[index / 2][index % 2] = actor
  end

  def on_ok
    if to_battle? && full_party?
      close
      Summoning::Summoner.summon @matrix
      @on_finish.call
    else
      list.setup items
    end
  end

  def setup
    alert 0
    super
    select last_index
  end

  def last_index
    index > 0 ? index : 0
  end

  def alert(i = index)
    info.alert Summoning::Vocab.lines[i / 2]
  end

  def current_item_enabled?
    enabled? index
  end

  def draw_item(i)
    if to_battle? i
      draw_swords
    elsif @matrix.flatten[i]
      draw_actor i
    else
      draw_cross i
    end
  end

  def refresh
    create_contents
    draw_all_items
  end

  def open
    info.show.open
    super
  end

  def close
    info.close
    super
  end

  private

  def items
    if current_actor?
      [@matrix.flatten[index]] + rest_heroes
    elsif full_party?
      rest_summons
    else
      rest_heroes
    end
  end

  def enabled?(i)
    to_battle?(i) ? full_party? : @matrix.flatten[i] || empty_places?
  end

  def draw_cross(i)
    draw_icon Cache.system(full_matrix? ? 'grey_cross' : 'yellow_cross'), i
  end

  def draw_actor(i)
    actor = @matrix.flatten[i]
    rect = item_rect i
    draw_actor_face actor, rect.x, rect.y
  end

  def draw_swords
    draw_icon Cache.system(full_matrix? ? 'active_swords' : 'grey_swords'), 6
  end

  def draw_icon(bitmap, i)
    contents.stretch_blt item_rect(i), bitmap, bitmap.rect
  end

  def current_actor?
    @heroes.include? @matrix.flatten[index]
  end

  def rest_heroes
    @heroes - @matrix.flatten
  end

  def rest_summons
    @summons - @matrix.flatten
  end

  def full_party?
    (@heroes - @matrix.flatten).none?
  end

  def full_matrix?
    full_party? && !empty_places?
  end

  def empty_places?
    @matrix.flatten.compact.size < 4
  end

  def to_battle?(i = index)
    i == 6
  end
end
