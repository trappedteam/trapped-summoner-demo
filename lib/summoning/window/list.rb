class Summoning::Window::List < Summoning::Window::Base
  attr_accessor :lines

  def initialize(*args)
    @items = []
    super
  end

  def item_max
    @items.size
  end

  def alert
    @items[index] && /<info>(.*)<\/info>/m =~ @items[index].actor.note
    info.alert $1 || ''
  end

  def names
    ['None'] + @items[1..-1].map(&:name)
  end

  def on_ok
    lines.set @items[index]
    close
    lines.setup
  end

  def on_cancel
    close
    lines.setup
  end

  def draw_item(index)
    draw_text item_rect(index), names[index]
  end

  def setup(items)
    @items = [nil] + items
    info.alert ''
    super()
    select 0
  end

  def refresh
    contents.clear
    draw_all_items
  end
end
