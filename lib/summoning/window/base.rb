class Summoning::Window::Base < Window_Selectable
  attr_accessor :info

  def self.configure(hash)
    class_eval do
      hash.each do |method_name, value|
        define_method(method_name) { value }
      end
    end
  end

  def initialize(*args)
    super
    set_handler :ok, method(:on_ok) if respond_to? :on_ok
    set_handler :cancel, method(:on_cancel) if respond_to? :on_cancel
    hide
  end

  def setup
    refresh
    activate
    show
    open
  end

  def cursor_down(wrap = false)
    super
    alert
  end

  def cursor_up(wrap = false)
    super
    alert
  end
end
