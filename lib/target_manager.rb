class TargetManager
  def initialize(actor, target, spell)
    @actor, @target, @spell = actor, target, spell
  end

  def can_hit?
    @target && (area? || (!alive_and_transparent? && (melee? || range?)))
  end

  private

  def alive_and_transparent?
    @target.transparent? && @target.alive?
  end

  def range?
    !@spell.melee? && distance <= max_distance_for_range
  end

  def max_distance_for_range
    @actor.line.is_a?(Line::Second) ? 3 : 2
  end

  def distance
    @actor.distance_to @target
  end

  def area?
    @spell.for_all?
  end

  def melee?
    @spell.melee? && distance <= 1
  end
end
