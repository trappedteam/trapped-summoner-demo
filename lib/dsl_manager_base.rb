class DslManagerBase
  class << self
    attr_writer :current_class

    def current_class
      @current_class || 'common'
    end

    def define(class_name = 'common', &block)
      defenitions[class_name] ||= []
      defenitions[class_name] << block
    end

    def prepare!
      to_prepare.each { |block| instance_eval(&block) }
    end

    def prepare(&block)
      to_prepare << block
    end

    def defenitions
      @defenitions ||= {}
    end

    def fragments
      @fragments ||= {}
    end

    def shared(key, &block)
      fragments[key] ||= []
      fragments[key] << block
    end

    def all(&block)
      @all ||= []
      @all << block if block_given?
      @all
    end

    def to_prepare
      @to_prepare ||= []
    end
  end
end
