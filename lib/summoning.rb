module Summoning
  #resurect real battlers if any summons alive and all real battlers dead
  SOFT_MODE = true
  ORIGINAL_SUMMONS = (16..20).to_a

  class << self
    attr_writer :ids

    def add(summon)
      if id = summon.is_a?(Integer) ? summon : $game_actors.index(summon)
        self.ids += id
        self.ids.uniq!
      end
    end

    def summons
      ids.map { |id| $game_actors[id] }
    end

    def summon?(actor)
      ids.include? actor.id
    end

    def ids
      @ids || ORIGINAL_SUMMONS
    end
  end
end

require 'summoning/vocab'
require 'summoning/summoner'
require 'summoning/window'
require 'summoning/patch'
