class SkillDsl < DslManagerBase
  class << self
    def skill(name = nil, &block)
      SkillBuilder.new.tap do |builder|
        builder.item.name = name if name
        all.each { |all_block| builder.instance_eval(&all_block) }
        builder.instance_eval(&block)
        skills[current_class] ||= []
        skills[current_class] << builder.item
      end
    end

    def skills
      unless @skills.is_a? Hash
        prepare!
        @skills = {}
        defenitions.each do|name, blocks|
          self.current_class = name
          blocks.each { |block| instance_eval(&block) }
        end
      end
      @skills
    end

    def inject
      skills.each { |class_name, skills| skills.each { |skill| $data_skills[skill.id] = skill if skill.id } }
      save_data $data_skills, 'Data/Skills.rvdata2'
    end
  end
end

require 'skill_dsl/base'
require 'skill_dsl/damage_builder'
require 'skill_dsl/effect_builder'
require 'skill_dsl/skill_builder'
