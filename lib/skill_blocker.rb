module SkillBlocker
  def self.blocks?(battler, skill)
    rules.any? { |rule| rule.blocks? battler, skill }
  end

  def self.rules
    @rules ||= []
  end
end

require 'skill_blocker/patch'
require 'skill_blocker/rules'