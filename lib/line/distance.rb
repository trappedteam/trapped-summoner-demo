class Line::Distance
  def initialize(first, second)
    check first, second
    @first, @second             = first, second
    @first_set, @second_set     = set(@first), set(@second)
    @first_index, @second_index = @first_set.index_of(@first), @second_set.index_of(@second)
  end

  def distance
    if @first_set == @second_set
      count(*[@first_index, @second_index].sort, @first_set)
    else
      count(0, @second_index, @second_set) + count(0, @first_index, @first_set)
    end -1
  end

  private

  def class_names
    [@first, @second].map { |battler| battler.line.class.name }.sort
  end

  def set(battler)
    [$game_troop, $game_party].map(&:line_set).find do |line_set|
      line_set.battlers.include? battler
    end
  end

  def count(from, to, line_set)
    line_set.lines[from .. to].count do |line|
      line.battlers.any? do |battler|
        !battler.transparent? || [@first, @second].include?(battler)
      end
    end
  end

  def check(*battlers)
    unless battlers.all? { |b| b.is_a? Game_Battler }
      raise ArgumentError.new('Only battlers allowed')
    end
  end
end
