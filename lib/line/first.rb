class First < Line
  def width
    in_correct_half { Graphics.width / 10 }
  end
end