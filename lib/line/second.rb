class Second < Line
  def width
    in_correct_half { Graphics.width / 10 + Graphics.width / 8 }
  end
end