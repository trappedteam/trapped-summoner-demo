class Line::Slot
  attr_accessor :battler
  
  def initialize(line, index)
    @line, @index = line, index
  end

  def x
    @line.width
  end

  def y
    @line.heights[@index]
  end
end