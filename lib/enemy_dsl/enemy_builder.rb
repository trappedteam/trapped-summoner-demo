class EnemyDsl
  class EnemyBuilder < Base

    PARAMS.each do |index, method_names|
      [method_names].flatten.each do |method_name|
        define_method method_name do |value|
          @item.params[index] = value
        end
      end
    end

    def initialize
      @item = RPG::Enemy.new
    end

    def skill(sk)
      skills [sk]
    end

    def skills(sks)
      @item.skills |= sks
    end

    def drop_item(&block)
      @item.drop_items << DropItemBuilder.new.tap do |builder|
        builder.instance_eval(&block)
      end.item
    end

    def expand_drop_item(&block)
      EffectBuilder.new(@item.drop_items.last).tap { |builder| builder.instance_eval(&block) }
    end

    def feature(&block)
      @item.features << FeatureBuilder.new.tap { |builder| builder.instance_eval(&block) }.item
    end

    def new_feature(&block)
      @item.features = []
      feature(&block)
    end

    def expand_feature(&block)
      FeatureBuilder.new(@item.features.last).tap { |builder| builder.instance_eval(&block) }
    end

    def sprite(name)
      @item.sprite_name = name
    end

    def copy_params(klass, lvl)
      %w(mhp mmp atk dff mat mdf agi luk).each.with_index do |method_name, index|
        public_send method_name, klass.params[index, lvl]
      end
    end

    def copy_skills(klass, lvl = 999)
      klass.learnings.each_with_object([]) do |learning, result|
        result << $data_skills[learning.skill_id] if learning.level <= lvl
      end.tap { |array| skills array }
    end
  end
end
