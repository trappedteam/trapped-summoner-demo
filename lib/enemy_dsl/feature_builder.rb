class EnemyDsl
  class FeatureBuilder < Base
    CODES = {
      Game_BattlerBase::FEATURE_ELEMENT_RATE  => 'element_rate',
      Game_BattlerBase::FEATURE_DEBUFF_RATE   => 'debuff_rate',
      Game_BattlerBase::FEATURE_STATE_RATE    => 'state_rate',
      Game_BattlerBase::FEATURE_STATE_RESIST  => 'state_resist',
      Game_BattlerBase::FEATURE_PARAM         => 'param',
      Game_BattlerBase::FEATURE_XPARAM        => 'xparam',
      Game_BattlerBase::FEATURE_SPARAM        => 'sparam',
      Game_BattlerBase::FEATURE_ATK_ELEMENT   => 'atk_element',
      Game_BattlerBase::FEATURE_ATK_STATE     => 'atk_state',
      Game_BattlerBase::FEATURE_ATK_SPEED     => 'atk_speed',
      Game_BattlerBase::FEATURE_ATK_TIMES     => 'atk_times',
      Game_BattlerBase::FEATURE_STYPE_ADD     => 'stype_add',
      Game_BattlerBase::FEATURE_STYPE_SEAL    => 'stype_seal',
      Game_BattlerBase::FEATURE_SKILL_ADD     => 'skill_add',
      Game_BattlerBase::FEATURE_SKILL_SEAL    => 'skill_seal',
      Game_BattlerBase::FEATURE_EQUIP_WTYPE   => 'equip_wtype',
      Game_BattlerBase::FEATURE_EQUIP_ATYPE   => 'equip_atype',
      Game_BattlerBase::FEATURE_EQUIP_FIX     => 'equip_fix',
      Game_BattlerBase::FEATURE_EQUIP_SEAL    => 'equip_seal',
      Game_BattlerBase::FEATURE_SLOT_TYPE     => 'slot_type',
      Game_BattlerBase::FEATURE_ACTION_PLUS   => 'action_plus',
      Game_BattlerBase::FEATURE_SPECIAL_FLAG  => 'special_flag' ,
      Game_BattlerBase::FEATURE_COLLAPSE_TYPE => 'collapse_type',
      Game_BattlerBase::FEATURE_PARTY_ABILITY => 'party_ability',
    }

    define_methods_for CODES

    def initialize(feature = RPG::BaseItem::Feature.new)
      @item = feature
    end
  end
end