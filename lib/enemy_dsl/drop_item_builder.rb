class EnemyDsl
  class DropItemBuilder < Base
    KINDS =  { 1 => 'stuff', 2 => 'weapon', 3 => 'armor' }
    define_methods_for KINDS

    def initialize(item = RPG::Enemy::DropItem.new)
      @item = item
    end
  end
end
