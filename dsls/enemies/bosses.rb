EnemyDsl.define do
  enemy 'Den Frosty' do
    id 1

    exp 500
    gold 500

    copy_skills $data_classes[16], 10
    copy_params $data_classes[16], 10

    lines_priority 3, 2, 1

    drop_item do
      kind stuff
      data 1
      denominator 1
    end

    expand_feature do
      code atk_element
      data ice
    end

    feature do
      code element_rate
      data ice
      value 2
    end

    feature do
      code element_rate
      data fire
      value 0
    end

    feature do
      code sparam
      data deal_mag_dmg
      value 1.5
    end

    feature do
      code param
      data mmp
      value 1.2
    end
  end
end
