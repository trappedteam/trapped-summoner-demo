#encoding=utf-8

EnemyDsl.define do
  (1..10).to_a.each do |lvl|
    enemy "Бихолд" do
      id  2 + lvl
      sprite 'behold'
      battler_name 'gayzer'
      copy_params $data_classes[14], lvl
      copy_skills $data_classes[14], lvl

      exp 20 * lvl
      gold 0

      tags :martial, :solo_ongoiner, :range
      tag  :solo_burster if lvl >= 3

      lines_priority 2, 1, 3

      new_feature do
        code xparam
        data acu
        value 1
      end

      feature do
        code xparam
        data critical
        value 0.1
      end

      feature do
        code param
        data agi
        value 1.1
      end

      feature do
        code param
        data atk
        value 1.1
      end
    end

    enemy "Лич" do
      id 12 + lvl
      sprite 'lich'
      battler_name 'Wizard_f'

      copy_skills $data_classes[16], lvl
      copy_params $data_classes[16], lvl

      exp 20 * lvl
      gold 0

      lines_priority 3, 2, 1

      new_feature do
        code param
        data mmp
        value 1.2
      end

      feature do
        code xparam
        data acu
        value 0.8
      end

      feature do
        code sparam
        data deal_mag_dmg
        value 1.2
      end

      tags :magical, :range, :area_ongoiner
      tag :area_burster if lvl >= 3
      tag :area_controller if lvl >= 2
    end


    enemy "Летучая мышь" do
      id 22 + lvl
      sprite 'bat'
      battler_name 'Cleric_f'

      copy_skills $data_classes[15], lvl
      copy_params $data_classes[15], lvl

      lines_priority 3, 2, 1

      new_feature do
        code xparam
        data acu
        value 0.8
      end

      exp 30 * lvl
      gold 0

      tags :magical, :range, :healer
    end


    enemy "Скорпион" do
      id 32 + lvl
      sprite 'sand_king'
      battler_name 'Paladin_m'
      copy_params $data_classes[12], lvl
      copy_skills $data_classes[12], lvl
      exp 30 * lvl
      gold 0
      tags :martial, :tank, :melee
      lines_priority 1, 2, 3

      new_feature do
        code element_rate
        data martial
        value 0.9
      end

      feature do
        code xparam
        data acu
        value 0.95
      end

      [drain, fire, ice, lighting, water, earth, air, holly, shadow].each do |elem|
        feature do
          code element_rate
          data elem
          value 0.9
        end
      end

      feature do
        code param
        data mhp
        value 1.1
      end

      feature do
        code param
        data dff
        value 1.1
      end

      feature do
        code param
        data mdf
        value 1.1
      end

      feature do
        code param
        data atk
        value 0.8
      end

      feature do
        code xparam
        data eva
        value 0.05
      end
    end

    enemy "Призрак" do
      id 42 + lvl
      sprite 'ghost'
      battler_name 'Thief_m'
      copy_params $data_classes[13], lvl
      copy_skills $data_classes[13], lvl
      exp 30 * lvl
      gold 0
      tags :martial, :rogue, :melee, :stealther, :rogue_ongoiner
      tag :rogue_burster if lvl >= 3
      lines_priority 1, 2, 3
      energy_user true

      new_feature do
        code param
        data atk
        value 1.1
      end

      feature do
        code xparam
        data acu
        value 0.95
      end

      feature do
        code param
        data luk
        value 1.1
      end

      feature do
        code xparam
        data critical
        value 0.3
      end
    end

    enemy "Синий слизень" do 
      id 52 + lvl 
      sprite 'blue_slime'
      battler_name 'Slime'
      copy_params $data_classes[17], lvl
      copy_skills $data_classes[17], lvl

      exp 10 * lvl
      gold 0

      tags :martial, :minion, :melee, :solo_ongoiner

      lines_priority 1, 2, 3

      new_feature do
        code xparam
        data acu
        value 0.95
      end
    end

    enemy "Красный слизень" do 
      id 62 + lvl 
      sprite 'red_slime'
      battler_name 'Slime'
      copy_params $data_classes[18], lvl
      copy_skills $data_classes[18], lvl

      exp 10 * lvl
      gold 0

      tags :martial, :minion, :range, :solo_ongoiner

      lines_priority 2, 1, 3

      new_feature do
        code xparam
        data acu
        value 0.95
      end
    end
  end
end