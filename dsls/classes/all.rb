ClassDsl.define do
  #Defender
  klass 12 do
    tags :tank, :melee
  end

  #Rogue
  klass 13 do
    tags :solo_burster, :solo_ongoiner, :melee, :stealth
  end

  #Shooter
  klass 14 do
    tags :solo_burster, :solo_ongoiner, :range
  end

  #Healer
  klass 15 do
    tags :healer, :range
  end

  #Mage
  klass 16 do
    tags :range, :area_ongoiner, :area_burster
  end
end
