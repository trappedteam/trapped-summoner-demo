#encoding=utf-8

SkillDsl.define 'archer' do
  skill 'Выстрел' do
    tp_gain 5
    id 147
    description 'Базовый выстрел'
    shared 'shot' do
      icon_index 133
      message 'shoots'
      hit_type physical
      shared 'range'
      scope enemy
      required_weapon bow
      required_weapon gun
      damage do
        critical true
        type to_hp
        element common
        formula 'a.atk * 2 + a.agi * 2 - b.def * 2'
      end
      tag :solo
      pose :attack
    end
    tags :tp_gain, :low_damage, :attack_skill
  end

  skill 'Прицельный' do
    description 'Прицельный выстрел. Наносит средний урон.'
    message 'shoots carefully'
    tp_cost 0
    id 153
    tags :normal_damage, :free
    shared 'shot'
    damage do
      formula 'a.atk * 2 + a.agi * 2 - b.def'
    end
  end

  skill 'Бронебой' do
    message 'shoots through amror'
    description 'Бронебойный выстрел. Наносит существенный урон.'
    id 149
    tp_cost 10
    shared 'shot'
    damage do
      formula 'a.atk * 2 + a.agi * 2'
    end
    tag :high_damage
    pose :skill
  end

  skill 'Blinding Shot' do
    message 'blinds with arrow'
    id 150
    tp_cost 20
    shared 'shot'
    effect do
      effect add_state
      data_id 3
      value1 1
    end
    tags :low_damage, :controll
  end

  skill 'Пронзатель' do
    message 'pierce with arrow'
    description <<-TEXT
Пробивает цель насквозь и поражает цель,
стоящую позади основной цели.
    TEXT
    shared 'shot'
    tp_cost 10
    id 151
    damage do
      formula 'a.atk * 2 + a.agi * 3 - b.def * 2'
    end
    tags :normal_damage, :piercing
    remove_tag :solo
    pose :skill
  end

  skill 'Burst Shot' do
    message 'burst with arrow'
    shared 'shot'
    tp_cost 10
    id 152
    scope all_enemies
    damage do
      formula 'a.atk + a.agi - b.def'
    end
    tags :low_damage, :area
    remove_tag :solo
    pose :skill
  end
end
