#encoding=utf-8
SkillDsl.define 'warrior' do
  skill 'Зрение' do
    shared 'perception'
    id 133
  end

  skill 'Защитный удар' do
    message 'slams with shield'
    description "Атака и защита однавременно."
    speed 50
    tp_cost 0
    id 127

    shared 'shield strike ' do
      icon_index 127
      scope enemy
      shared 'melee'
      required_armor small_shield
      required_armor big_shield
      damage do
        type to_hp
        element common
        formula 'a.atk * 3 - b.def * 2'
        critical true
      end
      tags :low_damage, :solo
      pose :attack
    end

    shared 'protective effect' do
      effect do
        effect add_state
        data_id 9
        value1 1
        direct false
      end
      tags :protective, :short_time, :self
      pose :guard
    end
  end

  skill 'Оглушающий удар' do
    message 'bash with shield '
    description 'Наносит небольшой урон и оглушает врага'
    tp_cost 10
    id 128
    shared 'shield strike'
    shared 'stun effect'
    tag :controll
    pose :attack
  end

  skill 'Warrior Stump' do
    message 'stumps!'
    tp_cost 20
    id 129
    scope all_enemies
    damage do
      type to_hp
      formula 'a.atk - 0.5 * b.def'
    end
    shared 'stun effect'
    tags :controll, :area
    pose :skill
  end

  skill 'Второе дыхание' do
    message 'became stronger '
    description <<-TEXT
Позволяет увеличить запас здоровья и
востановить некоторую его часть
    TEXT
    tp_cost 20
    id 130
    icon_index 32
    scope me

    damage do
      type heal_hp
      formula "a.param_base(#{mhp}) * 0.25"
    end

    effect do
      effect add_buff
      value1 4
      data mhp
    end
    tags :protective, :self, :extrimal
    pose :skill
  end

  skill 'Забота' do
    message 'protects ally '
    description <<-TEXT
Переносит треть урона, направленного на союзников,
на применившего этот навык.
    TEXT
    tp_cost 0
    id 131
    scope all_allies
    speed 50
    icon_index 13

    animation :target, 121
    animation :caster, 122

    effect do
      effect add_state
      value1 1
      data RPG::State.watched.id
    end

    tags :protective, :allies, :long_time
    pose :skill
  end

  skill 'Блокирование' do
    id 132
    message 'raise shield '
    description '10% шанс заблокировать любой урон'
    icon_index 160
    tp_cost 0
    tp_gain 4
    scope me
    speed 100
    effect do
      effect add_state
      value1 1
      data RPG::State.shield_block.id
    end
    tags :protective, :self, :long_time
    pose :skill
  end
end
