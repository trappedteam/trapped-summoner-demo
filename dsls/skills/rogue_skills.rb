#encoding=utf-8
SkillDsl.define 'rogue' do
  skill 'Зрение' do
    shared 'perception'
    energy_cost 10
    id 143
  end

  skill 'Стелс' do    
    description <<-TEXT
Скрывает вас в тени. Скрытую цель невозможно
атаковать. Полученный урон прерывает эффект.
    TEXT
    scope my_self
    id 137
    speed 100
    icon_index 12
    shared 'transparent effect' do
      effect do
        effect add_state
        data_id RPG::State.transparent.id
        value1 1
      end
      tag :stealth
    end
    tag :no_damage
    pose :skill
  end

  skill 'Вендета' do
    description 'Средний урон, средняя цена'
    message 'Hits Carefully'
    shared 'melee'
    id 142
    shared 'rogue hit with damage' do
      icon_index 131
      hit_type physical
      energy_cost Energy::Patch::RechargeRate::DEFAULT_RATE
      scope enemy
      required_weapon dagger
      damage do
        critical true
        type to_hp
        formula 'a.atk  + a.luk * 2'
      end
    end
    pose :attack
    tags :normal_damage, :free
  end

  skill 'Поцелуй аспида' do
    message 'kiss with poison'
    description 'Отравляет цель.'
    id 138
    damage do
      type debuff
      element none
    end
    energy_cost 5
    hit_type certain
    shared 'aspid kiss' do
      shared 'melee'
      scope enemy
      effect do
        effect add_state
        data_id 2
        value1 1
      end
      tag :poison
    end
    pose :attack
    tag :no_damage
  end

  skill 'Удар в спину' do
    id 139
    description 'Удар в спину, наносящй огромный урон.'
    shared 'backstab' do
      shared 'rogue hit with damage'
      message 'attacks behind'
      energy_cost 30
      damage do
        formula 'a.atk * 2 + a.luk * 3'
      end
      tag :high_damage
    end
    pose :attack
  end

  skill 'Шаг в тень' do
    shared 'backstab'
    description 'Удар в спину, который прячет вас в тень.'
    id 140
    energy_cost 35
    shared 'transparent effect'
    expand_effect do
      direct false
    end
    pose :skill
  end

  skill 'Отравленный удар' do
    shared 'aspid kiss'
    shared 'backstab'
    description 'Удар в спину, отравляющий цель'
    id 141
    energy_cost 35
    pose :attack
  end
end
