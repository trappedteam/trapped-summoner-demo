#encoding=utf-8
SkillDsl.prepare do
  #speed
  #success_rate
  #repeats
  #variance

  #effects
  #code
  #data_id
  #value1
  #value2
  all do
    occasion only_in_battle
    damage do
      variance 20
    end
  end

  shared 'range' do
    tag :range
    remove_tag :melee
  end

  shared 'melee' do
    tag :melee
    remove_tag :range
    hit_type physical
  end

  shared 'magical' do
    stype_id magic
    hit_type magical
  end

  shared 'stun effect' do
    effect do
      effect add_state
      data_id 8
      value1 0.66
    end
  end

  shared 'perception' do
    description 'Обнаруживает скрытые цели'
    icon_index 121
    scope all_enemies

    tag :perception
    animation :caster, 122

    effect do
      effect remove_state
      data_id RPG::State.transparent.id
      value1 0.5
    end

    effect do
      effect add_state 
      data_id RPG::State.observable.id 
      value1 0.5
    end
  end

  #skill 'Strike' do
  #  shared 'melee'
  #  scope enemy
  #  tp_gain 5
  #  required_wtype_id1 hammer
  #  required_wtype_id2 gun
  #  damage do
  #    type to_hp
  #    element common
  #    formula 'a.atk * 4 - b.def * 2'
  #  end
  #end

  #skill 'Inferno' do
  #  shared 'range'
  #  shared 'magical'
  #  scope all_enemies
  #  mp_cost 20
  #  damage do
  #    type to_hp
  #    element fire
  #    formula 'a.mat * 3 - b.def * 2 + 100'
  #    critical true
  #  end
  #end
end
