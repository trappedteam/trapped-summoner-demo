#encoding=utf-8

SkillDsl.define 'mage' do
  skill 'Водяная бомба' do
    message 'throws water bomb'
    id 167
    icon_index 99
    description 'Наносит средний урон по площади.'

    shared 'mage skill' do
      animation :caster, 112
      mp_cost 12
      icon_index 97
      shared 'range'
      shared 'magical'
      scope all_enemies

      damage do
        type to_hp
        element ice
        formula '4 * a.mat - 1.5 * b.mdf'
        critical true
      end

      tag :area
    end
    animation :caster, 113
    animation :target, 116

    tag :normal_damage

    damage do
      element water
    end
  end

  skill 'Метель' do
    message 'calls for blizzard'
    id 168
    icon_index 3
    description 'Наносит слабый урон по площади, ослепляет цели.'

    shared 'mage skill'
    shared 'mage controll' do
      damage do
        formula '3 * a.mat - b.mdf * 2'
      end
      tags :low_damage, :controll
    end

    effect do
      effect add_state
      data_id 3
      value1 0.5
    end

    pose :skill

    animation :target, 117
  end

  skill 'Ледяная бомба' do
    message 'throws ice bomb'
    description 'Наносит существенный урон по площади.'
    animation :target, 118
    id 169

    shared 'mage skill'
    mp_cost 60
    damage do
      formula '6 * a.mat - b.mdf'
    end
    pose :skill
    tag :high_damage
  end

  skill 'Град' do
    message 'attacks with heavy ice'
    id 170    
    animation :target, 119
    description 'Наносит слабый урон по площади, оглушает цели.'

    shared 'mage skill'
    shared 'stun effect'
    shared 'mage controll'
    expand_effect do
      value1 0.33
    end
    pose :skill

    mp_cost 18
  end
end
