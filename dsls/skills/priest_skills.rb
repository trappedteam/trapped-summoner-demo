#encoding=utf-8
SkillDsl.define 'priest' do

  skill 'Регенерация' do
    message 'call for regeneration power'
    description "Повышает скорость регенерации здоровья у союзника."
    mp_cost 12
    id 158
    shared 'regeneration' do
      icon_index 112
      shared 'range'
      scope ally
      stype magic
      animation :caster, 114
      animation :target, 120
      effect do
        effect add_state
        data_id 14
        value1 1
      end
      tag :hot
    end
    tag :solo
  end

  skill 'Общая регенерация' do
    description <<-TEXT
Повышает скорость регенерация здоровья у всех
союзников.
    TEXT
    id 162
    mp_cost 24
    shared 'regeneration'
    scope all_allies
    tag :area
  end

  skill 'Исцеление' do
    description 'Исцеляет союзника.'
    mp_cost 20
    id 157
    shared 'heal' do
      icon_index 112
      message 'healing'
      shared 'range'
      scope ally
      stype magic
      damage do
        type heal_hp
        formula 'b.mhp / 2.5'
      end
      tag :heal

      animation :target, 115
      animation :caster, 114
    end
    tag :solo
  end

  skill 'Общее исцеление' do
    description 'Исцеляет всех союзников.'
    id 160
    mp_cost 60
    shared 'group heal' do
      shared 'heal'
      scope all_allies
      damage do
        formula 'b.mhp / 2.5'
      end
    end
    tag :area
  end

  skill 'Очищение' do
    description 'Накладывает эффект регенерации и исцеляет союзника'
    id 159
    mp_cost 45
    shared 'regeneration'
    shared 'heal'
    tag :solo
  end

  skill 'Общее очищение' do
    description 'Накладывает эффект регенерации и исцеляет всех союзников'
    id 161
    mp_cost 100
    shared 'regeneration'
    shared 'group heal'
    tag :area
  end
end
