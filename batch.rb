#gems/aasm/lib/aasm.rb
#gems/aasm/lib/aasm/version.rb
module AASM
  VERSION = "4.0.7"
end
#gems/aasm/lib/aasm/errors.rb
module AASM
  class InvalidTransition < RuntimeError; end
  class UndefinedState < RuntimeError; end
  class NoDirectAssignmentError < RuntimeError; end
end
#gems/aasm/lib/aasm/configuration.rb
module AASM
  class Configuration
    # for all persistence layers: which database column to use?
    attr_accessor :column

    # let's cry if the transition is invalid
    attr_accessor :whiny_transitions

    # for all persistence layers: create named scopes for each state
    attr_accessor :create_scopes

    # for ActiveRecord: don't store any new state if the model is invalid
    attr_accessor :skip_validation_on_save

    # for ActiveRecord: use requires_new for nested transactions?
    attr_accessor :requires_new_transaction

    # forbid direct assignment in aasm_state column (in ActiveRecord)
    attr_accessor :no_direct_assignment

    attr_accessor :enum
  end
end
#gems/aasm/lib/aasm/base.rb
module AASM
  class Base

    attr_reader :state_machine

    def initialize(klass, options={}, &block)
      @klass = klass
      @state_machine = AASM::StateMachine[@klass]
      @state_machine.config.column ||= (options[:column] || :aasm_state).to_sym # aasm4
      # @state_machine.config.column = options[:column].to_sym if options[:column] # master
      @options = options

      # let's cry if the transition is invalid
      configure :whiny_transitions, true

      # create named scopes for each state
      configure :create_scopes, true

      # don't store any new state if the model is invalid (in ActiveRecord)
      configure :skip_validation_on_save, false

      # use requires_new for nested transactions (in ActiveRecord)
      configure :requires_new_transaction, true

      # set to true to forbid direct assignment of aasm_state column (in ActiveRecord)
      configure :no_direct_assignment, false

      configure :enum, nil

      if @state_machine.config.no_direct_assignment
        @klass.send(:define_method, "#{@state_machine.config.column}=") do |state_name|
          raise AASM::NoDirectAssignmentError.new('direct assignment of AASM column has been disabled (see AASM configuration for this class)')
        end
      end
    end

    # This method is both a getter and a setter
    def attribute_name(column_name=nil)
      if column_name
        @state_machine.config.column = column_name.to_sym
      else
        @state_machine.config.column ||= :aasm_state
      end
      @state_machine.config.column
    end

    def initial_state(new_initial_state=nil)
      if new_initial_state
        @state_machine.initial_state = new_initial_state
      else
        @state_machine.initial_state
      end
    end

    # define a state
    def state(name, options={})
      @state_machine.add_state(name, @klass, options)

      @klass.send(:define_method, "#{name.to_s}?") do
        aasm.current_state == name
      end

      unless @klass.const_defined?("STATE_#{name.to_s.upcase}")
        @klass.const_set("STATE_#{name.to_s.upcase}", name)
      end
    end

    # define an event
    def event(name, options={}, &block)
      @state_machine.events[name] = AASM::Core::Event.new(name, options, &block)

      # an addition over standard aasm so that, before firing an event, you can ask
      # may_event? and get back a boolean that tells you whether the guard method
      # on the transition will let this happen.
      @klass.send(:define_method, "may_#{name.to_s}?") do |*args|
        aasm.may_fire_event?(name, *args)
      end

      @klass.send(:define_method, "#{name.to_s}!") do |*args, &block|
        aasm.current_event = "#{name.to_s}!".to_sym
        aasm_fire_event(name, {:persist => true}, *args, &block)
      end

      @klass.send(:define_method, "#{name.to_s}") do |*args, &block|
        aasm.current_event = name.to_sym
        aasm_fire_event(name, {:persist => false}, *args, &block)
      end
    end

    def states
      @state_machine.states
    end

    def events
      @state_machine.events.values
    end

    # aasm.event(:event_name).human?
    def human_event_name(event) # event_name?
      AASM::Localizer.new.human_event_name(@klass, event)
    end

    def states_for_select
      states.map { |state| state.for_select }
    end

    def from_states_for_state(state, options={})
      if options[:transition]
        @state_machine.events[options[:transition]].transitions_to_state(state).flatten.map(&:from).flatten
      else
        events.map {|e| e.transitions_to_state(state)}.flatten.map(&:from).flatten
      end
    end

    private

    def configure(key, default_value)
      if @options.key?(key)
        @state_machine.config.send("#{key}=", @options[key])
      elsif @state_machine.config.send(key).nil?
        @state_machine.config.send("#{key}=", default_value)
      end
    end

  end
end
#gems/aasm/lib/aasm/dsl_helper.rb
module DslHelper

  class Proxy
    attr_accessor :options

    def initialize(options, valid_keys, source)
      @valid_keys = valid_keys
      @source = source

      @options = options
    end

    def method_missing(name, *args, &block)
      if @valid_keys.include?(name)
        options[name] = Array(options[name])
        options[name] << block if block
        options[name] += Array(args)
      else
        @source.send name, *args, &block
      end
    end
  end

  def add_options_from_dsl(options, valid_keys, &block)
    proxy = Proxy.new(options, valid_keys, self)
    proxy.instance_eval(&block)
    proxy.options
  end

end
#gems/aasm/lib/aasm/instance_base.rb
module AASM
  class InstanceBase

    attr_accessor :from_state, :to_state, :current_event

    def initialize(instance)
      @instance = instance
    end

    def current_state
      @instance.aasm_read_state
    end

    def current_state=(state)
      @instance.aasm_write_state_without_persistence(state)
      @current_state = state
    end

    def enter_initial_state
      state_name = determine_state_name(@instance.class.aasm.initial_state)
      state_object = state_object_for_name(state_name)

      state_object.fire_callbacks(:before_enter, @instance)
      # state_object.fire_callbacks(:enter, @instance)
      self.current_state = state_name
      state_object.fire_callbacks(:after_enter, @instance)

      state_name
    end

    def human_state
      AASM::Localizer.new.human_state_name(@instance.class, current_state)
    end

    def states(options={})
      if options[:permitted]
        # ugliness level 1000
        permitted_event_names = events(:permitted => true).map(&:name)
        transitions = @instance.class.aasm.state_machine.events.values_at(*permitted_event_names).compact.map {|e| e.transitions_from_state(current_state) }
        tos = transitions.map {|t| t[0] ? t[0].to : nil}.flatten.compact.map(&:to_sym).uniq
        @instance.class.aasm.states.select {|s| tos.include?(s.name.to_sym)}
      else
        @instance.class.aasm.states
      end
    end

    def events(options={})
      state = options[:state] || current_state
      events = @instance.class.aasm.events.select {|e| e.transitions_from_state?(state) }

      if options[:permitted]
        # filters the results of events_for_current_state so that only those that
        # are really currently possible (given transition guards) are shown.
        events.select! { |e| @instance.send("may_#{e.name}?") }
      end

      events
    end

    def state_object_for_name(name)
      obj = @instance.class.aasm.states.find {|s| s == name}
      raise AASM::UndefinedState, "State :#{name} doesn't exist" if obj.nil?
      obj
    end

    def determine_state_name(state)
      case state
        when Symbol, String
          state
        when Proc
          state.call(@instance)
        else
          raise NotImplementedError, "Unrecognized state-type given.  Expected Symbol, String, or Proc."
      end
    end

    def may_fire_event?(name, *args)
      if event = @instance.class.aasm.state_machine.events[name]
        event.may_fire?(@instance, *args)
      else
        false # unknown event
      end
    end

    def set_current_state_with_persistence(state)
      save_success = @instance.aasm_write_state(state)
      self.current_state = state if save_success
      save_success
    end

  end
end
#gems/aasm/lib/aasm/core/transition.rb
module AASM::Core
  class Transition
    include DslHelper

    attr_reader :from, :to, :opts
    alias_method :options, :opts

    def initialize(opts, &block)
      add_options_from_dsl(opts, [:on_transition, :guard, :after], &block) if block

      @from = opts[:from]
      @to = opts[:to]
      @guards = Array(opts[:guards]) + Array(opts[:guard]) + Array(opts[:if])
      @unless = Array(opts[:unless]) #TODO: This could use a better name

      if opts[:on_transition]
        warn '[DEPRECATION] :on_transition is deprecated, use :after instead'
        opts[:after] = Array(opts[:after]) + Array(opts[:on_transition])
      end
      @after = Array(opts[:after])
      @after = @after[0] if @after.size == 1

      @opts = opts
    end

    def allowed?(obj, *args)
      invoke_callbacks_compatible_with_guard(@guards, obj, args, :guard => true) &&
      invoke_callbacks_compatible_with_guard(@unless, obj, args, :unless => true)
    end

    def execute(obj, *args)
      invoke_callbacks_compatible_with_guard(@after, obj, args)
    end

    def ==(obj)
      @from == obj.from && @to == obj.to
    end

    def from?(value)
      @from == value
    end

    private

    def invoke_callbacks_compatible_with_guard(code, record, args, options={})
      if record.respond_to?(:aasm)
        record.aasm.from_state = @from if record.aasm.respond_to?(:from_state=)
        record.aasm.to_state = @to if record.aasm.respond_to?(:to_state=)
      end

      case code
      when Symbol, String
        arity = record.send(:method, code.to_sym).arity
        arity == 0 ? record.send(code) : record.send(code, *args)
      when Proc
        code.arity == 0 ? record.instance_exec(&code) : record.instance_exec(*args, &code)
      when Array
        if options[:guard]
          # invoke guard callbacks
          code.all? {|a| invoke_callbacks_compatible_with_guard(a, record, args)}
        elsif options[:unless]
          # invoke unless callbacks
          code.all? {|a| !invoke_callbacks_compatible_with_guard(a, record, args)}
        else
          # invoke after callbacks
          code.map {|a| invoke_callbacks_compatible_with_guard(a, record, args)}
        end
      else
        true
      end
    end

  end
end # AASM
#gems/aasm/lib/aasm/core/event.rb
module AASM::Core
  class Event
    include DslHelper

    attr_reader :name, :options

    def initialize(name, options = {}, &block)
      @name = name
      @transitions = []
      @guards = Array(options[:guard] || options[:guards] || options[:if])
      @unless = Array(options[:unless]) #TODO: This could use a better name

      # from aasm4
      @options = options # QUESTION: .dup ?
      add_options_from_dsl(@options, [:after, :before, :error, :success], &block) if block
    end

    # a neutered version of fire - it doesn't actually fire the event, it just
    # executes the transition guards to determine if a transition is even
    # an option given current conditions.
    def may_fire?(obj, to_state=nil, *args)
      _fire(obj, {:test_only => true}, to_state, *args) # true indicates test firing
    end

    def fire(obj, options={}, to_state=nil, *args)
      _fire(obj, options, to_state, *args) # false indicates this is not a test (fire!)
    end

    def transitions_from_state?(state)
      transitions_from_state(state).any?
    end

    def transitions_from_state(state)
      @transitions.select { |t| t.from.nil? or t.from == state }
    end

    def transitions_to_state?(state)
      transitions_to_state(state).any?
    end

    def transitions_to_state(state)
      @transitions.select { |t| t.to == state }
    end

    def fire_callbacks(callback_name, record, *args)
      # strip out the first element in args if it's a valid to_state
      # #given where we're coming from, this condition implies args not empty
      invoke_callbacks(@options[callback_name], record, args)
    end

    def ==(event)
      if event.is_a? Symbol
        name == event
      else
        name == event.name
      end
    end

    ## DSL interface
    def transitions(definitions=nil, &block)
      if definitions # define new transitions
        # Create a separate transition for each from-state to the given state
        Array(definitions[:from]).each do |s|
          @transitions << AASM::Core::Transition.new(attach_event_guards(definitions.merge(:from => s.to_sym)), &block)
        end
        # Create a transition if :to is specified without :from (transitions from ANY state)
        if @transitions.empty? && definitions[:to]
          @transitions << AASM::Core::Transition.new(attach_event_guards(definitions), &block)
        end
      end
      @transitions
    end

  private

    def attach_event_guards(definitions)
      unless @guards.empty?
        given_guards = Array(definitions.delete(:guard) || definitions.delete(:guards) || definitions.delete(:if))
        definitions[:guards] = @guards + given_guards # from aasm4
      end
      unless @unless.empty?
        given_unless = Array(definitions.delete(:unless))
        definitions[:unless] = given_unless + @unless
      end
      definitions
    end

    # Execute if test == false, otherwise return true/false depending on whether it would fire
    def _fire(obj, options={}, to_state=nil, *args)
      result = options[:test_only] ? false : nil
      if @transitions.map(&:from).any?
        transitions = @transitions.select { |t| t.from == obj.aasm.current_state }
        return result if transitions.size == 0
      else
        transitions = @transitions
      end

      # If to_state is not nil it either contains a potential
      # to_state or an arg
      unless to_state == nil
        if !to_state.respond_to?(:to_sym) || !transitions.map(&:to).flatten.include?(to_state.to_sym)
          args.unshift(to_state)
          to_state = nil
        end
      end

      transitions.each do |transition|
        next if to_state and !Array(transition.to).include?(to_state)
        if (options.key?(:may_fire) && Array(transition.to).include?(options[:may_fire])) ||
           (!options.key?(:may_fire) && transition.allowed?(obj, *args))
          result = to_state || Array(transition.to).first
          if options[:test_only]
            # result = true
          else
            transition.execute(obj, *args)
          end

          break
        end
      end
      result
    end

    def invoke_callbacks(code, record, args)
      case code
        when Symbol, String
          unless record.respond_to?(code, true)
            raise NoMethodError.new("NoMethodError: undefined method `#{code}' for #{record.inspect}:#{record.class}")
          end
          arity = record.send(:method, code.to_sym).arity
          record.send(code, *(arity < 0 ? args : args[0...arity]))
          true

        when Proc
          arity = code.arity
          record.instance_exec(*(arity < 0 ? args : args[0...arity]), &code)
          true

        when Array
          code.each {|a| invoke_callbacks(a, record, args)}
          true

        else
          false
      end
    end

  end
end # AASM
#gems/aasm/lib/aasm/core/state.rb
module AASM::Core
  class State
    attr_reader :name, :options

    def initialize(name, klass, options={})
      @name = name
      @klass = klass
      update(options)
    end

    def ==(state)
      if state.is_a? Symbol
        name == state
      else
        name == state.name
      end
    end

    def <=>(state)
      if state.is_a? Symbol
        name <=> state
      else
        name <=> state.name
      end
    end

    def to_s
      name.to_s
    end

    def fire_callbacks(action, record)
      action = @options[action]
      catch :halt_aasm_chain do
        action.is_a?(Array) ?
                action.each {|a| _fire_callbacks(a, record)} :
                _fire_callbacks(action, record)
      end
    end

    def display_name
      @display_name ||= begin
        if Module.const_defined?(:I18n)
          localized_name
        else
          name.to_s.gsub(/_/, ' ').capitalize
        end
      end
    end

    def localized_name
      AASM::Localizer.new.human_state_name(@klass, self)
    end
    alias human_name localized_name

    def for_select
      [display_name, name.to_s]
    end

  private

    def update(options = {})
      if options.key?(:display) then
        @display_name = options.delete(:display)
      end
      @options = options
      self
    end

    def _fire_callbacks(action, record)
      case action
        when Symbol, String
          record.send(action)
        when Proc
          action.call(record)
      end
    end

  end
end # AASM
#gems/aasm/lib/aasm/localizer.rb
module AASM
  class Localizer
    def human_event_name(klass, event)
      checklist = ancestors_list(klass).inject([]) do |list, ancestor|
        list << :"#{i18n_scope(klass)}.events.#{i18n_klass(ancestor)}.#{event}"
        list
      end
      translate_queue(checklist) || I18n.translate(checklist.shift, :default => event.to_s.humanize)
    end

    def human_state_name(klass, state)
      checklist = ancestors_list(klass).inject([]) do |list, ancestor|
        list << item_for(klass, state, ancestor)
        list << item_for(klass, state, ancestor, :old_style => true)
        list
      end
      translate_queue(checklist) || I18n.translate(checklist.shift, :default => state.to_s.humanize)
    end

  private

    def item_for(klass, state, ancestor, options={})
      separator = options[:old_style] ? '.' : '/'
      :"#{i18n_scope(klass)}.attributes.#{i18n_klass(ancestor)}.#{klass.aasm.attribute_name}#{separator}#{state}"
    end

    def translate_queue(checklist)
      (0...(checklist.size-1)).each do |i|
        begin
          return I18n.translate(checklist.shift, :raise => true)
        rescue I18n::MissingTranslationData
          # that's okay
        end
      end
      nil
    end

    # added for rails 2.x compatibility
    def i18n_scope(klass)
      klass.respond_to?(:i18n_scope) ? klass.i18n_scope : :activerecord
    end

    # added for rails < 3.0.3 compatibility
    def i18n_klass(klass)
      klass.model_name.respond_to?(:i18n_key) ? klass.model_name.i18n_key : klass.name.underscore
    end

    def ancestors_list(klass)
      klass.ancestors.select do |ancestor|
        ancestor.respond_to?(:model_name) unless ancestor.name == 'ActiveRecord::Base'
      end
    end
  end
end # AASM
#gems/aasm/lib/aasm/state_machine.rb
module AASM
  class StateMachine

    # the following two methods provide the storage of all state machines
    def self.[](klass)
      (@machines ||= {})[klass.to_s]
    end

    def self.[]=(klass, machine)
      (@machines ||= {})[klass.to_s] = machine
    end

    attr_accessor :states, :events, :initial_state, :config

    def initialize
      @initial_state = nil
      @states = []
      @events = {}
      @config = AASM::Configuration.new
    end

    # called internally by Ruby 1.9 after clone()
    def initialize_copy(orig)
      super
      @states = @states.dup
      @events = @events.dup
    end

    def add_state(name, klass, options)
      set_initial_state(name, options)

      # allow reloading, extending or redefining a state
      @states.delete(name) if @states.include?(name)

      @states << AASM::Core::State.new(name, klass, options)
    end

    private

    def set_initial_state(name, options)
      @initial_state = name if options[:initial] || !initial_state
    end

  end # StateMachine
end # AASM
#gems/aasm/lib/aasm/persistence.rb
module AASM
  module Persistence
    class << self

      def load_persistence(base)
        include_persistence base, :plain
      end

      private

      def include_persistence(base, type)
        base.send(:include, constantize("AASM::Persistence::#{capitalize(type)}Persistence"))
      end

      def capitalize(string_or_symbol)
        string_or_symbol.to_s.split('_').map {|segment| segment[0].upcase + segment[1..-1]}.join('')
      end

      def constantize(string)
        instance_eval(string)
      end

    end # class << self
  end
end # AASM
#gems/aasm/lib/aasm/aasm.rb
module AASM

  # provide a state machine for the including class
  # make sure to load class methods as well
  # initialize persistence for the state machine
  def self.included(base) #:nodoc:
    base.extend AASM::ClassMethods

    # do not overwrite existing state machines, which could have been created by
    # inheritance, see class method inherited
    AASM::StateMachine[base] ||= AASM::StateMachine.new

    AASM::Persistence.load_persistence(base)
    super
  end

  module ClassMethods

    # make sure inheritance (aka subclassing) works with AASM
    def inherited(base)
      AASM::StateMachine[base] = AASM::StateMachine[self].clone
      super
    end

    # this is the entry point for all state and event definitions
    def aasm(options={}, &block)
      @aasm ||= AASM::Base.new(self, options)
      @aasm.instance_eval(&block) if block # new DSL
      @aasm
    end

    # deprecated, remove in version 4.1
    def aasm_human_event_name(event) # event_name?
      warn '[DEPRECATION] AASM: aasm_human_event_name is deprecated, use aasm.human_event_name instead'
      aasm.human_event_name(event)
    end
  end # ClassMethods

  def aasm
    @aasm ||= AASM::InstanceBase.new(self)
  end

private

  # Takes args and a from state and removes the first
  # element from args if it is a valid to_state for
  # the event given the from_state
  def process_args(event, from_state, *args)
    # If the first arg doesn't respond to to_sym then
    # it isn't a symbol or string so it can't be a state
    # name anyway
    return args unless args.first.respond_to?(:to_sym)
    if event.transitions_from_state(from_state).map(&:to).flatten.include?(args.first)
      return args[1..-1]
    end
    return args
  end

  def aasm_fire_event(event_name, options, *args, &block)
    event = self.class.aasm.state_machine.events[event_name]
    begin
      old_state = aasm.state_object_for_name(aasm.current_state)

      # new event before callback
      event.fire_callbacks(
        :before,
        self,
        *process_args(event, aasm.current_state, *args)
      )

      if may_fire_to = event.may_fire?(self, *args)
        old_state.fire_callbacks(:before_exit, self)
        old_state.fire_callbacks(:exit, self) # TODO: remove for AASM 4?

        if new_state_name = event.fire(self, {:may_fire => may_fire_to}, *args)
          aasm_fired(event, old_state, new_state_name, options, *args, &block)
        else
          aasm_failed(event_name, old_state)
        end
      else
        aasm_failed(event_name, old_state)
      end
    rescue StandardError => e
      event.fire_callbacks(:error, self, e, *process_args(event, aasm.current_state, *args)) || raise(e)
    end
  end

  def aasm_fired(event, old_state, new_state_name, options, *args)
    persist = options[:persist]

    new_state = aasm.state_object_for_name(new_state_name)

    new_state.fire_callbacks(:before_enter, self)

    new_state.fire_callbacks(:enter, self) # TODO: remove for AASM 4?

    persist_successful = true
    if persist
      persist_successful = aasm.set_current_state_with_persistence(new_state_name)
      if persist_successful
        yield if block_given?
        event.fire_callbacks(:success, self)
      end
    else
      aasm.current_state = new_state_name
      yield if block_given?
    end

    if persist_successful
      old_state.fire_callbacks(:after_exit, self)
      new_state.fire_callbacks(:after_enter, self)
      event.fire_callbacks(
        :after,
        self,
        *process_args(event, old_state.name, *args)
      )

      self.aasm_event_fired(event.name, old_state.name, aasm.current_state) if self.respond_to?(:aasm_event_fired)
    else
      self.aasm_event_failed(event.name, old_state.name) if self.respond_to?(:aasm_event_failed)
    end

    persist_successful
  end

  def aasm_failed(event_name, old_state)
    if self.respond_to?(:aasm_event_failed)
      self.aasm_event_failed(event_name, old_state.name)
    end

    if AASM::StateMachine[self.class].config.whiny_transitions
      raise AASM::InvalidTransition, "Event '#{event_name}' cannot transition from '#{aasm.current_state}'"
    else
      false
    end
  end

end
#gems/aasm/lib/aasm/persistence/plain_persistence.rb
module AASM
  module Persistence
    module PlainPersistence

      def aasm_read_state
        # all the following lines behave like @current_state ||= aasm.enter_initial_state
        current = aasm.instance_variable_get("@current_state")
        return current if current
        aasm.instance_variable_set("@current_state", aasm.enter_initial_state)
      end

      # may be overwritten by persistence mixins
      def aasm_write_state(new_state)
        true
      end

      # may be overwritten by persistence mixins
      def aasm_write_state_without_persistence(new_state)
        true
      end

    end
  end
end
#gems/debugger/lib/debugger.rb
class Debugger
  VERSION = '0.0.2'
  WORDS = {
    hello: "debug console activated from %s, version: #{VERSION}",
    bye:   "good bye"
  }
  TRIGGER = Input::F5
  WIN  = {
    focus:     Win32API.new('user32', 'BringWindowToTop', 'I', 'I'),
    find:      Win32API.new('user32', 'FindWindow', 'PP', 'I'),
    get_title: Win32API.new('kernel32', 'GetConsoleTitle', 'PI', 'I'),
  }
  PROMPTS = {
    result:   "=> ",
    enter:    "> ",
    continue: "* "
  }
  SIGNALS = {
    close: 'exit',
    clear: 'clear_eval'
  }
  CLOSE_SIGNAL = 'exit'

  class << self
    def render(binding, render_constants = false)
      Renderer.render binding, render_constants
    end

    #Loads Console
    def load_console(binding = Object.__send__(:binding))
      say_hello binding
      focus Console.window #focus on debug console window
      Console.run(binding) #run with binding
    end

    #methods checks if user input is a signal
    def handle_signal(signal)
      case signal.chop     #remove new line in the end
      when SIGNALS[:close] #when user going to close the console
        close_console
      when SIGNALS[:clear] #when user going to clear eval stack
        Console.clear_eval
        :continue
      end
    end

    private

    def say_hello(binding) #greeting words
      klass, separator = binding.eval "is_a?(Class) ? [self, '.'] : [self.class, '#']"
      method_name      = binding.eval '__method__'
      puts WORDS[:hello] % "#{klass.name}#{separator}#{method_name}"
    end

    #closes console
    def close_console
      puts WORDS[:bye]        #say good bye
      focus GameWindow.window #focus the game window
      Console.close
      sleep 1                 #hack, prevent enter in the game window
      raise StopIteration     #stops loop
    end

    #we have two winows - console and game window
    #method to focuse one of them
    def focus(window)
      WIN[:focus].call window
    end
  end
end


#gems/debugger/lib/debugger/console.rb
class Debugger
  class Console
    class << self
      #runs console
      def run(binding)
        @current_instance = new binding #initialize new instance and store it
        @current_instance.run           #run new console instance
      end

      #returns console window via win32 api
      def window
        WIN[:find].call 'ConsoleWindowClass', title
      end

      #clears eval stack
      def clear_eval
        @current_instance.clear_eval
      end

      def close
        @current_instance = nil
      end

      private

      #returns title of the console window
      def title
        ("\0" * 256).tap do |buffer|
          WIN[:get_title].call(buffer, buffer.length - 1)
        end.gsub "\0", ''
      end
    end

    def initialize(binding)
      @binding = binding #store binding
      clear_eval         #clear eval stack (set it to empty string)
    end

    #sets eval stack to empty string
    def clear_eval
      @to_eval = ''
    end

    #eval loop
    def run
      loop do
        prompt #prints prompt to enter command
        gets.tap do |code| #gets - returns user's input
          evaluate code unless code.nil? || Debugger.handle_signal(code) == :continue #evaluate code
        end
      end
    end

    private

    #prints prompt
    def prompt
      if @to_eval != ''
        Debugger::PROMPTS[:continue] #when eval stack is not empty
      else
        Debugger::PROMPTS[:enter]    #when eval stack is empty
      end.tap { |string| print string }
    end

    #evals code
    def evaluate(code)
      @to_eval << code #add code to stack
      result(eval @to_eval, @binding) #evals code
    rescue SyntaxError #when sytax error happens do nothing (do not clear stack)
    rescue Exception => e #return error to the console
      puts e.message
      clear_eval
    end

    #clears eval stack and prints result
    def result(res)
      clear_eval
      puts Debugger::PROMPTS[:result] + res.to_s
    end
  end
end
#gems/debugger/lib/debugger/game_window.rb
class Debugger
  class GameWindow
    class << self
      #returns game window
      def window
        WIN[:find].call 'RGSS Player', game_title
      end

      private

      def game_title
        @game_title ||= load_data('Data/System.rvdata2').game_title
      end
    end
  end
end
#gems/debugger/lib/debugger/renderer.rb
class Debugger
  class Renderer
    FILE_NAME = 'debugger.txt'

    def self.render(binding, include_constants = false)
      new(binding, include_constants).render
    end

    def initialize(binding, include_constants)
      @binding, @include_constants = binding, include_constants
    end

    def render
      in_file do |file|
        keys.each do |key|
          write key, file
        end
      end
    end

    def keys
      arr = %w(local_variables instance_variables)
      @include_constants ? arr + ['self.class.constants'] : arr
    end

    def in_file
      File.open(File.join(Dir.pwd, FILE_NAME), 'a') { |file| yield file }
    end

    def write(key, file)
      @binding.eval(key).map do |element|
        "#{element} => #{@binding.eval(element.to_s)}"
      end.tap do |result|
        file.puts "#{key}:"
        file.puts result
        file.puts
      end
    end
  end
end
#gems/debugger/lib/debugger/patch.rb
module Debugger::Patch
end

#gems/debugger/lib/debugger/patch/binding_patch.rb
class Binding
  def bug
    Debugger.load_console self
  end
end
#gems/debugger/lib/debugger/patch/scene_base_patch.rb
class Scene_Base
  alias_method :original_update_basic_for_debugger, :update_basic

  def update_basic(*args, &block)
    Debugger.load_console if Input.trigger? Debugger::TRIGGER
    original_update_basic_for_debugger *args, &block
  end
end
#gems/messager/lib/messager.rb
#encoding=utf-8
#Popup messages for VX ACE
#author: Iren_Rin
#restrictions of use: none
#how to use:
#1) Look through Messager::Vocab and Messager::Settings
#and change if needed
#2) Unless turned off in Messager::Settings.general
#the script will automatically display gained items and gold on Scene_Map
#and taken damage, states, buffs and etc on Scene_Battle
#for battlers with defined screen_x and screen_y.
#By default only enemies has these coordinates.
#3) You can call message popup manually with following
#a) 
# battler = $game_troop.members[0]
# battler.message_queue.damage_to_hp 250
# battler.message_queue.heal_tp 100
#b)
# $game_player.message_queue.gain_item $data_items[1]
# $game_player.message_queue.gain_armor $data_armors[2]
# $game_player.message_queue.gain_weapon $data_weapons[3]
# $game_player.message_queue.gain_gold 300
#c)
# message = Message::Queue::Message.new :add_state
# state = $data_states[3]
# message.text = state.name
# message.icon_index = state.icon_index
# $game_troop.members.sample.message_queue.push message#encoding=utf-8
module Messager
  VERSION = '0.0.1'

  module Vocab
    CounterAttack = 'Контр.'
    MagicReflection = 'Отраж.'
    Substitute = 'Уст.'
    NoEffect = 'Нет эффекта'
    Miss = 'Промах'
    Evasion = 'Укл.'
    Block = 'Блок'
    Gold = 'Злт.'
  end

  module Settings
    def self.general
      {
        monitor_items: true,
        monitor_gold: true,
        monitor_weapons: true,
        monitor_armors: true,
        in_battle: true,
        allow_collapse_effect: false
      }
    end

    module Popup
      def settings
        {
          battler_offset: -80, #distance between battler screen_y and popup
          character_offset: -50, #distance between character screen_y and popup
          font_size: 24, 
          font_name: 'Arial',
          dead_timeout: 70, #in frames, time to dispose popup
          icon_width: 24, 
          icon_height: 24,

          colors: { #RGB
            damage_to_hp: [255, 255, 0],
            gain_gold: [255, 215, 0],
            damage_to_tp: [255, 0, 0],
            damage_to_mp: [255, 0, 255],
            heal_hp: [0, 255, 0],
            heal_tp: [255, 0, 0],
            heal_mp: [0, 128, 255],
            magic_reflection: [0, 128, 255],
            failure: [255, 0, 0],
            substitute: [50, 50, 50],
            cast: [204, 255, 255],
            evasion: [153, 255, 153],
            gain_item: [0, 128, 255],
            gain_weapon: [0, 128, 128],
            gain_armor:  [34, 139, 34]
          }.tap { |h| h.default = [255, 255, 255] },

          postfixes: {
            damage_to_hp: 'HP', heal_hp: 'HP',
            damage_to_tp: 'TP', heal_tp: 'TP',
            damage_to_mp: 'MP', heal_mp: 'MP',
          }.tap { |h| h.default = '' }
        }
      end
    end
  end
end

#gems/messager/lib/messager/concerns.rb
module Messager::Concerns
end

#gems/messager/lib/messager/concerns/queueable.rb
module Messager::Concerns::Queueable
  def message_queue
    @message_queue ||= Messager::Queue.new(self)
  end
end
#gems/messager/lib/messager/concerns/popupable.rb
module Messager::Concerns::Popupable
  def create_message_popup(battler, message)
    message_popups << Messager::Popup.new(battler, message)
  end

  def remove_message_popup(popup)
    self.message_popups -= [popup]
    popup.dispose unless popup.disposed?
  end

  def message_popups
    @message_popups ||= []
  end

  private

  def flush_message_popups
    message_popups.each(&:dispose)
    @message_popups = []
  end

  def update_message_popups
    message_popups.each(&:update)
  end

  def self.included(klass)
    klass.class_eval do 
      attr_reader :viewport2
      attr_writer :message_popups 

      alias original_initialize_for_message_popups initialize
      def initialize
      	flush_message_popups
      	original_initialize_for_message_popups
      end

      alias original_dispose_for_message_popups dispose
      def dispose
        flush_message_popups
        original_dispose_for_message_popups
      end

      alias original_update_for_message_popups update
      def update
        update_message_popups
        original_update_for_message_popups
      end
    end
  end
end
#gems/messager/lib/messager/popup.rb
class Messager::Popup < Sprite_Base
  include Messager::Settings::Popup

  def initialize(target, message)
    spriteset = SceneManager.scene.instance_variable_get :@spriteset
    super spriteset.viewport2
    @target, @message = target, message
    @y_offset, @current_opacity = original_offset, 255
    calculate_text_sizes
    create_rects
    create_bitmap
    self.visible, self.z = true, 199
    Ticker.delay settings[:dead_timeout] do 
      spriteset.remove_message_popup self
    end
    update
  end

  def update
    super
    update_bitmap
    update_position
  end

  def dispose
    self.bitmap.dispose
    super
  end

  private

  def create_rects
    create_icon_rect
    create_text_rect
  end

  def create_icon_rect
    @icon_rect = Rect.new 0, icon_y, settings[:icon_width], settings[:icon_height] if @message.with_icon?
  end

  def create_text_rect
    @text_rect = Rect.new icon_width, 0, @text_width, height
  end

  def calculate_text_sizes
    fake_bitmap = Bitmap.new 1, 1
    configure_font! fake_bitmap
    fake_bitmap.text_size(text).tap do |rect|
      @text_width, @text_height = rect.width, rect.height
    end
  ensure
    fake_bitmap.dispose
  end

  def icon_width
    (@icon_rect && @icon_rect.width).to_i
  end

  def height
    [settings[:icon_height], @text_height].max
  end

  def icon_y
    (height - settings[:icon_height]) / 2.0
  end

  def change_opacity
    self.opacity = @current_opacity
  end

  def create_bitmap
    self.bitmap = Bitmap.new width, height
    display_icon
    display_text
  end

  def width
    icon_width + @text_rect.width
  end

  def configure_font!(bmp)
    bmp.font.size = font_size
    bmp.font.name = settings[:font_name]
    bmp.font.bold = @message.critical?
    bmp.font.color.set Color.new(*settings[:colors][@message.type])
  end

  def font_size
    (@message.type.to_s =~ /^(damage|heal)/ ? 1 : 0) + settings[:font_size]
  end

  def display_text
    configure_font! bitmap 
    bitmap.draw_text @text_rect, text, 1
  end

  def update_bitmap
    @current_opacity -= opacity_speed unless @current_opacity == 0
    @y_offset -= offset_speed unless @y_offset == -200
    change_opacity
  end

  def text
    @text ||= if @message.damage?
      "#{prefix}#{@message.damage.abs} #{postfix}"
    else
      @message.text
    end
  end

  def prefix
    @message.damage > 0 ? '-' : (@message.damage == 0 ? '' : '+')
  end

  def postfix
    "#{settings[:postfixes][@message.type]}#{@message.critical? ? '!' : ''}"
  end

  def opacity_speed
    if @current_opacity > 220
      1
    elsif @current_opacity > 130
      10
    else
      20
    end
  end

  def original_offset
    if @target.is_a? Game_Battler
      settings[:battler_offset]
    else
      settings[:character_offset]
    end
  end

  def offset_speed
    if @y_offset < original_offset - 10
      1
    elsif @y_offset < original_offset - 20
      2
    elsif @y_offset < original_offset - 30
      6
    elsif @y_offset < original_offset - 40
      8
    else
      10
    end
  end

  def display_icon
    if @message.with_icon?
      icons = Cache.system "Iconset"
      rect = Rect.new(
        @message.icon_index % 16 * settings[:icon_width],
        @message.icon_index / 16 * settings[:icon_height],
        24, 24
      )
      bitmap.stretch_blt @icon_rect, icons, rect
    end
  end

  def current_y
    @target.screen_y + @y_offset
  end

  def current_x
    result = @target.screen_x + x_offset
    if result < 0
      0
    elsif result > Graphics.width - width 
      Graphics.width - width 
    else
      result
    end
  end

  def x_offset
    -width / 2 - 2
  end

  def update_position
    self.x, self.y = current_x, current_y
  end
end
#gems/messager/lib/messager/patch.rb
module Messager::Patch
end

#gems/messager/lib/messager/patch/spriteset_battle_patch.rb
class Spriteset_Battle
  include Messager::Concerns::Popupable
end
#gems/messager/lib/messager/patch/spriteset_map_patch.rb
class Spriteset_Map
  include Messager::Concerns::Popupable
end
#gems/messager/lib/messager/patch/window_battle_log_patch.rb
class Window_BattleLog
  METHODS = %w(
    display_action_results display_use_item display_hp_damage 
    display_mp_damage display_tp_damage
    display_counter display_reflection display_substitute
    display_failure display_miss display_evasion display_affected_status
    display_auto_affected_status display_added_states display_removed_states
    display_current_state display_changed_buffs display_buffs
  )
  
  METHODS.each { |name| alias_method "#{name}_for_messager", name }

  def queue(battler)
    @message_queues ||= {}
    @message_queues[battler] ||= battler.message_queue
  end

  def display_current_state(subject)
    unless enabled? subject
      display_current_state_for_messager subject
    end
  end

  def display_action_results(target, item)
    if enabled? target
      if target.result.used
        display_damage(target, item)
        display_affected_status(target, item)
        display_failure(target, item)
      end
    else
      display_action_results_for_messager target, item
    end
  end

  def display_use_item(subject, item)
    if enabled? subject
      queue(subject).push icon_message(
        item.icon_index,
        item.is_a?(RPG::Skill) ? :cast : :use, 
        item.name
      )
    else
      display_use_item_for_messager subject, item
    end
  end

  def display_hp_damage(target, item)
    if enabled? target
      return if target.result.hp_damage == 0 && item && !item.damage.to_hp?
      if target.result.hp_damage > 0 && target.result.hp_drain == 0
        target.perform_damage_effect
      end
      Sound.play_recovery if target.result.hp_damage < 0
      queue(target).push damage_message(target, :hp)
    else
      display_hp_damage_for_messager target, item
    end
  end

  def display_mp_damage(target, item)
    if enabled? target
      return if target.dead? || target.result.mp_damage == 0
      Sound.play_recovery if target.result.mp_damage < 0
      queue(target).push damage_message(target, :mp)
    else
      display_mp_damage_for_messager target, item
    end
  end

  def display_tp_damage(target, item)
    if enabled? target
      return if target.dead? || target.result.tp_damage == 0
      Sound.play_recovery if target.result.tp_damage < 0
      queue(target).push damage_message(target, :tp)
    else
      display_tp_damage_for_messager target, item
    end
  end

  def display_energy_damage(target, item)
    if enabled? target
      return if target.dead? || target.result.energy_damage == 0
      Sound.play_recovery if target.result.energy_damage < 0
      queue(target).push damage_message(target, :energy)
    end
  end

  def display_counter(target, item)
    if enabled? target
      Sound.play_evasion
      queue(target).push text_message(
        Messager::Vocab::CounterAttack,
        :counter_attack
      )
    else
      display_counter_for_messager target, item
    end
  end

  def display_reflection(target, item)
    if enabled? target
      Sound.play_reflection
      queue(target).push text_message(
        Messager::Vocab::MagicReflection,
        :magic_reflection
      )
    else
      display_reflection_for_messager target, item
    end
  end

  def display_substitute(substitute, target)
    if enabled? target
      queue(target).push text_message(
        Messager::Vocab::Substitute,
        :substitute
      )
    else
      display_substitute_for_messager substitute, target
    end
  end

  def display_failure(target, item)
    if enabled? target
      if target.result.hit? && !target.result.success
        queue(target).push text_message(Messager::Vocab::NoEffect, :failure)
      end
    else
      display_failure_for_messager target, item
    end
  end

  def display_shield_block(target)
    queue(target).push text_message(Messager::Vocab::Block, :failure)
  end

  def display_miss(target, item)
    if enabled? target
      type, text = if !item || item.physical?
        Sound.play_miss
        [:miss, Messager::Vocab::Miss]
      else
        [:failure, Messager::Vocab::NoEffect]
      end
      queue(target).push text_message(text, type)
    else
      display_miss_for_messager target, item
    end
  end

  def display_evasion(target, item)
    if enabled? target
      if !item || item.physical?
        Sound.play_evasion
      else
        Sound.play_magic_evasion
      end
      queue(target).push text_message(Messager::Vocab::Evasion, :evasion)
    else
      display_evasion_for_messager target, item
    end
  end

  def display_affected_status(target, item)
    if enabled? target
      if target.result.status_affected?
        display_changed_states target
        display_changed_buffs target
      end
    else
      display_affected_status_for_messager target, item
    end
  end

  def display_auto_affected_status(target)
    if enabled? target
      display_affected_status target, nil
    else
      display_auto_affected_status_for_messager target
    end
  end

  def display_added_states(target)
    if enabled? target
      target.result.added_state_objects.each do |state|
        if state.id == target.death_state_id && Messager::Settings.general[:allow_collapse_effect]
          target.perform_collapse_effect
          wait
          wait_for_effect
        end 
        queue(target).push icon_message(state.icon_index, :icon, state.name)
      end
    else
      display_added_states_for_messager target
    end
  end

  def display_removed_states(target)
    unless enabled? target
      display_removed_states_for_messager target
    end
  end

  def display_changed_buffs(target)
    if enabled? target
      display_buffs(target, target.result.added_buffs, Vocab::BuffAdd)
      display_buffs(target, target.result.added_debuffs, Vocab::DebuffAdd)
    else
      display_changed_buffs_for_messager target
    end
  end

  def display_buffs(target, buffs, fmt)
    if enabled? target
      buffs.each do |param_id|
        lvl = target.instance_variable_get(:@buffs)[param_id]
        icon_index = target.buff_icon_index lvl, param_id
        queue(target).push icon_message(icon_index, :icon)
      end
    else
      display_buffs_for_messager target, buffs, fmt
    end
  end

  private

  def enabled?(target)
    return false unless Messager::Settings.general[:in_battle]
    [:screen_x, :screen_y].all? do |method_name|
      target.respond_to? method_name
    end
  end

  def text_message(text, type)
    message(type).tap { |m| m.text = text }
  end

  def icon_message(icon_index, type, text = '')
    message(type).tap do |object|
      object.icon_index = icon_index
      object.text = text
    end 
  end

  def damage_message(target, key)
    value = target.result.public_send "#{key}_damage"
    message(value < 0 ? :"heal_#{key}" : :"damage_to_#{key}").tap do |the_message|
      the_message.damage = value
      the_message.critical = target.result.critical
    end
  end

  def message(type)
    Messager::Queue::Message.new type
  end
end
#gems/messager/lib/messager/patch/game_battler_patch.rb
class Game_Battler
  include Messager::Concerns::Queueable
end
#gems/messager/lib/messager/patch/game_player_patch.rb
class Game_Player
  include Messager::Concerns::Queueable
end
#gems/messager/lib/messager/patch/game_follower_patch.rb
class Game_Follower
  include Messager::Concerns::Queueable
end
#gems/messager/lib/messager/patch/game_interpreter_patch.rb
 class Game_Interpreter
  #--------------------------------------------------------------------------
  # * Change Gold
  #--------------------------------------------------------------------------
  alias command_125_for_messager command_125
  def command_125
    value = operate_value(@params[0], @params[1], @params[2])
    if Messager::Settings.general[:monitor_gold]
      $game_player.message_queue.gain_gold value
    end
    command_125_for_messager
  end
  #--------------------------------------------------------------------------
  # * Change Items
  #--------------------------------------------------------------------------
  alias command_126_for_messager command_126
  def command_126
    value = operate_value(@params[1], @params[2], @params[3])
    item  = $data_items[@params[0]]
    if Messager::Settings.general[:monitor_items] && item
      $game_player.message_queue.gain_item item, value
    end
    command_126_for_messager
  end
  #--------------------------------------------------------------------------
  # * Change Weapons
  #--------------------------------------------------------------------------
  alias command_127_for_messager command_127
  def command_127
    value  = operate_value(@params[1], @params[2], @params[3])
    weapon = $data_weapons[@params[0]]
    if Messager::Settings.general[:monitor_weapons] && weapon
      $game_player.message_queue.gain_weapon weapon, value
    end
    command_127_for_messager 
  end
  #--------------------------------------------------------------------------
  # * Change Armor
  #--------------------------------------------------------------------------
  alias command_128_for_messager command_128
  def command_128
    value = operate_value(@params[1], @params[2], @params[3])
    armor = $data_armors[@params[0]]
    if Messager::Settings.general[:monitor_armors] && armor
      $game_player.message_queue.gain_armor armor, value
    end
    command_128_for_messager
  end
end
#gems/messager/lib/messager/queue.rb
class Messager::Queue
  TIMEOUT = 30 #frames
  include AASM

  aasm do 
    state :ready, initial: true
    state :beasy

    event :load do
      transitions to: :beasy 

      after do
        show_message
      end

      after do
        Ticker.delay TIMEOUT do 
          check
        end
      end
    end

    event :release do 
      transitions to: :ready 
    end
  end

  def initialize(target)
    @target, @messages, = target, []
  end

  %w(hp tp mp).each do |postfix|
    %w(heal damage_to).each do |prefix|
      name = "#{prefix}_#{postfix}"
      define_method name do |value, critical = false|
        message = Messager::Queue::Message.new name.to_sym 
        message.damage = prefix == 'heal' ? -value : value
        message.critical = critical
        push message
      end
    end
  end

  def cast(spell)
    message = Messager::Queue::Message.new :cast 
    message.text = spell.name
    message.icon_index = spell.icon_index
    push message
  end

  def gain_item(item, number = 1, type = 'item')
    message = Messager::Queue::Message.new :"gain_#{type}" 
    message.text = "#{sign number}#{number} #{item.name}"
    message.icon_index = item.icon_index
    push message
  end

  def gain_weapon(item, number = 1)
    gain_item item, number, 'weapon'
  end

  def gain_armor(item, number = 1)
    gain_item item, number, 'armor'
  end

  def gain_gold(amount)
    message = Messager::Queue::Message.new :gain_gold
    text = "#{sign amount}#{amount} #{Messager::Vocab::Gold}"
    message.text = text
    push message
  end

  def push(message)
    @messages << message
    check if ready?
  end

  def check
    @messages.any? ? load : release
  end

  def show_message
    if message = @messages.shift
      spriteset.create_message_popup @target, message
    end
  end

  private

  def spriteset
    SceneManager.scene.instance_variable_get :@spriteset
  end

  def sign(amount)
    amount >= 0 ? '+' : '-'
  end
end

#gems/messager/lib/messager/queue/message.rb
class Messager::Queue::Message
  attr_accessor :icon_index, :damage, :text
  attr_writer :critical
  attr_reader :type

  def initialize(type)
    @type = type
  end

  def critical?
    !!@critical
  end

  def damage?
    @damage.is_a? Numeric
  end

  def with_icon?
    @icon_index.is_a? Integer
  end
end
#gems/trap/lib/trap.rb
#Traps library
#Author: Iren_Rin
#Terms of use: none
#Requirements: AASM, Ticker. Messager is supported
#Version 0.0.1
#How to install
#- install AASM
#- install Ticker
#- install Messager (not required, but supported)
#- install the script as gem with Sides script loader OR add batch.rb to a project scripts
#How to use
#- read through Trap::Defaults, change if needed
####Thorns
#Thors is collection of events. Trap::Thorns will switch local switches
#of each of these events from A to D by timing.
#When character stands on a event from the collection
#and it switches to A local switch, the character will
#be damaged.
#You can create thorns with following
#trap = Trap::Thorns.build 'thorns1' do #.build method must be called with unique selector
#  map 1             #map id, required
#  events 1, 2, 3, 4 #also events can be setted with array or range
#  damage 20         #damage
#end
#trap.run
#Then you can receive the trap object with the unique selector
#Trap['thorns1'].stop
#Trap['thorns1'].run
####Fireboll
#Fireboll is missile that fly by route and deal damage if touches character.
#Then fieboll expodes with animation.
#Fireboll need 4 by 4 sprite with following scheme
#down | up | right | left 
#down | up | right | left
#down | up | right | left
#down | up | right | left
#Frames of one direaction whill be switching during fly, so you can animate the missile
#You can create fireboll with following
#fireboll = Trap::Fireboll.build 'fireboll1' do
#  map 1
#  speed 10 #speed of the missile, smaller number will lead to faster missile
#  damage 200
#  route do
#    start 1, 1 #x, y
#    down  1, 10
#    right 10, 10
#    up    10, 1
#  end
#  sprite do
#    sprite_path 'Graphics/system/fireboll' #path to missile sprite
#    animation 11 #expoloding animation
#  end
#end
#fireboll.run
#Now you can get the fireboll via Trap[] with selector
####Machinegun
#Machinegun is Trap::Fireboll automated launcher.
#Create it with following code
#trap = Trap::Machinegun.build 'machinegun1' do 
#  #accepts all the settings a firebolls accepts pluse interval
#  interval 200 #interval between launches in frames
#  map 1
#  speed 10 #speed of the missile, smaller number will lead to faster missile
#  damage 200
#  route do
#    start 1, 1 #x, y
#    down  1, 10
#    right 10, 10
#    up    10, 1
#  end
#  sprite do
#    sprite_path 'Graphics/system/fireboll' #path to missile sprite
#    animation 11 #expoloding animation
#  end
#end
#trap.run
#Trap['machinegun1'].stop
#Trap['machinegun1'].run

module Trap
  VERSION = '0.0.1'

  module Defaults
    module Thorns
      def default_options
        {
          damage: 100, #damage of thorn's hit
          speed: 30,   #whole cycle in frames
          hazard_timeout: 5, #after switching to A how many frames the thor will be cutting?
          se: { 'A' => 'Sword4'}, #se playing on each local switch
          timing: { #on which frame of the cycle will be enabled every local switch 
            0 => 'A', 2 => 'B', 4 => 'C', 19 => 'D', 21 => 'OFF'
          }
        }
      end
    end

    module Fireboll
      def default_options
        { 
          speed: 16,  #speed of missile (smaller number for faster missile fly)
          damage: 200 #damage of missile
        }
      end
    end

    module Machinegun
      def default_options
        { interval: 200 } #interval in frames between every missile launch
      end
    end

    module FirebollSprite
      def default_options
        { 
          speed: 0.15, #speed of updating missile sprite 
          sprite_path: 'Graphics/System/fireboll', #path to missile sprite
          animation: 111 #die animation id
        }
      end
    end
  end

  class << self
    def [](id)
      id.is_a?(Regexp) ? matched(id) : traps[id]
    end

    def []=(id, trap)
  	  traps[id] = trap
    end

    def main(id)
      self[id].select(&:main?)
    end

    def all
      traps.values
    end

    def matched(pattern)
      traps.each_with_object([]) do |(key, trap), result|
        result << trap if key.to_s =~ pattern
      end
    end

    def delete(id)
      traps.delete id
    end

    def to_save
      Hash[traps.map { |k, v| [k, v.to_save] }]
    end

    def reset(hash)
      @traps = hash if hash.is_a? Hash
    end

    def flush
      @traps = nil
    end

    def for_map(map_id)
      all.select { |t| t.main? && t.map_id == map_id }
    end

    private

    def traps
      @traps ||= {}
    end
  end
end

#gems/trap/lib/trap/options.rb
class Trap::Options
  def self.build(&block)
    new.tap { |b| b.instance_eval(&block) }
  end

  def initialize
    @options = {}
  end

  def to_h
    @options 
  end

  def method_missing(key, value)
    @options[key] = value 
  end

  def events(*evs)
    evs = evs.first.is_a?(Range) ? evs.map(&:to_a) : evs
    @options[:events] = evs.flatten
  end

  def route(value = nil, &block)
    @options[:route] = block_given? ? Trap::Route.draw(&block) : value
  end

  def sprite(value = nil, &block)
    @options[:sprite] = if block_given?
      init_and_eval block
    else
      value
    end
  end

  def [](key)
    @options[key]
  end

  def init_and_eval(block)
    Trap::Options.new.tap { |o| o.instance_eval(&block) }
  end
end
#gems/trap/lib/trap/route.rb
class Trap::Route
  def self.draw(&block)
    new.tap { |route| route.instance_eval(&block) }
  end

  def initialize(cells = [])
    @cells, @index = cells, 0
  end

  def start(x, y)
    @cells << [x, y]
  end

  %w(down up left right).each do |method_name|
    define_method method_name do |*args|
      exact_method_name = %w(up down).include?(method_name) ? 'exact_y' : 'exact_x'
      __send__(exact_method_name, args) { __send__ "step_#{method_name}" }
    end
  end

  def cell
    if @index < @cells.size
      current_index = @index
      @index += 1
      @cells[current_index]
    end
  end

  def to_enum!
    @cells = @cells.each
  end

  def copy
    self.class.new @cells
  end

  private

  def exact_y(args)
    if args.size > 1
      yield until y == args[1]
    else
      args[0].times { yield }
    end
  end

  def exact_x(args)
    if args.size > 1
      yield until x == args[0]
    else
      args[0].times { yield }
    end
  end

  def step_down
    @cells.last << :down
    @cells << [x, y + 1]
  end

  def step_up
    @cells.last << :up
    @cells << [x, y - 1]
  end

  def step_left
    @cells.last << :left
    @cells << [x - 1, y]
  end

  def step_right
    @cells.last << :right
    @cells << [x + 1, y]
  end

  def x
    @cells.last.first
  end

  def y
    @cells.last[1]
  end
end
#gems/trap/lib/trap/patch.rb
module Trap::Patch
end

#gems/trap/lib/trap/patch/spriteset_map_patch.rb
class Spriteset_Map
  class << self
    attr_writer :trap_sprites 
    
    def trap_sprites
      @trap_sprites ||= []
    end

    def dispose_trap_sprites
      trap_sprites.each(&:dispose)
      @trap_sprites = []
    end
  end

  alias original_update_for_traps update 
  def update
    Spriteset_Map.trap_sprites.each(&:update)
    original_update_for_traps
  end

  alias original_dispose_for_traps dispose 
  def dispose
    Spriteset_Map.dispose_trap_sprites
    original_dispose_for_traps 
  end
end
#gems/trap/lib/trap/patch/data_manager_patch.rb
module DataManager
  instance_eval do
    alias make_save_contents_for_trap make_save_contents

    def make_save_contents
      make_save_contents_for_trap.tap do |contents|
        contents[:traps] = Trap.to_save
      end
    end

    alias extract_save_contents_for_trap extract_save_contents
    def extract_save_contents(contents)
      extract_save_contents_for_trap contents
      Trap.reset contents[:traps]
    end
  end
end
#gems/trap/lib/trap/patch/scene_base_patch.rb
class Scene_Base
  alias original_terminate_for_trap terminate
  def terminate
    original_terminate_for_trap
    if [Scene_Title, Scene_Gameover].include? self.class
      Trap.flush
    end
  end
end
#gems/trap/lib/trap/patch/scene_map_patch.rb
class Scene_Map
  alias original_start_for_trap start 
  def start
    original_start_for_trap
    Trap.all.each(&:restore_after_save_load)
  end
end
#gems/trap/lib/trap/patch/game_map_patch.rb
class Game_Map
  attr_reader :map_id

  def id
    map_id
  end

  alias original_setup_for_trap setup 
  def setup(map_id)
    Trap.for_map(@map_id).each(&:pause)
    original_setup_for_trap map_id 
    Trap.for_map(@map_id).each(&:resume)
  end
end
#gems/trap/lib/trap/concerns.rb
module Trap::Concerns
end

#gems/trap/lib/trap/concerns/hpable.rb
module Trap::Concerns::HPable
  def hp
    actor.hp
  end

  def hp=(val)
    actor.hp = val
  end

  def mhp
    actor.mhp
  end
end
#gems/trap/lib/trap/patch/game_player_patch.rb
class Game_Player
  include Trap::Concerns::HPable
end
#gems/trap/lib/trap/patch/game_follower_patch.rb
class Game_Follower
  include Trap::Concerns::HPable
end
#gems/trap/lib/trap/patch/game_followers_patch.rb
class Game_Followers
  def visible_followers
    visible_folloers
  end
end
#gems/trap/lib/trap/base.rb
class Trap::Base
  include AASM
  attr_writer :main, :slow
  attr_reader :damage_value, :default_speed, :map_id

  def self.build(name, &block)
    options = Trap::Options.build(&block)
    new(name, options).tap { |trap| Trap[name] = trap }
  end

  def initialize(name, options = nil)
    @name = name 
    @options = if options 
      default_options.merge options.to_h
    else 
      default_options
    end
    init_variables
  end

  def main?
    defined?(@main) ? !!@main : true
  end

  def characters
    [$game_player] + $game_player.followers.visible_followers
  end

  def distance_to_player
    ((x - $game_player.x).abs ** 2 + (y - $game_player.y).abs ** 2) ** 0.5
  end

  def restore_after_save_load
    track if running?
  end

  def to_save
    self
  end

  private

  def assert(name)
    unless yield
      raise ArgumentError.new("blank #{name}")
    end
  end

  def same_map?
    $game_map.id == @map_id
  end

  def play_se(se_name, o_volume = 100)
    if se_name && same_map?
      volume = o_volume - 100 / 10 * distance_to_player
      if volume > 0
        se = RPG::SE.new se_name
        se.volume = volume
        se.play
      end
    end
  end

  def message
    Messager::Queue::Message.new(:damage_to_hp).tap do |message| 
      message.damage = damage_value 
    end
  end

  def display_damage(char)
    char.message_queue.push message if defined? Messager
  end

  def speed
    default_speed * (@slow || 1)
  end


  def track
    Ticker.track self
  end

  def untrack
    Ticker.untrack self
  end
end
#gems/trap/lib/trap/thorns.rb
module Trap
  class Thorns < Base
    include Trap::Defaults::Thorns

    aasm do
      state :idle, initial: true
      state :running
      state :paused

      event :run do
        transitions from: :idle, to: :running do
          after { track }
        end
      end

      event :stop do
        transitions from: [:running, :paused], to: :idle do
          after { untrack }
        end
      end

      event :pause do
        transitions from: :running, to: :paused do
          after { untrack }
        end

        transitions from: :idle, to: :idle
      end

      event :resume do
        transitions from: :paused, to: :running do
          after { track }
        end

        transitions from: :idle, to: :idle
      end
    end

  	def init_variables
      @damage_value  = @options[:damage]
      @default_speed = @options[:speed]
      assert('map') { @map_id = @options[:map] }
      assert('events') { @events = @options[:events] }
      @ticked, @hazard, @current = 0, false, -1
  	end

    def tick
      @hazard = false if @ticked % @options[:hazard_timeout] == 0
      next_thorns if frame == 0
      current_thorns if @options[:timing].has_key? frame
      deal_damage
      @ticked += 1
    end

  	private

    def frame
      @ticked % speed
    end

    def next_thorns
      change_current
      @hazard = true
    end

    def current_thorns
      disable_previouse_switch
      enable_current_switch
    end

    def enable_current_switch
      unless @options[:timing][frame] == 'OFF'
        enable_switch @options[:timing][frame] 
      end
    end

    def disable_previouse_switch
      if prev_key = @options[:timing].keys.select { |k| k < frame }.max
        disable_switch @options[:timing][prev_key]
      end
    end

    def disable_switch(sw)
      turn_switch sw, false
    end

    def enable_switch(sw)
      play_se @options[:se][sw]
      turn_switch sw, true
    end

    def turn_switch(sw, bool)
      $game_self_switches[switch_index(sw)] = bool
    end

    def change_current
      @current = @current >= max_current ? 0 : @current + 1
    end

    def deal_damage
      if same_map? && @hazard
        characters.select { |char| char.x == x && char.y == y }.each do |char|
          @hazard = false
          char.hp -= damage_value
          display_damage char
        end
      end
    end

  	def switch_index(char = 'A')
  	  [@map_id, @events[@current], char]
  	end

    def event
      $game_map.events[@events[@current]] if same_map?
    end

    def x
      event.x
    end

    def y
      event.y
    end

  	def max_current
  	  @events.length - 1
  	end
  end
end
#gems/trap/lib/trap/machinegun.rb
class Trap::Machinegun < Trap::Base
  include Trap::Defaults::Machinegun
  aasm do
    state :idle, initial: true
    state :running
    state :paused

    event :run do
      transitions from: :idle, to: :running do
        after { track }
      end
    end

    event :stop do
      transitions from: [:running, :paused], to: :idle do
        after { untrack }
      end
    end

    event :pause do
      transitions from: :running, to: :paused do
        after do
          untrack
          firebolls.each(&:pause)
        end
      end

      transitions from: :idle, to: :idle do
        after { firebolls.each(&:pause) }
      end
    end

    event :resume do
      transitions from: :paused, to: :running do
        after do
          track
          firebolls.each(&:resume)
        end
      end

      transitions from: :idle, to: :idle do 
        after { firebolls.each(&:resume) }
      end
    end
  end

  def init_variables
    assert(:map) { @map_id = @options[:map] }
    assert(:route) { @route  = @options[:route] }
    @interval =  @options[:interval]
    @firebolls_count, @ticked = 0, 0
    @salt = Time.now.to_i + rand(999)
  end

  def tick
    fire if @ticked % speed == 0
    @ticked += 1
  end

  private

  def default_speed
    @interval
  end

  def fire
    if running?
      @firebolls_count += 1
      Trap::Fireboll.build(fireboll_name, &new_options).tap do |trap|
        trap.main = false
        trap.slow = @slow if @slow
      end.run
    end
  end

  def new_options
    map, route = @map_id, @route.copy
    dmg, spd = @options[:damage], @options[:speed]
    sprite_options = @options[:sprite]
    proc do 
      map map
      route route
      damage dmg if dmg
      speed spd if spd
      sprite sprite_options if sprite_options
    end
  end

  def fireboll_name
    "#{@salt}#{@firebolls_count}"
  end

  def firebolls
    Trap[/#{@salt}/]
  end
end
#gems/trap/lib/trap/fireboll.rb
class Trap::Fireboll < Trap::Base
  include Trap::Defaults::Fireboll 

  attr_accessor :x, :y, :direction

  aasm do
    state :idle, initial: true
    state :running
    state :paused

    event :run do
      transitions from: :idle, to: :running do
        after { track }
      end
    end

    event :stop do
      transitions from: [:running, :paused], to: :idle do
        after do
          untrack
          unless @sprite.disposed?
            @sprite.die_animation do
              dispose_sprite
              Trap.delete @name
            end
          end
        end
      end
    end

    event :pause do
      transitions from: :running, to: :paused do
        after do
          untrack
          dispose_sprite
        end
      end

      transitions from: :idle, to: :idle do 
        after { dispose_sprite }
      end
    end

    event :resume do
      transitions from: :paused, to: :running do
        after { track }
      end

      transitions from: :idle, to: :idle
    end
  end

  def init_variables
    assert(:map) { @map_id = @options[:map] }
    assert(:route) { @route  = @options[:route] }
    @damage_value  = @options[:damage]
    @default_speed = @options[:speed]
    @ticked = -1
  end

  def tick
    @ticked += 1
    @x, @y, @direction = @route.cell if @ticked % speed == 0
    create_sprite
    deal_damage
    stop if @direction.nil?
  end

  def screen_x
    x * 32 + x_offset
  end

  def screen_y
    y * 32 + y_offset
  end

  def to_save
    dispose_sprite
    self
  end

  private

  def offset
    (@ticked % speed) * (32.0 / speed)
  end

  def x_offset
    if @direction == :left
      -offset
    elsif @direction == :right
      offset
    else
      0
    end
  end

  def y_offset
    if @direction == :up
      -offset
    elsif @direction == :down
      offset
    else
      0
    end
  end

  def deal_damage
    return unless same_map?
    dealed = false
    characters.select { |char| xes.include?(char.x) && yes.include?(char.y) }.each do |char|
      dealed = true
      char.hp -= damage_value
      display_damage char
    end
    stop if dealed
  end

  def next_x
    x_offset > 0 ? x + 1 : x - 1
  end

  def next_y
    y_offset > 0 ? y + 1 : y - 1
  end

  def xes
    case x_offset.abs
    when 24 .. 32
      [next_x]
    when 8 .. 23
      [x, next_x]
    else
      [x]
    end
  end

  def yes
    case y_offset.abs
    when 24 .. 32
      [next_y]
    when 8 .. 23
      [y, next_y]
    else
      [y]
    end
  end

  def dispose_sprite
    if @sprite
      Spriteset_Map.trap_sprites -= [@sprite]
      @sprite.dispose
      @sprite = nil
    end
  end

  def create_sprite
    if !@sprite || @sprite.disposed?
      @sprite = Trap::Fireboll::Sprite.new self, @options[:sprite]
      Spriteset_Map.trap_sprites << @sprite
    end
  end
end

#gems/trap/lib/trap/fireboll/sprite.rb
class Trap::Fireboll::Sprite < Sprite_Base
  include Trap::Defaults::FirebollSprite

  ROWS  = 4
  COLUMNS = 4
  COLUMNS_HASH = { down: 0, up: 1, right: 2, left: 3 }.tap { |h| h.default = 0 }

  def initialize(trap, options = nil)
    @options = make_options options
    @trap = trap
    @updated = -1
    super nil
    create_bitmap
    update
  end

  def make_options(options)
    options ? default_options.merge(options.to_h) : default_options
  end

  def update
    @updated += @options[:speed]
    update_bitmap
    update_position
    super
  end

  def dispose
    bitmap.dispose
    super
  end

  def die_animation(&b)
    if id = @options[:animation]
      start_animation $data_animations[id], &b
    else
      b.call 
    end
  end

  def start_animation(*args, &block)
    @animated = true
    @on_animation_end = block
    super(*args)
  end

  def end_animation
    super
    @animated = false
    @on_animation_end.call if @on_animation_end
    @on_animation_end = nil
  end

  def animation_process_timing(timing)
    volume = 100 - 100 / 10 * @trap.distance_to_player
    timing.se.volume = volume > 0 ? volume : 0
    super
  end

  private

  def update_bitmap
    src_rect.set column, row, rect_width, rect_height
  end

  def rect_width
    nullify_when_animated { @width / COLUMNS }
  end

  def rect_height
    nullify_when_animated { @height / ROWS }
  end

  def column
    nullify_when_animated do
      @width / COLUMNS * COLUMNS_HASH[@trap.direction]
    end
  end

  def row
    nullify_when_animated do
      (@height / ROWS) * (@updated.to_i % ROWS)
    end
  end

  def nullify_when_animated
    @animated ? 0 : yield
  end

  def create_bitmap
    self.bitmap = Bitmap.new @options[:sprite_path]
    @width, @height = width, height
  end

  def update_position
    self.x, self.y = @trap.screen_x, @trap.screen_y
    self.z = 1
  end
end
#gems/hp_bar/lib/hp_bar.rb
#Author: Iren_Rin
#Use restricions: none
#How to use: read through HPBar constants, change if needed
class HPBar < Sprite_Base
  VERSION = '0.0.1'
  #USE SETTINGS
  USE = {
    battle: true,         #show hp bar in battle?
    map: true             #show hp bar on map
  }
  #SIZES SETTINGS
  WIDTH = {               #width of hp bar
    character: 30,        #for character
    battler: 38           #for battler
  }
  HEIGHT = {              #height of hp bar
    character: 5,         #for charcter
    battler: 5            #for battler
  }
  #POSITION SETTINGS
  #if target (Game_Enemy, Game_Actor, Game_Player, Game_Follower) responds to
  #hp_bar_offset_x and \ or hp_bar_offset_y, the methods will be taken for offsets
  X_OFFSET = {            #x offset from target screen_x
    battler: -19,         #when target is battler
    character: -15        #when target is character
  }
  Y_OFFSET = {            #y offset from target screen y
    battler: -60,          #when target is battler
    character: -45        #when target is character
  }
  #DISAPEARING SETTINGS
  TIMER = {               #how much frames will be displayed the bar?
    character: {          #on map
      #max: 300,           #timer in frames
      #disapearing: 60     #how much frames from the timer the bar will be disapearing?
                          #set equal to 0 to disapear instantly
    },
    battler: {}           #if you do not want to hide the bar after timeout -
                          #set key to empty hash, or nil
  }

  def initialize(viewport, target)
    @target = target
    @y_offset = setting :hp_bar_offset_x, Y_OFFSET[target_key]
    @x_offset = setting :hp_bar_offset_y, X_OFFSET[target_key]
    super viewport
    create_bitmap
    self.opacity = 0 unless use?
    update
  end

  def update
    super
    if use?
      update_bitmap
      update_position
      update_opacity
      @hp_was = @target.hp
    end
  end

  def dispose
    self.bitmap.dispose
    super
  end

  private

  def setting(key, default)
    @target.respond_to?(key) ? @target.send(key) : default
  end

  def target_key
    @target.is_a?(Game_Battler) ? :battler : :character
  end

  def use?
    key = SceneManager.scene.is_a?(Scene_Map) ? :map : :battle
    USE[key] && %w(screen_x screen_y).all? do |name|
      @target.respond_to? name
    end
  end

  def timer_options
    TIMER[target_key] || {}
  end

  def max_timer
    timer_options[:max]
  end

  def disapearing_timer
    timer = timer_options[:disapearing]
    timer < max_timer ? timer : max_timer
  end

  def update_opacity
    return unless max_timer
    if @hp_was == @target.hp
      if @frames_to_dismiss >= 0
        @frames_to_dismiss -= 1
        if @frames_to_dismiss < disapearing_timer
          self.opacity = 255 * @frames_to_dismiss / disapearing_timer
        end
      end
    else
      self.opacity = 255
      @frames_to_dismiss = max_timer
    end
  end

  def update_bitmap
    self.bitmap.clear
    self.bitmap.fill_rect 0, 0, current_width, HEIGHT[target_key], color
  end

  def color
    Color.new red, green, 0
  end

  def current_width
    WIDTH[target_key] * ratio
  end

  def red
    ratio > 0.5 ? 255 * (1 - ratio) * 3 : 255
  end

  def green
    ratio > 0.5 ? 200 : 200 * ratio * 2
  end

  def ratio
    @target.hp.to_f / @target.mhp
  end

  def create_bitmap
    self.bitmap = Bitmap.new WIDTH[target_key], HEIGHT[target_key]
  end

  def update_position
    self.x, self.y = @target.screen_x + @x_offset, @target.screen_y + @y_offset
    self.z = 50
  end
end

#gems/hp_bar/lib/hp_bar/concerns.rb
module HPBar::Concerns
end

#gems/hp_bar/lib/hp_bar/concerns/spritesetable.rb
module HPBar::Concerns::Spritesetable
  def self.included(klass)
    klass.class_eval do
      alias original_hp_bar_initialize initialize
      def initialize
        create_hp_bars
        original_hp_bar_initialize
      end

      alias original_hp_bar_dispose dispose
      def dispose
        dispose_hp_bars
        original_hp_bar_dispose
      end

      alias original_hp_bar_update update
      def update
        update_hp_bars
        original_hp_bar_update
      end

      private

      def create_hp_bars
        @hp_bars = hp_bar_targets.map { |target| HPBar.new @viewport2, target }
      end

      def update_hp_bars
        @hp_bars.each(&:update)
      end

      def dispose_hp_bars
        @hp_bars.each(&:dispose)
      end
    end
  end
end
#gems/hp_bar/lib/hp_bar/concerns/hpable.rb
module HPBar::Concerns::HPable
  def hp
    actor.hp
  end

  def hp=(val)
    actor.hp = val
  end

  def mhp
    actor.mhp
  end
end
#gems/hp_bar/lib/hp_bar/patch.rb
module HPBar::Patch
end

#gems/hp_bar/lib/hp_bar/patch/spriteset_map_patch.rb
class Spriteset_Map
  include HPBar::Concerns::Spritesetable

  def hp_bar_targets
    [$game_player] + $game_player.followers.visible_folloers
  end
end
#gems/hp_bar/lib/hp_bar/patch/spriteset_battle_patch.rb
class Spriteset_Battle
  include HPBar::Concerns::Spritesetable

  def hp_bar_targets
    $game_troop.members + $game_party.members
  end
end
#gems/hp_bar/lib/hp_bar/patch/game_follower_patch.rb
class Game_Follower
  include HPBar::Concerns::HPable
end
#gems/hp_bar/lib/hp_bar/patch/game_player_patch.rb
class Game_Player
  include HPBar::Concerns::HPable
end
#vendor/CC_PMS_VXA_v4.rb
=begin
==============================================================
Title         : CC_PMS_VXA_v4.1
Author        : CC (http://yellow-mantaray.blogspot.jp/)
Last Modified : 2014/07/30

--------------------------------------------------------------
 Features
--------------------------------------------------------------
Parallax Mapping System(PMS). 
You can show multiple layers on the map screen.
As many layers as you want.
Each layer can have a different scroll speed.
Flip animation.

--------------------------------------------------------------
 Terms of Use (2014/07/28)
--------------------------------------------------------------
Creative Commons 4.0
 This work is licensed under the Creative Commons Attribution 4.0 International License.
 To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

 In Short:
  As long as you give credits, you are free to use this script in Commercial/Non-Commercial Projects.

We appreciate your donation.
 If you find this script useful, please consider to make a donation.
 Pledgie URL: https://pledgie.com/campaigns/26257

--------------------------------------------------------------
 How to use
--------------------------------------------------------------

[1] Settings
  Edit CC::PMS.
  CC::PMS::LAYERS decide how many layer it use and how does it appears.
  LAYERS[6] is Array for the animation. Set [] if you don't need the animation.
  
[2] Prepare Images
  In default, file directory is ./Graphics/ParallaxMaps/
  
  Filename format is;
  
  (1)without animation
  map[@map_id]_[name][type]
    ex1. 'map1_gnd0'
  
  (2)with animation
  If LAYERS[6] is Array of Numeric, format will be;
  map[@map_id]_[name][type]_[anim_position]
    ex2. [1,2,3,2] and "map1_water0_1" -> "map1_water0_2" -> "map1_water0_3" -> "map1_water0_2"
  
  The script loads images automatically.
  and if required image is not found, nothing happens.
  
[3] Cache
  This script uses cache for speed. It can be heavy load for memory.
  Call "Cache.clear_parallax_map()" to clear cache manually.
  
[4] Remarks
  This script check files only when you enter the map and not again.
  So, even if you change some variables in the map, PMS don't react and change images.

--------------------------------------------------------------
 References
--------------------------------------------------------------
Yami, "Overlay Mapping", https://yamiworld.wordpress.com/
DasMoony, "DMO - LIQUID & PARALLAX SCRIPT", http://www.rpgmakervxace.net/topic/676-dmo-liquid-parallax-script/

==============================================================
=end

$cc_imported ||= {}
$cc_imported["CC_PMS"] = true

#==============================================================================
## CC::PMSモジュール

module CC
module PMS
	DIR = "Graphics/ParallaxMaps/"
	
	# off_sw_id (osi)      : When Switch[osi] is true, the layer is invisible.
	# changer_var_id (cvi) : When Variables[cvi] > 0, the file 'map(mapid)_(name)(value of Variables[cvi])' is called.
	#                        Othewise, 0 is used as last character of filename.
	# blend_type : 0 = normal, 1 = add, 2 = sub
	
	LAYERS = [ # name, z, opacity, blend_type, off_sw_id, changer_var_id, animate_arr, scroll_ratio
		["gnd", 1, 255, 0, 22, 22, [], nil, ],
		["high", 250, 255, 0, 23, 23, [], nil, ],
		["shd", 251, 85, 2, 25, 25, [], nil, ],
		["anim", 1, 255, 0, 26, 26, [0,1,2,1], nil,  ],
		["rati", 250, 255, 0, 27, 27, [], [0.5, 1], ],
		#["water", 50, 255, 0, 24, 24, [0,1,2,1], nil, ],
	] #
	OFF_SW		= 21		# If $game_variables[OFF_SW] is true, entire PMS will stop.
	
	# Interval to change images.
	ANIM_INTERVAL = 60
	
end # of PMS
end # of CC


#==============================================================
## キャッシュ

module Cache
	def self.parallax_map(filename)
		self.load_bitmap(CC::PMS::DIR, filename)
	end
	
	def self.clear_parallax_map
		# パララックスはメモリを食うので、手動解放を用意しておく
		flag = false
		@cache.each{|key, value|
			if key.include?(CC::PMS::DIR)
				@cache.delete(key)
				flag = true
			end
		}
		GC.start if flag
	end
end

#==============================================================================
## 組み込み

class Spriteset_Map
  alias _pmscc_create_parallax create_parallax
  def create_parallax
    _pmscc_create_parallax
    create_pms
  end
  
  alias _pmscc_dispose_parallax dispose_parallax
  def dispose_parallax
  	dispose_pms
    _pmscc_dispose_parallax
  end
  
  alias _pmscc_update_parallax update_parallax
  def update_parallax
  	_pmscc_update_parallax
  	update_pms
  end
  
  def create_pms
    @cc_pms = []
    @cc_pms_mapid = $game_map.map_id
    CC::PMS::LAYERS.each {|params| @cc_pms.push( CC_pms.new(params, @viewport1) ) }
  end
  
  def dispose_pms
		@cc_pms.each {|layer| layer.dispose }
  end
  
  def update_pms
  	if @cc_pms_mapid != $game_map.map_id
	    @cc_pms_mapid = $game_map.map_id
  		refresh_pms
  	end
  	@cc_pms.each {|layer| layer.update }
  end
  
  def refresh_pms
  	dispose_pms
  	create_pms
  end
end # Spriteset_Map


#==============================================================================
## CC_pms クラス

class CC_pms
	attr_reader :name
	def initialize(params, viewport)
		@name = params[0]
		
		@sprite = Sprite.new
		@sprite.viewport = viewport
		@sprite.z = params[1]
		@sprite.opacity = params[2]
		@sprite.blend_type = params[3]
		
		@off_switch_id = params[4]
		changer_var_id = params[5]
		animation_array = params[6]
		
		@scroll_ratio_x = 1
		@scroll_ratio_y = 1
		if params[7] != nil
			@scroll_ratio_x = params[7][0]
			@scroll_ratio_y = params[7][1]
		end
		
		# アニメ処理
		@filename_itr = 0 # 何番目のファイルネームを使うか
		@frame_count = CC::PMS::ANIM_INTERVAL # ファイルネーム交換のフレーム周期
		
		# ファイル名の配列を作る
		filename = 'map' << $game_map.map_id.to_s << '_' << @name
		filename << ( ( changer_var_id > 0 )?( $game_variables[changer_var_id].to_s ):( '0' ) )
		@filename_arr = []
		if animation_array == []
			@filename_arr.push(filename)
		else
			animation_array.each {|num| @filename_arr.push( filename + '_' + num.to_s )}
		end
		
		reset_image
		update
	end
	
	#-------------------------------------
	def dispose
		@sprite.dispose
	end
	
	#-------------------------------------
	def reset_image
		# 画像のロードを試みてロード失敗したら空にする
		begin
			@sprite.bitmap = Cache.parallax_map( @filename_arr[@filename_itr] )
		rescue
			@sprite.bitmap = nil
			return
		end
		#p @filename_arr[@filename_itr]
	end # of reset_image
	
	#-------------------------------------
  def update
  	# マップ変更のチェック
  	
  	# 画像ないならリターン
  	return if @sprite.bitmap.nil?
  	
  	# オフスイッチが入ってるならvisibleをオフにしてリターン
  	if $game_switches[@off_switch_id] || $game_switches[CC::PMS::OFF_SW]
  		@sprite.visible = false
  		return
  	else
  		@sprite.visible = true
  	end
  	
  	# アニメーションのカウントを進める
  	if @filename_arr.size > 1
			@frame_count -= 1
			if @frame_count < 1
				@frame_count = CC::PMS::ANIM_INTERVAL
				@filename_itr += 1
				@filename_itr = 0 if @filename_itr >= @filename_arr.size
				reset_image
			end
  	end
  	
		# 表示位置を更新
		@sprite.ox = $game_map.display_x * 32 * @scroll_ratio_x
		@sprite.oy = $game_map.display_y * 32 * @scroll_ratio_y
  end # of update
end

#lib/cursor.rb
class Cursor < Sprite_Base
  WIDTH = 32
  HEIGHT = 32
  Y_OFFSET = -100
  X_OFFSET = -18
  MAX_PADDING = 20

  include AASM

  aasm do
    state :hidden,  initial: true
    state :rising
    state :falling

    event :rise do 
      transitions to: :rising
      after do 
        self.padding_correction = -0.5
      end
    end

    event :show do
      transitions to: :rising
      after do 
        self.visible = true
        self.padding_correction = -0.5
      end
    end

    event :fall do
      transitions to: :falling
      after do 
        self.padding_correction = 0.5
      end
    end

    event :hide do
      transitions to: :hidden
      after do
        self.visible = false
      end
    end
  end

  attr_accessor :padding_correction

  def initialize(viewport, color)
  	@color = color
  	super viewport
  	create_bitmap
    self.visible = false
  	self.z = 51
    self.opacity = 255
  	update
  end

  def dispose
  	self.bitmap.dispose
  	super
  end

  def mark(battler)
  	@original_y = @current_y = battler.screen_y + Y_OFFSET
    self.x, self.y = battler.screen_x + X_OFFSET, @original_y
  end

  def update
  	super
  	if visible?
  	  rise if falling? && @original_y <= y
  	  fall if rising?  && @original_y - MAX_PADDING >= y
  	  @current_y += padding_correction
  	  self.y = @current_y.to_i
  	end
  end

  def visible?
    !hidden?
  end

  private

  def create_bitmap
  	self.bitmap = Cache.system 'Arrows'
    src_rect.set *rect_coords, WIDTH, HEIGHT
  end


  def rect_coords
    case @color.to_s
    when 'green'
      [0, 0]
    when 'red'
      [6 * 32, 0]
    else
      [3 * 32, 0]
    end
  end
end
#lib/dsl_builder_base.rb
class DslBuilderBase
  attr_reader :item

  def self.define_methods_for(*method_hashes)
    method_hashes.each do |hash|
      hash.each do |value, method_names|
        [method_names].flatten.each do |method_name|
          define_method(method_name) { value }
        end
      end
    end
  end

  ELEMENTS = {
    -1 => :common, 0 => %w(none nothing), 1 => %w(physical martial),
    2 => %w(drain vampiric), 3 => 'fire', 4 => 'ice', 5 => %w(lighting electrical),
    6 => 'water', 7 => 'earth', 8 => %w(air wind), 9 => %w(holy holly), 10 => 'shadow'
  }

  WTYPES  = {
    1 => 'axe', 2 => %w(claw claws), 3 => 'spear', 4 => 'sword', 5 => %w(katana catana),
    6 => 'bow', 7 => %w(dagger dager), 8 => 'hammer', 9 => 'staff', 10 => %w(gun fire_shooter)
  }

  ATYPES = {
    1 => 'common', 3 => 'light', 4 => %w(massive heavy), 5 => 'small_shield', 6 => %w(big_shield shield)
  }

  PARAMS = {
    0 => 'mhp', 1 => 'mmp', 2 => 'atk', 3 => %w(dff defence defense),
    4 => 'mat', 5 => 'mdf', 6 => 'agi', 7 => 'luk'
  }

  XPARAMS = {
    0 => %w(acu acuracy), 1 => %w(eva evasion), 2 => %w(cri critical), 3 => %w(res resilence),
    4 => %w(sev spell_evasion), 5 => %w(sre spell_reflection), 6 => %w(cou counter_attack),
    7 => %w(hpr hp_regeneration), 8 => %w(mpr mp_regeneration), 9 => %w(tpr tp_regeneration)
  }

  SPARAMS = {
    0 => 'target_chance', 1 => %w(deff_effect_rate deffence_effect_rate), 2 => %w(rec_effect_rate recovery_effect_rate),
    3 => 'medical', 4 => 'spell_mp_cost', 5 => 'tp_rate', 6 => %w(deal_physical_damage deal_phys_damage deal_phys_dmg),
    7 => %w(deal_magical_damage del_mag_damage deal_mag_dmg), 8 => %w(cutting_age_damage cutting_age_dmg fall_damage fall_dmg),
    8 => 'exp_rate'
  }


  define_methods_for ELEMENTS, WTYPES, ATYPES, PARAMS, XPARAMS, SPARAMS

  def method_missing(*args, &block)
    method_name = :"#{args[0]}="

    if item.public_methods.include? method_name
      item.public_send(method_name, *args[1..-1], &block)
    else
      super
    end
  end

  def data(id)
    data_id id
  end

  def shared(key, &block)
    manager_class.shared(key, &block) if block_given?
    (manager_class.fragments[key] || []).each { |fragment| instance_eval(&fragment) }
  end

  def tag(*tags)
    item.tags += tags
  end
  alias tags tag

  def remove_tag(*tags)
    item.tags -= tags
  end
  alias remove_tags remove_tag
end
#lib/dsl_manager_base.rb
class DslManagerBase
  class << self
    attr_writer :current_class

    def current_class
      @current_class || 'common'
    end

    def define(class_name = 'common', &block)
      defenitions[class_name] ||= []
      defenitions[class_name] << block
    end

    def prepare!
      to_prepare.each { |block| instance_eval(&block) }
    end

    def prepare(&block)
      to_prepare << block
    end

    def defenitions
      @defenitions ||= {}
    end

    def fragments
      @fragments ||= {}
    end

    def shared(key, &block)
      fragments[key] ||= []
      fragments[key] << block
    end

    def all(&block)
      @all ||= []
      @all << block if block_given?
      @all
    end

    def to_prepare
      @to_prepare ||= []
    end
  end
end
#lib/enemy_dsl.rb
class EnemyDsl < DslManagerBase
  class << self
    def enemies
      unless @enemies.is_a? Array
        prepare!
        @enemies = []
        defenitions.each { |key, blocks| blocks.each { |block| instance_eval(&block) } }
      end
      @enemies
    end

    def enemy(name = nil, &block)
      EnemyBuilder.new.tap do |builder|
        builder.instance_eval(&block)
        all.each { |all_block| builder.instance_eval(&all_block) }
        builder.item.name = name if name
        enemies << builder.item
      end
    end

    def inject
      enemies.each { |enemy| $data_enemies[enemy.id] = enemy if enemy.id }
      save_data $data_enemies, 'Data/Enemies.rvdata2'
    end
  end
end

#gems/../lib/enemy_dsl/base.rb
class EnemyDsl
  class Base < DslBuilderBase
    def manager_class
      EnemyDsl
    end
  end
end
#gems/../lib/enemy_dsl/enemy_builder.rb
class EnemyDsl
  class EnemyBuilder < Base

    PARAMS.each do |index, method_names|
      [method_names].flatten.each do |method_name|
        define_method method_name do |value|
          @item.params[index] = value
        end
      end
    end

    def initialize
      @item = RPG::Enemy.new
    end

    def skill(sk)
      skills [sk]
    end

    def skills(sks)
      @item.skills |= sks
    end

    def drop_item(&block)
      @item.drop_items << DropItemBuilder.new.tap do |builder|
        builder.instance_eval(&block)
      end.item
    end

    def expand_drop_item(&block)
      EffectBuilder.new(@item.drop_items.last).tap { |builder| builder.instance_eval(&block) }
    end

    def feature(&block)
      @item.features << FeatureBuilder.new.tap { |builder| builder.instance_eval(&block) }.item
    end

    def new_feature(&block)
      @item.features = []
      feature(&block)
    end

    def expand_feature(&block)
      FeatureBuilder.new(@item.features.last).tap { |builder| builder.instance_eval(&block) }
    end

    def sprite(name)
      @item.sprite_name = name
    end

    def copy_params(klass, lvl)
      %w(mhp mmp atk dff mat mdf agi luk).each.with_index do |method_name, index|
        public_send method_name, klass.params[index, lvl]
      end
    end

    def copy_skills(klass, lvl = 999)
      klass.learnings.each_with_object([]) do |learning, result|
        result << $data_skills[learning.skill_id] if learning.level <= lvl
      end.tap { |array| skills array }
    end
  end
end
#gems/../lib/enemy_dsl/drop_item_builder.rb
class EnemyDsl
  class DropItemBuilder < Base
    KINDS =  { 1 => 'stuff', 2 => 'weapon', 3 => 'armor' }
    define_methods_for KINDS

    def initialize(item = RPG::Enemy::DropItem.new)
      @item = item
    end
  end
end
#gems/../lib/enemy_dsl/feature_builder.rb
class EnemyDsl
  class FeatureBuilder < Base
    CODES = {
      Game_BattlerBase::FEATURE_ELEMENT_RATE  => 'element_rate',
      Game_BattlerBase::FEATURE_DEBUFF_RATE   => 'debuff_rate',
      Game_BattlerBase::FEATURE_STATE_RATE    => 'state_rate',
      Game_BattlerBase::FEATURE_STATE_RESIST  => 'state_resist',
      Game_BattlerBase::FEATURE_PARAM         => 'param',
      Game_BattlerBase::FEATURE_XPARAM        => 'xparam',
      Game_BattlerBase::FEATURE_SPARAM        => 'sparam',
      Game_BattlerBase::FEATURE_ATK_ELEMENT   => 'atk_element',
      Game_BattlerBase::FEATURE_ATK_STATE     => 'atk_state',
      Game_BattlerBase::FEATURE_ATK_SPEED     => 'atk_speed',
      Game_BattlerBase::FEATURE_ATK_TIMES     => 'atk_times',
      Game_BattlerBase::FEATURE_STYPE_ADD     => 'stype_add',
      Game_BattlerBase::FEATURE_STYPE_SEAL    => 'stype_seal',
      Game_BattlerBase::FEATURE_SKILL_ADD     => 'skill_add',
      Game_BattlerBase::FEATURE_SKILL_SEAL    => 'skill_seal',
      Game_BattlerBase::FEATURE_EQUIP_WTYPE   => 'equip_wtype',
      Game_BattlerBase::FEATURE_EQUIP_ATYPE   => 'equip_atype',
      Game_BattlerBase::FEATURE_EQUIP_FIX     => 'equip_fix',
      Game_BattlerBase::FEATURE_EQUIP_SEAL    => 'equip_seal',
      Game_BattlerBase::FEATURE_SLOT_TYPE     => 'slot_type',
      Game_BattlerBase::FEATURE_ACTION_PLUS   => 'action_plus',
      Game_BattlerBase::FEATURE_SPECIAL_FLAG  => 'special_flag' ,
      Game_BattlerBase::FEATURE_COLLAPSE_TYPE => 'collapse_type',
      Game_BattlerBase::FEATURE_PARTY_ABILITY => 'party_ability',
    }

    define_methods_for CODES

    def initialize(feature = RPG::BaseItem::Feature.new)
      @item = feature
    end
  end
end
#lib/energy.rb
module Energy
end

#gems/../lib/energy/patch.rb
module Energy::Patch
end





#gems/../lib/energy/patch/rpg_enemy_patch.rb
class RPG::Enemy
  attr_writer :energy_user
  
  def energy_user?
    !!@energy_user
  end
end
#gems/../lib/energy/patch/rpg_actor_patch.rb
class RPG::Actor
  attr_writer :energy_user
  
  def energy_user?
    !!@energy_user
  end
end
#gems/../lib/energy/patch/rpg_skill_patch.rb
class RPG::Skill
  attr_writer :energy_cost, :energy_gain

  def energy_cost
    @energy_cost || 0
  end

  def energy_gain
    @energy_gain || 0
  end
end
#gems/../lib/energy/patch/game_battler_base_patch.rb
class Game_BattlerBase

  attr_writer :energy

  alias original_initliaze_for_energy initialize
  def initialize
    @energy = 100
    original_initliaze_for_energy
  end

  def skill_energy_cost(skill)
    skill.energy_cost
  end

  def skill_cost_payable?(skill)
    tp >= skill_tp_cost(skill) && mp >= skill_mp_cost(skill) && energy >= skill_energy_cost(skill)
  end

  def pay_skill_cost(skill)
    self.mp -= skill_mp_cost(skill)
    self.tp -= skill_tp_cost(skill)
    self.energy -= skill_energy_cost(skill)
  end

  def energy
    energy_user? ? @energy : 0
  end

  def energy_user?
    false
  end

  alias recover_all_for_energy recover_all 
  def recover_all
    recover_all_for_energy
    init_energy 
  end

  def energy_rate
    @energy.to_f / 100
  end
end
#gems/../lib/energy/patch/game_actor_patch.rb
class Game_Actor
  def energy_user?
    actor.energy_user?
  end

  def energy_recharge_rate
    actor.energy_recharge_rate
  end
end
#gems/../lib/energy/patch/game_enemy_patch.rb
class Game_Enemy
  def energy_user?
    enemy.energy_user?
  end

  def energy_recharge_rate
    enemy.energy_recharge_rate
  end
end
#gems/../lib/energy/patch/window_base_patch.rb
class Window_Base
  def energy_cost_color
    Color.new 255, 255, 0
  end

  def energy_gauge_color1
    tp_gauge_color1
  end

  def energy_gauge_color2
    tp_gauge_color2
  end

  def energy_color(actor)
    normal_color
  end

  def draw_actor_energy(actor, x, y, width = 124)
    draw_gauge x, y, width, actor.energy_rate, energy_gauge_color1, energy_gauge_color2
    change_color system_color
    draw_text x, y, 30, line_height, Vocab.energy_a
    change_color energy_color(actor)
    draw_text x + width - 42, y, 42, line_height, actor.energy.to_i, 2
  end
end
#gems/../lib/energy/patch/window_skill_list_patch.rb
class Window_SkillList
  alias draw_skill_cost_for_energy draw_skill_cost
  def draw_skill_cost(rect, skill)
    if @actor.skill_energy_cost(skill) > 0
      change_color energy_cost_color, enable?(skill)
      draw_text rect, @actor.skill_energy_cost(skill), 2
    else
      draw_skill_cost_for_energy rect, skill
    end
  end
end
#gems/../lib/energy/patch/vocab_patch.rb
#encoding=utf-8
module Vocab
  def self.energy
    'Энергия'
  end

  def self.energy_a
    'EN'
  end
end
#gems/../lib/energy/patch/game_action_result_patch.rb
class Game_ActionResult
  attr_accessor :energy_damage

  alias clear_damage_values_for_energy clear_damage_values
  def clear_damage_values
    @energy_damage = 0
    clear_damage_values_for_energy
  end

  def energy_damage_text
    if @energy_damage > 0
      fmt = @battler.actor? ? Vocab::ActorLoss : Vocab::EnemyLoss
      sprintf(fmt, @battler.name, Vocab.energy, @energy_damage)
    elsif @energy_damage < 0
      fmt = @battler.actor? ? Vocab::ActorGain : Vocab::EnemyGain
      sprintf(fmt, @battler.name, Vocab.energy, -@energy_damage)
    else
      ""
    end
  end
end
#gems/../lib/energy/patch/game_battler_patch.rb
class Game_Battler
  EFFECT_GAIN_ENERGY = 401

  alias original_initialize_for_energy initialize
  def initialize
    @additional_effect_methods = { EFFECT_GAIN_ENERGY => :item_effect_gain_energy }
    original_initialize_for_energy
  end

  def item_effect_gain_energy(user, item, effect)
    value = effect.value1.to_i
    @result.energy_damage -= value
    @result.success = true if value != 0
    self.energy += value
  end

  alias item_user_effect_for_energy item_user_effect
  def item_user_effect(user, item)
    user.energy += item.energy_gain
    item_user_effect_for_energy user, item
  end

  alias regenerate_all_for_energy regenerate_all
  def regenerate_all
    regenerate_energy if alive?
    regenerate_all_for_energy
  end

  def regenerate_energy
    self.energy = [energy + energy_recharge_rate, 100].min if energy_user?
  end

  alias on_battle_start_for_energy on_battle_start 
  def on_battle_start
    init_energy 
    on_battle_start_for_energy
  end

  def init_energy
    self.energy = 100 if energy_user?
  end

  alias on_battle_end_for_energy on_battle_end 
  def on_battle_end
    init_energy
    on_battle_end_for_energy
  end
end
#gems/../lib/energy/patch/window_battle_log_patch.rb
class Window_BattleLog
  alias display_damage_for_energy display_damage
  def display_damage(target, item)
    unless target.result.missed || target.result.evaded
      display_energy_damage target, item
    end

    display_damage_for_energy target, item
  end

  def display_energy_damage(target, item)
    return if target.dead? || target.result.energy_damage == 0
    Sound.play_recovery if target.result.energy_damage < 0
    add_text(target.result.energy_damage_text)
    wait
  end
end
#gems/../lib/energy/patch/window_battle_status_patch.rb
class Window_BattleStatus
  def draw_gauge_area_with_tp(rect, actor)
    draw_actor_hp actor, rect.x + 0, rect.y, 72
    draw_actor_mp actor, rect.x + 82, rect.y, 64
    if actor.energy_user?
      draw_actor_energy actor, rect.x + 156, rect.y, 64
    else
      draw_actor_tp actor, rect.x + 156, rect.y, 64
    end
  end
end
#gems/../lib/energy/patch/rpg_item_patch.rb
class RPG::Item
  attr_writer :energy_gain

  def energy_gain
    @energy_gain || 0
  end
end
#gems/../lib/energy/patch/energy_recharge_rate_patch.rb
module Energy::Patch::RechargeRate
  DEFAULT_RATE = 10

  def energy_recharge_rate=(rate)
    @energy_recharge_rate = rate 
  end

  def energy_recharge_rate
    @energy_recharge_rate || DEFAULT_RATE
  end
end

[RPG::Actor, RPG::Enemy].each do |klass|
  klass.__send__ :include, Energy::Patch::RechargeRate
end
#lib/line.rb
class Line
  ALPHA  = Math::PI / 8

  attr_reader :slots

  def initialize(line_set)
    @line_set = line_set
    @max_number_of_actors = 2
    @slots = (0..2).to_a.map { |index| Line::Slot.new(self, index) }
  end

  def add(battler)
    add_battler battler if battlers.size < @max_number_of_actors
  end

  def battlers
    @slots.map(&:battler).compact
  end

  def remove(battler)
    @slots.find { |slot| slot.battler == battler }.battler = nil
    if battlers.size > 0
      @slots[1].battler = battlers.first
      @slots[0].battler, @slots[2].battler = [nil] * 2
    end
  end

  def slot_with(battler)
    @slots.find { |s| s.battler == battler }
  end

  def slot_index_of(battler)
    @slots.index slot_with(battler)
  end

  def heights
    (1 .. 3).to_a.map { |i|  Graphics.height - offset - 40 * i }
  end

  private

  def in_correct_half
    number = yield
    Graphics.width / 2 + (@line_set.half == 1 ? -number : number)
  end

  def offset
    @line_set.offset
  end

  def add_battler(battler)
    if battlers.size == 0
      @slots[1].battler = battler
    else
      old_battler = @slots[1].battler
      @slots[0].battler, @slots[1].battler, @slots[2].battler = old_battler, nil, battler
    end
  end
end

#gems/../lib/line/first.rb
class First < Line
  def width
    in_correct_half { Graphics.width / 10 }
  end
end
#gems/../lib/line/second.rb
class Second < Line
  def width
    in_correct_half { Graphics.width / 10 + Graphics.width / 8 }
  end
end
#gems/../lib/line/third.rb
class Third < Line
  def width
    in_correct_half { Graphics.width / 10 + Graphics.width / 8 * 2 }
  end
end
#gems/../lib/line/slot.rb
class Line::Slot
  attr_accessor :battler
  
  def initialize(line, index)
    @line, @index = line, index
  end

  def x
    @line.width
  end

  def y
    @line.heights[@index]
  end
end
#gems/../lib/line/distance.rb
class Line::Distance
  def initialize(first, second)
    check first, second
    @first, @second             = first, second
    @first_set, @second_set     = set(@first), set(@second)
    @first_index, @second_index = @first_set.index_of(@first), @second_set.index_of(@second)
  end

  def distance
    if @first_set == @second_set
      count(*[@first_index, @second_index].sort, @first_set)
    else
      count(0, @second_index, @second_set) + count(0, @first_index, @first_set)
    end -1
  end

  private

  def class_names
    [@first, @second].map { |battler| battler.line.class.name }.sort
  end

  def set(battler)
    [$game_troop, $game_party].map(&:line_set).find do |line_set|
      line_set.battlers.include? battler
    end
  end

  def count(from, to, line_set)
    line_set.lines[from .. to].count do |line|
      line.battlers.any? do |battler|
        !battler.transparent? || [@first, @second].include?(battler)
      end
    end
  end

  def check(*battlers)
    unless battlers.all? { |b| b.is_a? Game_Battler }
      raise ArgumentError.new('Only battlers allowed')
    end
  end
end
#lib/line_set.rb
class LineSet
  attr_reader   :padding, :lines
  attr_accessor :half
  #halves:
  #1 2
  def initialize(half)
    check half

    @half = half
    @lines  = [Line::First.new(self), Line::Second.new(self), Line::Third.new(self)]
  end

  def slot_with(battler)
    slots.find { |slot| slot.battler == battler }
  end

  def line_with(battler)
    lines.find { |line| line.battlers.include? battler }
  end

  def index_of(battler)
    case line_with(battler)
    when Line::First
      0
    when Line::Second
      1
    when Line::Third
      2
    end
  end

  def slots
    lines.map(&:slots).flatten
  end

  def offset
    Graphics.height / 4
  end

  def front_battlers
    @lines.inject([]) do |result, line|
      return result if result.any?
      alive_and_visible line
    end
  end

  def visible_battlers
    result = @lines.each_with_object count: 0, battlers: [] do |line, object|
      battlers = alive_and_visible line
      if battlers.any? && object[:count] < 2
        object[:battlers] += battlers
        object[:count] += 1
      end
    end
    result[:battlers]
  end

  def battlers
    slots.map(&:battler).compact
  end

  def slot_index_of(battler)
    line_with(battler).slot_index_of(battler) if battlers.include? battler
  end

  def battlers_behind(battler)
    if index = slot_index_of(battler)
      lines[index_of(battler) + 1 .. 2].map { |line| line.slots[index].battler }.compact
    else
      []
    end
  end

  def x
    lines[1].slots[1].x
  end

  def y
    lines[1].slots[1].y
  end

  private

  def alive_and_visible(line)
    line.battlers.select { |battler| battler.visible? && battler.alive? }
  end

  def check(half)
    raise ArgumentError unless (1..2).to_a.include? half
  end
end
#lib/nottable.rb
module Nottable
  def self.note!
  	$data_actors[1..-1].each(&:note!)
  	save_data $data_actors, 'Data/Actors.rvdata2'
  end

  def note!
    if /<ruby>(.*)<\/ruby>/m =~ note
      instance_eval $1
    end
  end
end
#lib/skill_blocker.rb
module SkillBlocker
  def self.blocks?(battler, skill)
    rules.any? { |rule| rule.blocks? battler, skill }
  end

  def self.rules
    @rules ||= []
  end
end

#gems/../lib/skill_blocker/patch.rb
module SkillBlocker::Patch
end

#gems/../lib/skill_blocker/patch/game_battler_base_patch.rb
class Game_BattlerBase
  alias skill_conditions_met_for_skill_blocker skill_conditions_met?
  def skill_conditions_met?(skill)
    skill_conditions_met_for_skill_blocker(skill) && !SkillBlocker.blocks?(self, skill)
  end
end
#gems/../lib/skill_blocker/rules.rb
module SkillBlocker::Rules
end

#gems/../lib/skill_blocker/rules/observable.rb
class SkillBlocker::Rules::Observable
  def blocks?(battler, skill)
    skill.effects.any? do |effect|
      effect.code == 21 && effect.data_id == RPG::State.transparent.id
    end && battler.observable?
  end

  SkillBlocker.rules << new
end
#lib/skill_dsl.rb
class SkillDsl < DslManagerBase
  class << self
    def skill(name = nil, &block)
      SkillBuilder.new.tap do |builder|
        builder.item.name = name if name
        all.each { |all_block| builder.instance_eval(&all_block) }
        builder.instance_eval(&block)
        skills[current_class] ||= []
        skills[current_class] << builder.item
      end
    end

    def skills
      unless @skills.is_a? Hash
        prepare!
        @skills = {}
        defenitions.each do|name, blocks|
          self.current_class = name
          blocks.each { |block| instance_eval(&block) }
        end
      end
      @skills
    end

    def inject
      skills.each { |class_name, skills| skills.each { |skill| $data_skills[skill.id] = skill if skill.id } }
      save_data $data_skills, 'Data/Skills.rvdata2'
    end
  end
end

#gems/../lib/skill_dsl/base.rb
class SkillDsl
  class Base < DslBuilderBase
    def manager_class
      SkillDsl
    end
  end
end
#gems/../lib/skill_dsl/damage_builder.rb
class SkillDsl
  class DamageBuilder < Base
    TYPES = {
      0 => %w(buff debuff), 1 => :to_hp, 2 => :to_mp,
      3 => 'heal_hp', 4 => 'heal_mp', 5 => 'drain_hp', 6 => 'drain_mp'
    }

    define_methods_for TYPES

    def element(id)
      element_id id
    end

    def critical(value)
      @item.critical = value
    end

    def initialize(item = nil)
      @item = item || RPG::UsableItem::Damage.new
    end
  end
end
#gems/../lib/skill_dsl/effect_builder.rb
class SkillDsl
  class EffectBuilder < Base
    CODES = {
      11 => 'recover_hp', 12 => 'recover_mp', 13 => 'gain_tp', 21 => 'add_state',
      22 => 'remove_state', 31 => 'add_buff', 32 => 'add_debuff', 33 => 'remove_buff',
      34 => 'remove_debuff', 41 => 'special', 42 => 'raise_parameter', 43 => 'learn_skill',
      44 => 'common_event', 401 => 'gain_energy'
    }

    define_methods_for CODES

    def initialize(item = nil)
      @item = item || RPG::UsableItem::Effect.new
    end

    def effect(value)
      code value
    end
  end
end
#gems/../lib/skill_dsl/skill_builder.rb
class SkillDsl
  class SkillBuilder  < Base
    HIT_TYPES = { 1 => 'physical', 0 => 'certain', 2 => 'magical' }
    SCOPES = {
      0 => %w(none empty), 1 => 'enemy', 2 => 'all_enemies', 3 => 'double_on_enemy',
      4 => 'random_enemy', 5 => 'two_random_enemies', 6 => 'three_random_enemies',
      7 => %w(ally aly friend), 8 => %w(all_allies all_alies all_friends),
      9 => %w(dead_friend dead_ally dead_aly), 10 => %w(all_dead_friends all_dead_allies all_dead_alies),
      11 => %w(me my_self myself)
    }
    OCCASIONS = { 0 => %w(all_the_time allways), 1 => 'only_in_battle', 2 => 'only_from_menu', 3 => 'never' }
    STYPES    = { 1 => %w(special spec), 2 => 'magic' }



    define_methods_for SCOPES, HIT_TYPES, OCCASIONS, STYPES

    def initialize
      @item = RPG::Skill.new
    end

    def animation(key, value)
      @item.animations[key] = value
    end

    def class_name(name = nil)
      name ? @class_name = name : (@class_name || 'general')
    end

    def damage(&block)
      @item.damage = DamageBuilder.new(@item.damage).tap { |builder| builder.instance_eval(&block) }.item
    end

    def effect(&block)
      @item.effects << EffectBuilder.new.tap { |builder| builder.instance_eval(&block) }.item
    end

    def expand_effect(&block)
      EffectBuilder.new(@item.effects.last).tap { |builder| builder.instance_eval(&block) }
    end

    def required_weapon(id)
      @item.required_wtype_id1.to_i > 0 ? required_wtype_id2(id) : required_wtype_id1(id)
    end

    def required_armor(id)
      @item.required_atype_id1.to_i > 0 ? required_atype_id2(id) : required_atype_id1(id)
    end

    def message(text)
      text = ' ' + text
      @item.message1.to_s.length > 0 ? message2(text) : message1(text)
    end

    def stype(id)
      stype_id id
    end

    def pose(value)
      @item.pose = value
    end
  end
end
#lib/sprite_animated_battler.rb
class Sprite_AnimatedBattler < Sprite_Battler
  ROWS = {
    iddle: 0,
    guard: 1, guarded: 1, deffence: 1, block: 1,
    dying: 2, woozy: 2, low_hp: 2,
    attacked: 3, taking_damage: 3,
    attack: 4, attacking: 4, counter_attack: 4,
    using_item: 5,item: 5,
    skill: 6,
    casting: 7, spell: 7, magic_reflection: 7,
    dash: 8, run: 8,
    run_away: 9,
    victory: 10,
    enter: 11, battle_start: 11,
    dead: 12, die: 12
  }

  MAX_ROWS = 14
  MAX_COLUMNS = 4

  def initialize(*args)
    @column = 0
    super
  end

  def new_bitmap
    Cache.battler @battler.sprite_name, @battler.battler_hue
  end

  def update_bitmap
    if bitmap != new_bitmap
      self.bitmap = new_bitmap
      set_sizes
      set_mirror
      init_visibility
    end
  end

  def update
    super
    if @battler
      @use_sprite = @battler.use_sprite?
      if @use_sprite
        update_bitmap
        set_rectangle
        update_origin
        update_position
        update_opacity
      end
      setup_new_effect
      setup_new_animation
      update_effect
    else
      self.bitmap = nil
      @effect_type = nil
    end
  end

  def update_opacity
    self.opacity = @battler.transparent? && @battler.alive? ? 100 : 255
  end

  def set_animation_origin
    offset = @height / 3
    if @animation.position == 3
      line_set = @battler.friends_unit.line_set
      @ani_ox = line_set.x
      @ani_oy = line_set.y - offset
    else
      @ani_ox = @battler.screen_x
      @ani_oy = @battler.screen_y - offset
      if @animation.position == 0
        @ani_oy -= offset
      elsif @animation.position == 2
        @ani_oy += offset
      end
    end
  end

  def revert_to_normal
    self.blend_type = 0
    self.color.set(0, 0, 0, 0)
    self.opacity = 255
  end

  private

  def set_sizes
    @width  = bitmap.width / MAX_COLUMNS
    @height = bitmap.height / MAX_ROWS
  end

  def set_rectangle
    new_y = row * @height
    new_x = column * @width
    src_rect.set new_x, new_y, @width, @height
  end

  def init_visibility
    self.opacity = 255
  end

  def set_mirror
    self.mirror = @battler.friends_unit.line_set.half == 1
  end

  def update_origin
    self.ox, self.oy = @width / 2, @height
  end

  def row
    @row = ROWS[@battler.pose].tap do |battler_row|
      @column = 0 unless battler_row == @row
    end
    @row
  end

  def speed
    row > 2 ? 0.22 : 0.075
  end

  def freeze?
    row > 2
  end

  def column
    value = @column
    if @column >= MAX_COLUMNS - 1
      @column = 0 unless freeze? 
    else
      @column += speed
    end
    value.to_i
  end
end
#lib/states_bar.rb
class StatesBar < Sprite_Base
  ICON_WIDTH = ICON_HEIGHT = 24
  Y_OFFSET = -80
  X_OFFSET = -30
  MAX_ICONS = 3
  ZOOM = 0.8

  def initialize(viewport, target, y_offset = Y_OFFSET)
    @target, @y_offset = target, y_offset
    @states, @buff_icons = [], []
    super viewport
    create_bitmap
    update
  end

  def update
    super
    update_bitmap
    update_position unless disposed?
  end

  def dispose
    self.bitmap.dispose
    super
  end

  private

  def update_bitmap
    if need_update?
      if @target.alive?
        self.visible = true
        self.bitmap.clear
        draw_icons
        @states = target_states
        @buff_icons = target_buff_icons
      else
        self.visible = false
      end
    end
  end

  def need_update?
    target_states != @states || target_buff_icons != @buff_icons
  end

  def target_states
    @target.states 
  end

  def target_buff_icons
    @target.buff_icons
  end

  def draw_icons
    (@target.state_icons + target_buff_icons).each.with_index do |icon_index, index| 
      draw_state_icon icon_index, index
    end
  end

  def draw_state_icon(icon_index, index)
    rect = Rect.new(
      icon_index % 16 * ICON_WIDTH,
      icon_index / 16 * ICON_HEIGHT,
      ICON_WIDTH, ICON_HEIGHT
    )
    bitmap.stretch_blt icon_rect(index), icons, rect
  end

  def icons
    Cache.system "Iconset"
  end

  def icon_rect(index)
    Rect.new ICON_WIDTH * index, 0, ICON_WIDTH, ICON_HEIGHT
  end

  def create_bitmap
    self.bitmap = Bitmap.new ICON_WIDTH * MAX_ICONS, ICON_HEIGHT
    self.zoom_x = self.zoom_y = ZOOM
  end

  def update_position
    self.x, self.y = @target.screen_x + X_OFFSET, @target.screen_y + @y_offset
    self.z = 50
  end
end

#gems/../lib/states_bar/patch.rb
module StatesBar::Patch
end

#gems/../lib/states_bar/patch/spriteset_battle_patch.rb
class Spriteset_Battle
  alias initialize_for_states_bar initialize
  def initialize
    create_states_bars
    initialize_for_states_bar
  end

  alias dispose_for_states_bar dispose
  def dispose
    dispose_states_bars
    dispose_for_states_bar
  end

  alias update_for_states_bar update
  def update
    update_states_bars
    update_for_states_bar
  end

  private

  def update_states_bars
    if $game_party.members.size != @actor_sprites.size
      dispose_states_bars
      create_states_bars
    end
    @states_bars.each(&:update)
  end

  def dispose_states_bars
    @states_bars.each(&:dispose)
  end

  def create_states_bars
    @states_bars = states_bars_targets.map do |target|
      StatesBar.new @viewport2, target
    end
  end

  def states_bars_targets
    $game_party.alive_members + $game_troop.alive_members
  end
end
#lib/strategy.rb
class Strategy
  class << self
    def best
      strategies.max_by(&:success_index).new
    end

    def strategies
      @@strategies ||= []
    end

    def register
      strategies << self unless strategies.include? self
    end
  end

  def initialize
    @current_phase = :waiting
  end

  def lines_matrix
    matrix = [[], [], []]
    $game_troop.sort_by_priority!
    $game_troop.members.each_with_index do |enemy, index|
      wish_index = 0
      wish_index += 1 until matrix[enemy.lines_priority[wish_index] - 1].length < 2
      matrix[enemy.lines_priority[wish_index] - 1] << index
    end
    matrix
  end

  def current_phase
    switch_if_needed
    @current_phase
  end

  def actions(phase)
    damage_dealers.map { |enemy| action_for enemy, phase }
  end

  def cost_ok?(key, phase)
    actions(phase).all? do |action|
      skill, enemy = action[:wish_skill], action[:enemy]
      skill.nil? || (skill.public_send("#{key}_cost") * 3 <= enemy.public_send(key))
    end
  end

  def cost_low?(key, phase)
    actions(phase).all? do |action|
      skill, enemy = action[:wish_skill], action[:enemy]
      skill.nil? || (skill.public_send("#{key}_cost") > enemy.public_send(key))
    end
  end

  def action_for(enemy, phase = current_phase)
    Strategy::Role.find(enemy.current_role).action_for enemy, phase, target
  end

  def set_roles
    $game_troop.alive_members.each { |member| member.current_role = nil }
  end

  private

  def front_target
    weakest $game_party.line_set.front_battlers
  end

  def visible_taget
    weakest $game_party.line_set.visible_battlers
  end

  def weakest(actors)
    actors.min_by { |actor| actor.effective_hp attack_type }
  end

  def attack_type
    [:martial, :magical].max_by do |tag|
      damage_dealers.count { |dd| dd.tag? tag }
    end
  end

  def target
    $game_troop.alive_members.any?(&:melee?) ? front_target : visible_taget
  end

  def at_least(number, role)
    enemies_fit_for(role).sample(number).each do |enemy|
      enemy.current_role = role
    end
  end

  def rest_to(*roles)
    roles.each do |role|
      enemies_without_role.each do |enemy|
        enemy.current_role = enemy.tag?(role) ? role : role_key(enemy)
      end
    end
  end

  def role_key(enemy)
    roles.find { |role| enemy.tag? role }
  end

  def enemies_without_role
    $game_troop.alive_members.select { |member| member.current_role.nil? }
  end

  def enemies_fit_for(role)
    enemies_without_role.select { |enemy| enemy.tag? role }
  end
end
#gems/../lib/strategy/concerns.rb
module Strategy::Concerns
end

#gems/../lib/strategy/concerns/skillable.rb
module Strategy::Concerns::Skillable
  def area_burst
    burst_skill area_skills
  end

  def solo_burst
    burst_skill solo_skills
  end

  def burst_skill(skills)
    skills.select { |skill| skill.tag? :high_damage }.max_by(&damage)
  end

  def area_ongoing
    ongoing_skill area_skills
  end

  def ongoing_skill(skills)
    skills.select { |skill| (skill.tags & [:normal_damage, :low_damage]).any? }.max_by(&damage)
  end

  def solo_ongoing
    ongoing_skill solo_skills
  end

  def area_controll
    area_skills.select { |skill| skill.tag? :controll }.select(&usefull_controll).sample
  end

  def defence
    $data_skills[@enemy.guard_skill_id]
  end

  def attack_skill
    $data_skills[@enemy.attack_skill_id]
  end

  def attack
    reachable { usable { [attack_skill] } }.first
  end

  def waiting
    tp_or_mp_gain_skill || cheap_skill || attack || defence
  end

  def usable
    yield.select { |skill|  @enemy.usable?(skill) || @wish_mode }
  end

  def reachable
    yield.select { |skill| @enemy.can_hit? target, with: skill }
  end

  def solo_skills
    reachable { usable { all_solo_skills + [attack_skill] } }
  end

  def all_solo_skills
    @enemy.skills.select(&solo)
  end

  def solo
    proc do |skill|
      skill.tag?(:solo) || !skill.tag?(:area) || !skill.for_all?
    end
  end

  def recharging
    proc do |skill|
      skill.any_tag?(:tp_gain, :mp_gain) || skill == attack_skill
    end
  end

  def cheap
    proc { |skill| skill.any_tag? :low_cost, :cheap, :free }
  end

  def area_skills
    usable do
      @enemy.skills.select { |skill| skill.tag?(:area) || skill.for_all? }
    end
  end
end
#gems/../lib/strategy/rate.rb
class Strategy::Rate
  def self.success_rate(&block)
    new.tap { |rate| rate.instance_eval(&block) }.rate
  end

  def initialize
    @prefer, @secondary = [], []
    @progressive = {}
  end

  def prefer(*roles)
    @prefer |= roles
  end

  alias firstly prefer

  def progressive(role, rules = {})
    @progressive[role] = rules
  end

  def secondary(*roles)
    @secondary |= roles
  end

  def rate
    hash = $game_troop.members.each_with_object rate: 0, enemies: [] do |enemy, hash|
      hash[:rate] +=  (enemy_rate(enemy.tags) + progressively(enemy.tags, hash[:enemies]))
      hash[:enemies] << enemy
    end
    hash[:rate]
  end

  private

  def progressively(tags, enemies)
    role, max = rule(tags)
    if role
      count = enemies.count { |enemy| enemy.tags.include? role }
      count > max.to_i ? 0 : count
    end.to_i * 2
  end

  def rule(tags)
    if (tags & @progressive.keys).any?
      role = tags.max { |tag| (@progressive[tag] && (@progressive[tag][:max] || Float::INFINITY)) || 0 }
      [role, @progressive[role][:max]]
    else
      [nil, nil]
    end
  end

  def enemy_rate(tags)
    if (@prefer & tags).any?
      2
    elsif (@secondary & tags).any?
      1
    end.to_i
  end
end
#gems/../lib/strategy/role.rb
class Strategy
  class Role
    include Strategy::Concerns::Skillable

    class << self
      def register(key)
        roles[key] = self unless roles[key]
      end

      def all
        roles.values
      end

      def roles
        @@roles ||= {}
      end

      def find(key)
        roles[key]
      end

      def action_for(enemy, phase, target = nil)
        new(enemy, phase, target).action
      end
    end

    attr_reader :target

    def initialize(enemy, phase, target)
      @target = target
      @enemy, @phase = enemy, phase
    end

    def action
      { skill: skill, target_index: target_index, enemy: @enemy, wish_skill: wish_skill }
    end

    def wish_skill
      @wish_mode = true
      try @phase
    ensure
      @wish_mode = false
    end

    def skill
      try(@phase) || hierarhy_skill
    end

    def perception
      if $game_party.alive_members.any?(&:transparent?)
        skills.find { |skill| skill.tag? :perception }
      end
    end

    private


    def alive_enemies
      $game_troop.alive_members
    end

    def damage
      proc do |skill|
        targets_for(skill).inject(0) { |sum, target| sum + target.check_damage_value(@enemy, skill) }
      end
    end

    def usefull_controll
      proc { |skill| !overlaping_state?(skill) }
    end

    def overlaping_state?(skill)
      if skill.for_all?
        $game_party.alive_members.count { |m| (m.states & skill.states).any? } /
          $game_party.alive_members.count.to_f > 0.5
      else
        (target.states & skill.states).any?
      end
    end

    def targets_for(skill)
      if skill.for_all?
        $game_party.alive_members
      else
        [target]
      end
    end

    def target_index
      target.friends_unit.members.index target if target
    end
  end
end

%w(
  area_burster area_ongoiner solo solo_burster solo_ongoiner healer
  tank solo_controller area_controller rogue rogue_burster rogue_ongoiner
).each do |role|
end
#gems/../lib/strategy/role/area_burster.rb
class Strategy::Role::AreaBurster < Strategy::Role
  def hierarhy_skill
    area_burst || area_controll || area_ongoing  || attack || waiting  || defence
  end

  private

  def cheap_skill
    area_skills.select(&cheap).max_by(&damage)
  end

  def tp_or_mp_gain_skill
    area_skills.select(&recharging).max_by(&damage)
  end

  register :area_burster
end
#gems/../lib/strategy/role/area_ongoiner.rb
class Strategy::Role::AreaOngoiner < Strategy::Role

  private

  def hierarhy_skill
    area_ongoing || area_controll || solo_ongoing  || attack || defence
  end

  register :area_ongoiner
end
#gems/../lib/strategy/role/solo.rb
class Strategy::Role::Solo < Strategy::Role
  private

  def tp_or_mp_gain_skill
    solo_skills.select(&recharging).max_by(&damage)
  end

  def cheap_skill
    solo_skills.select(&cheap).max_by(&damage)
  end
end
#gems/../lib/strategy/role/solo_burster.rb
class Strategy::Role::SoloBurster < Strategy::Role::Solo
  def hierarhy_skill
    solo_burst  || solo_ongoing || waiting || attack || defence
  end

  register :solo_burster
end
#gems/../lib/strategy/role/solo_ongoiner.rb
class  Strategy::Role::SoloOngoiner < Strategy::Role::Solo
  def hierarhy_skill
    solo_ongoing || waiting || attack || defence
  end

  register :solo_ongoiner
end
#gems/../lib/strategy/role/healer.rb
class Strategy::Role::Healer < Strategy::Role
  HEALTH_PERCENT = 75 / 100.0

  def skill
    area_heal || solo_heal || area_hot || 
      ally_without_hot { solo_hot } ||
      with_opositive_target { attack } || defence
  end

  def wish_skill
    skill
  end

  private

  def with_opositive_target
    if skill = yield
      @opositive_target = if @enemy.can_hit? @target, with: skill
        @target
      else
        $game_party.alive_members.find do |actor|
          @enemy.can_hit? actor, with: skill
        end
      end
      skill if @opositive_target
    end
  end

  def ally_without_hot
    if skill = yield
      @forced_target = alive_enemies.select do |member|
        !member.states.include? $data_states[14]
      end.max_by(&rest_hp)
      skill if @forced_target
    end
  end

  def solo_hot
    solo_skills.find { |skill| skill.tag? :hot }
  end

  def area_hot
    hot_needed? && area_skills.find { |s| s.tag?(:hot) }
  end

  def solo_heal
    need_heal.any? && solo_skills.select { |s| s.tag? :heal }.max_by(&heal_value)
  end

  def area_heal
    need_heal.count > 1 && area_skills.select { |s| s.tag? :heal }.max_by(&heal_value)
  end

  def target
    @forced_target || @opositive_target || weaker_enemy
  end

  def weaker_enemy
    alive_enemies.min_by(&rest_hp)
  end

  def rest_hp
    proc do |member|
      member.hp / member.mhp.to_f
    end
  end

  def need_heal
    alive_enemies.select { |member| member.hp < member.mhp * HEALTH_PERCENT }
  end

  def hot_needed?
    alive_enemies.any? { |member| !member.states.include? $data_states[14] }
  end

  def heal_value
    proc { |skill| eval skill.damage.formula }
  end

  def b
    weaker_enemy
  end

  register :healer
end
#gems/../lib/strategy/role/tank.rb
class Strategy::Role::Tank < Strategy::Role
  attr_reader :target

  def initialize(enemy, phase, target)
    super
    @strategy_target = target
  end

  def skill
    @targret = nil
    with_self_target { self_protection_extreme } ||
      allies_protection ||
      ally_protection ||
      perception ||
      with_self_target { self_protection_long_time } || 
      with_opositive_target { self_protection_short_time || attack } ||
      defence
  end

  def wish_skill
    skill
  end

  private

  def self_protective_skills
    skills.select { |skill| skill.tags? :protective, :self }
  end

  def ally_protective_skills
    skills.select { |skill| skill.tags? :protective, :ally  }
  end

  def allies_protective_skills
    skills.select { |skill| skill.tags? :protective, :allies  }
  end

  def allies_protection
    allies_protective_skills.find do |skill|
      alive_enemies.any? { |enemy| stateable? skill, enemy }
    end
  end

  def ally_protection
    ally_protective_skills.find do |skill|
      if target = (alive_enemies - [@enemy]).find { |enemy| stateable? skill, enemy }
        @target = target
      end
    end
  end

  def self_protection_extreme
    if @enemy.hp < @enemy.mhp * 0.3
      self_protective_skills.find { |s| s.any_tag? :extreme, :extrimal } 
    end
  end

  def self_protection_long_time
    self_protective_skills.find do |skill|
      skill.tag?(:long_time) && stateable?(skill, @enemy)
    end
  end

  def self_protection_short_time
    self_protective_skills.find { |skill| skill.tag? :short_time }
  end

  def skills
    usable { @enemy.skills }
  end

  def stateable?(skill, target)
    (skill.states.empty? || (skill.states - target.states).any?) && @enemy.can_hit?(target, with: skill)
  end

  def with_self_target
    yield.tap { |result| result && @target = @enemy }
  end

  def with_opositive_target
    if skill = yield
      @target = if @enemy.can_hit? @strategy_target, with: skill
        @strategy_target
      else
        $game_party.alive_members.find { |member| @enemy.can_hit? member, with: skill }
      end
      skill if @target
    end
  end

  register :tank
end
#gems/../lib/strategy/role/solo_controller.rb
class Strategy::Role::SoloController < Strategy::Role
  register :solo_controller
end
#gems/../lib/strategy/role/area_controller.rb
class Strategy::Role::AreaController < Strategy::Role
  register :area_controller

  def area_ongoing
    area_controll || super
  end

  def area_burst
    area_controll || super
  end

  private

  def tp_or_mp_gain_skill
    hierarhy_skill
  end

  def cheap_skill
    hierarhy_skill
  end

  def hierarhy_skill
    area_controll || attack || defence
  end
end
#gems/../lib/strategy/role/rogue.rb
module Strategy::Role::Rogue
  def skill
    stealth || super
  end

  private

  def stealth
    @phase == :solo_burst && high_damage_stealth || normal_stealth
  end

  def high_damage_stealth
    when_stealth_required do
      burst_skill solo_skills.select { |skill| skill.tags? :stealth }
    end
  end

  def normal_stealth
    when_stealth_required do
      all_solo_skills.find { |skill| skill.tags? :stealth, :no_damage }
    end
  end

  def when_stealth_required
    yield if stealth_required?
  end

  def stealth_required?
    area_damage_absent? && !@enemy.observable? && !@enemy.transparent?
  end

  def area_damage_absent?
    $game_party.alive_members.none? do |actor|
      actor.any_tag? :area_ongoiner, :area_controller, :area_burster
    end
  end
end
#gems/../lib/strategy/role/rogue_burster.rb
class Strategy::Role::RogueBurster < Strategy::Role::SoloBurster
  include Strategy::Role::Rogue 

  def hierarhy_skill
    solo_burst  || solo_ongoing || waiting || attack || perception || defence
  end

  register :rogue_burster
end
#gems/../lib/strategy/role/rogue_ongoiner.rb
class Strategy::Role::RogueOngoiner < Strategy::Role::SoloOngoiner
  include Strategy::Role::Rogue 

  def hierarhy_skill
    solo_ongoing || waiting || attack || perception || defence
  end

  register :rogue_ongoiner
end
#gems/../lib/strategy/area.rb
class Strategy::Area < Strategy
  def target
    $game_troop.alive_members.any?(&:melee_attack?) ? front_target : visible_taget
  end
end
#gems/../lib/strategy/slice.rb
class Strategy::Slice < Strategy::Area
  def self.success_index
    Strategy::Rate.success_rate do
      prefer :area_ongoiner
      secondary :area_controller, :healer, :tank
    end
  end

  def current_phase
    :area_ongoing
  end

  def set_roles
    super
    at_least 2, :area_ongoiner
    at_least 2, :area_controller
    at_least 1, :healer
    at_least 1, :tank
    rest_to :area_ongoiner
  end

  def roles
    %w(
      area_ongoiner area_controller
      area_burster healer tank
      solo_burster solo_ongoiner solo_controller
    ).map(&:to_sym)
  end

  def damage_dealers
    $game_troop.alive_members.select { |member| member.tag? :area_ongoiner }
  end

  register
end
#gems/../lib/strategy/cleave.rb
class Strategy::Cleave < Strategy::Area
  def self.success_index
    Strategy::Rate.success_rate do
      prefer :area_burster
      progressive :area_burster, max: 2
      secondary :area_controller, :healer
    end
  end

  def set_roles
    super
    at_least 2, :area_burster
    at_least 1, :area_controller
    rest_to :area_burster
  end

  def damage_dealers
    $game_troop.alive_members.select { |member| member.tag? :area_burster }
  end

  private

  def tp_or_mp_gain_skill
    area_skills.select(&recharging).max_by(&damage)
  end

  def cheap_skill
    area_skills.select(&cheap).max_by(&damage)
  end

  def switch_if_needed
    if @current_phase == :waiting
      @current_phase = :area_burst if %w(tp mp energy).all? { |key| cost_ok? key, :area_burst }
    else
      @current_phase = :waiting if %w(tp mp energy).any? { |key| cost_low? key, :area_burst }
    end
  end

  def roles
    %w(
      area_burster area_controller
      area_ongoiner healer tank
      solo_burster solo_ongoiner solo_controller
    ).map(&:to_sym)
  end

  register
end
#gems/../lib/strategy/solo.rb
class Strategy::Solo < Strategy
  private

  def target
    damage_dealers.any?(&:melee?) ? front_target : visible_taget
  end
end
#gems/../lib/strategy/fist.rb
class Strategy::Fist < Strategy::Solo
  def self.success_index
    Strategy::Rate.success_rate do
      prefer :solo_ongoiner, :rogue_ongoiner
      secondary :controller, :healer, :tank
    end
  end

  def set_roles
    super
    at_least 3, :solo_ongoiner
    at_least 2, :rogue_ongoiner
    at_least 1, :healer
    at_least 1, :tank
    rest_to :solo_ongoiner, :rogue_ongoiner
  end

  def current_phase
    :solo_ongoing
  end

  private

  def damage_dealers
    $game_troop.alive_members.select do |member|
      member.any_tag? :solo_ongoiner, :rogue_ongoiner
    end
  end

  def roles
    %w(
      solo_ongoiner solo_burster
      healer tank area_controller solo_controller
      area_ongoiner area_burster
    ).map(&:to_sym)
  end

  register
end
#gems/../lib/strategy/needle.rb
class Strategy::Needle < Strategy::Solo
  def self.success_index
    Strategy::Rate.success_rate do
      prefer :solo_burster, :rogue_burster
      progressive :solo_burster, max: 3
      progressive :rogue_burster, max: 2
      secondary :controller, :healer, :tank
    end
  end

  def set_roles
    super
    at_least 3, :solo_burster
    at_least 2, :rogue_burster
    at_least 1, :healer
    at_least 1, :tank
    rest_to :solo_burster, :rogue_burster
  end

  private

  def damage_dealers
    $game_troop.alive_members.select do |member|
      member.any_tag? :solo_burster, :rogue_burster
    end
  end

  def switch_if_needed
    if @current_phase == :waiting
      @current_phase = :solo_burst if %w(tp energy mp).all? { |key| cost_ok? key, :solo_burst }
    else
      @current_phase = :waiting if %w(tp energy mp).any? { |key| cost_low? key, :solo_burst }
    end
  end

  def roles
    %w(
      solo_burster solo_ongoiner
      healer tank area_controller solo_controller
      area_burster area_ongoiner
    ).map(&:to_sym)
  end

  register
end
#lib/summoning.rb
module Summoning
  #resurect real battlers if any summons alive and all real battlers dead
  SOFT_MODE = true
  ORIGINAL_SUMMONS = (16..20).to_a

  class << self
    attr_writer :ids

    def add(summon)
      if id = summon.is_a?(Integer) ? summon : $game_actors.index(summon)
        self.ids += id
        self.ids.uniq!
      end
    end

    def summons
      ids.map { |id| $game_actors[id] }
    end

    def summon?(actor)
      ids.include? actor.id
    end

    def ids
      @ids || ORIGINAL_SUMMONS
    end
  end
end

#gems/../lib/summoning/vocab.rb
#encoding=utf-8
module Summoning::Vocab
  def lines
    [first_line, second_line, third_line, to_battle]
  end

  def first_line
    'Линия плотного боя.'
  end

  def second_line
    <<-TEXT
      Воины первой линии
      прикрывают вторую линию
      от атак плотного боя.
    TEXT
  end

  def third_line
    <<-TEXT
      Воины первой и второй
      линии прикрывают
      третью линию от 
      прямых атак.
    TEXT
  end

  def to_battle
    'В бой'
  end

  extend self
end
#gems/../lib/summoning/summoner.rb
class Summoning::Summoner
  class << self
    def summon(matrix)
      instance = new matrix
      instance.summon
      instance.place
    end

    def release
      $game_party.members.each do |member|
        if member.summon?
          member.on_battle_end
          $game_party.remove_actor member.id
        end
      end
    end
  end

  def initialize(matrix)
    @matrix = matrix
  end

  def summon
    @matrix.flatten.compact.each.with_index do |actor, index|
      if actor.summon?
        actor.change_level $game_party.leader.level, false
        actor.recover_all
        $game_party.add_actor actor.id, index
      end
    end
  end

  def place
    $game_party.init_lines_matrix lines_matrix
  end

  def lines_matrix
    @matrix.map { |arr| arr.compact.map { |actor| $game_party.members.index actor } }
  end
end
#gems/../lib/summoning/window.rb
module Summoning::Window
end

#gems/../lib/summoning/window/base.rb
class Summoning::Window::Base < Window_Selectable
  attr_accessor :info

  def self.configure(hash)
    class_eval do
      hash.each do |method_name, value|
        define_method(method_name) { value }
      end
    end
  end

  def initialize(*args)
    super
    set_handler :ok, method(:on_ok) if respond_to? :on_ok
    set_handler :cancel, method(:on_cancel) if respond_to? :on_cancel
    hide
  end

  def setup
    refresh
    activate
    show
    open
  end

  def cursor_down(wrap = false)
    super
    alert
  end

  def cursor_up(wrap = false)
    super
    alert
  end
end
#gems/../lib/summoning/window/info.rb
class Summoning::Window::Info < Window_Base
  LINE_HEIGHT = 23

  def initialize(*args)
    super
    hide
  end

  def alert(text)
    contents.clear
    text.lines.each.with_index do |line, index|
      contents.draw_text(*sizes(index), line.strip)
    end
  end

  def sizes(index)
    [0, LINE_HEIGHT * index, contents.rect.width, LINE_HEIGHT * (index + 1)]
  end
end
#gems/../lib/summoning/window/list.rb
class Summoning::Window::List < Summoning::Window::Base
  attr_accessor :lines

  def initialize(*args)
    @items = []
    super
  end

  def item_max
    @items.size
  end

  def alert
    @items[index] && /<info>(.*)<\/info>/m =~ @items[index].actor.note
    info.alert $1 || ''
  end

  def names
    ['None'] + @items[1..-1].map(&:name)
  end

  def on_ok
    lines.set @items[index]
    close
    lines.setup
  end

  def on_cancel
    close
    lines.setup
  end

  def draw_item(index)
    draw_text item_rect(index), names[index]
  end

  def setup(items)
    @items = [nil] + items
    info.alert ''
    super()
    select 0
  end

  def refresh
    contents.clear
    draw_all_items
  end
end
#gems/../lib/summoning/window/lines.rb
class Summoning::Window::Lines < Summoning::Window::Base
  configure col_max: 2, item_max: 7, item_width: 96, item_height: 96
  attr_accessor :list, :scene

  def initialize(x, y, width, height)
    @matrix = Array.new(3) { Array.new(2) { nil } }
    @heroes, @summons = $game_party.members, Summoning.summons.uniq(&:name)
    super
  end

  def on_finish(&block)
    @on_finish = block
  end

  def set(actor)
    @matrix[index / 2][index % 2] = actor
  end

  def on_ok
    if to_battle? && full_party?
      close
      Summoning::Summoner.summon @matrix
      @on_finish.call
    else
      list.setup items
    end
  end

  def setup
    alert 0
    super
    select last_index
  end

  def last_index
    index > 0 ? index : 0
  end

  def alert(i = index)
    info.alert Summoning::Vocab.lines[i / 2]
  end

  def current_item_enabled?
    enabled? index
  end

  def draw_item(i)
    if to_battle? i
      draw_swords
    elsif @matrix.flatten[i]
      draw_actor i
    else
      draw_cross i
    end
  end

  def refresh
    create_contents
    draw_all_items
  end

  def open
    info.show.open
    super
  end

  def close
    info.close
    super
  end

  private

  def items
    if current_actor?
      [@matrix.flatten[index]] + rest_heroes
    elsif full_party?
      rest_summons
    else
      rest_heroes
    end
  end

  def enabled?(i)
    to_battle?(i) ? full_party? : @matrix.flatten[i] || empty_places?
  end

  def draw_cross(i)
    draw_icon Cache.system(full_matrix? ? 'grey_cross' : 'yellow_cross'), i
  end

  def draw_actor(i)
    actor = @matrix.flatten[i]
    rect = item_rect i
    draw_actor_face actor, rect.x, rect.y
  end

  def draw_swords
    draw_icon Cache.system(full_matrix? ? 'active_swords' : 'grey_swords'), 6
  end

  def draw_icon(bitmap, i)
    contents.stretch_blt item_rect(i), bitmap, bitmap.rect
  end

  def current_actor?
    @heroes.include? @matrix.flatten[index]
  end

  def rest_heroes
    @heroes - @matrix.flatten
  end

  def rest_summons
    @summons - @matrix.flatten
  end

  def full_party?
    (@heroes - @matrix.flatten).none?
  end

  def full_matrix?
    full_party? && !empty_places?
  end

  def empty_places?
    @matrix.flatten.compact.size < 4
  end

  def to_battle?(i = index)
    i == 6
  end
end
#gems/../lib/summoning/patch.rb
#gems/../lib/summoning/patch/data_manager_patch.rb
module DataManager
  instance_eval do
    alias make_save_contents_for_summoning make_save_contents
    def make_save_contents
      make_save_contents_for_summoning.tap do |contents|
        contents[:summons] = Summoning.ids
      end
    end

    alias extract_save_contents_for_summoning extract_save_contents
    def extract_save_contents(contents)
      extract_save_contents_for_summoning contents
      Summoning.ids = contents[:summons]
    end
  end
end
#gems/../lib/summoning/patch/scene_battle_patch.rb
class Scene_Battle
  alias create_all_windows_original_for_summoning create_all_windows
  def create_all_windows
    create_all_windows_original_for_summoning
    create_summoning_window
  end

  def create_summoning_window
    w, h = Graphics.width / 2, Graphics.height
    @summoning_lines_window = Summoning::Window::Lines.new 0, 0, w, h
    @summoning_list_window = Summoning::Window::List.new w, 0, w, h/ 2
    @summoning_info_window = Summoning::Window::Info.new w, h / 2, w, h / 2
    @summoning_lines_window.list = @summoning_list_window
    @summoning_lines_window.info = @summoning_info_window
    @summoning_list_window.lines = @summoning_lines_window
    @summoning_list_window.info = @summoning_info_window
  end

  def battle_start
    BattleManager.battle_start
    process_event
    start_summoning
  end

  def start_summoning
    @summoning_lines_window.on_finish do
      start_party_command_selection
    end
    @summoning_lines_window.setup
  end
end
#gems/../lib/summoning/patch/game_actor_patch.rb
class Game_Actor
  def summon?
    Summoning.summon? self
  end
end
#gems/../lib/summoning/patch/battle_manager_patch.rb
module BattleManager
  instance_eval do
    def need_summon_release?
      @phase && (
        $game_party.members.empty? || $game_party.all_dead? ||
        $game_troop.all_dead? || aborting?
      )
    end

    def need_resurect?
      return false unless Summoning::SOFT_MODE
      summons, battlers = $game_party.alive_members.each_with_object([[], []]) do |member, result|
        (member.summon? ? result[0] : result[1]) << member
      end
      summons.any? && battlers.empty?  
    end

    def resurect_real_battlers
      $game_party.dead_members.each do |member|
        member.remove_state 1 if member.dead? && !member.summon?
      end
    end

    alias judge_win_loss_for_summoning judge_win_loss
    def judge_win_loss
      if need_summon_release?
        resurect_real_battlers if need_resurect?
        Summoning::Summoner.release 
      end
      judge_win_loss_for_summoning
    end
  end
end
#gems/../lib/summoning/patch/game_party_patch.rb
class Game_Party
  def add_actor(actor_id, index = nil)
    index = index || @actors.length
    
    unless @actors.include?(actor_id)
      existing = @actors[index]
      @actors[index] = actor_id 
      @actors.push existing
      @actors.compact!
    end

    $game_player.refresh
    $game_map.need_refresh = true
  end
end
#lib/target_manager.rb
class TargetManager
  def initialize(actor, target, spell)
    @actor, @target, @spell = actor, target, spell
  end

  def can_hit?
    @target && (area? || (!alive_and_transparent? && (melee? || range?)))
  end

  private

  def alive_and_transparent?
    @target.transparent? && @target.alive?
  end

  def range?
    !@spell.melee? && distance <= max_distance_for_range
  end

  def max_distance_for_range
    @actor.line.is_a?(Line::Second) ? 3 : 2
  end

  def distance
    @actor.distance_to @target
  end

  def area?
    @spell.for_all?
  end

  def melee?
    @spell.melee? && distance <= 1
  end
end
#lib/ticker.rb
#Ticker
#Allows:
#a) Call block of code in timeout in frames
#b) Track object so #tick method in the object will be called on every frame
#author: Iren_Rin
#restrictions of use: none
#How to use
#a) In any object 
# timeout_in_frames = 80
# Ticker.delay timeout_in_frames do
#   puts 'finally'
# end
#b) In any object with #tick method inside
# Ticker.track self #tick method will be called at every frame
#
#Track and Delay queues will be flush during switches between some scenes.
#There are three flush strategies
#a) :soft - queues flush during switching to any scene
#b) :middle - queues flush during switching to Scene_Title, Scene_End, Scene_Gameover and Scene_Battle
#c) :hard - queues flush during switching to Scene_Title and Scene_Gameover
#By default Ticker.delay uses :middle strategy and
#Ticker.track uses :hard strategy
#You can change it with following
#a)
# timeout_in_frames = 80
# Ticker.delay timeout_in_frames, :hard do
#   puts 'finally'
# end
#b)
# Ticker.track self, :soft
#
module Ticker
  FLUSH_STRATEGIES = Hash.new [Scene_Base]
  FLUSH_STRATEGIES[:hard] = [Scene_Title, Scene_Gameover]
  FLUSH_STRATEGIES[:middle] = [Scene_Title, Scene_End, Scene_Gameover, Scene_Battle]

  def current_klass=(klass)
    queue[klass] ||= []
    tracked[klass] ||= []
    @current_klass = klass 
  end

  def track(object, strategy = :hard)
    unless tracked[@current_klass].include? [object, strategy]
      tracked[@current_klass] << [object, strategy]
    end
  end

  def untrack(object)
    tracked.each { |klass, arr| arr.reject! { |arr| arr[0] == object } }
  end

  def delay(frames, strategy = :middle, &job)  
    queue[@current_klass] << [frames, strategy, job]
  end

  def tick
    queue[@current_klass].each do |arr| 
      arr[0] -= 1       
      arr[2].call if arr[0] <= 0 
    end
    clear_queue 
    tracked[@current_klass].each do |arr|
      arr[0].tick if arr[0].respond_to? :tick
    end
  end

  def flush
    b = proc do |arr| 
      (FLUSH_STRATEGIES[arr[1]] & @current_klass.ancestors).any?
    end
    tracked.each { |klass, arr| arr.reject!(&b) }
    queue.each { |klass, arr| arr.reject!(&b) }
  end

  def queue
    @queue ||= {}
  end

  def tracked
    @tracked ||= {}
  end

  def clear_queue
    queue.each { |klass, arr| arr.reject! { |arr| arr[0] <= 0 } }
  end

  extend self
end

class Scene_Base
  alias original_start_for_ticker start 
  def start
    Ticker.current_klass = self.class
    Ticker.flush
    original_start_for_ticker
  end

  alias original_update_basic_for_ticker update_basic
  def update_basic
    original_update_basic_for_ticker
    Ticker.tick
  end

  alias original_terminate_for_ticker terminate
  def terminate
    original_terminate_for_ticker
    Ticker.flush
  end
end
#patches/battle_manager_patch.rb
module BattleManager
  instance_eval do
    alias original_process_victory process_victory
    def process_victory
      victory_pose $game_party.battle_members
      original_process_victory
    end

    alias original_process_defeat process_defeat
    def process_defeat
      victory_pose $game_troop.members
      original_process_defeat
    end

    private

    def victory_pose(battlers)
      battlers.each { |battler| battler.pose = :victory unless battler.dead? }
    end
  end
end
#patches/effect_patch.rb
class RPG::UsableItem::Effect
  attr_accessor :direct

  def direct?
    defined?(@direct) ? !!@direct : true
  end
end
#patches/game_action_patch.rb
class Game_Action
  def set_strategy_action(hash)
    set_skill hash[:skill].id
    self.target_index = hash[:target_index] if hash[:target_index]
  end

  def decide_random_target
    if item.for_dead_friend?
      target = until_reachable { friends_unit.random_dead_target }
    elsif item.for_friend?
      target = until_reachable { friends_unit.random_target }
    else
      target = until_reachable { opponents_unit.random_target }
    end
    if target
      @target_index = target.index
    else
      clear
    end
  end

  alias confusion_target_original confusion_target
  def confusion_target
    until_reachable { confusion_target_original }
  end

  def targets_for_opponents
    if item.for_random?
      Array.new(item.number_of_targets) { until_reachable { opponents_unit.random_target } }
    elsif item.for_one?
      num = 1 + (attack? ? subject.atk_times_add.to_i : 0)
      add_if_piercing do
        if @target_index < 0
          [until_reachable { opponents_unit.random_target }] * num
        else
          [if_reachable { opponents_unit.smooth_target(@target_index) }] * num
        end
      end
    else
      opponents_unit.alive_members
    end
  end

  def targets_for_friends
    if item.for_user?
      [subject]
    elsif item.for_dead_friend?
      if item.for_one?
        [if_reachable { friends_unit.smooth_dead_target(@target_index) }]
      else
        friends_unit.dead_members
      end
    elsif item.for_friend?
      if item.for_one?
        add_if_piercing { [if_reachable { friends_unit.smooth_target(@target_index) }] }
      else
        friends_unit.alive_members
      end
    end
  end

  alias item_target_candidates_original item_target_candidates
  def item_target_candidates
    item_target_candidates_original.select { |battler| subject.can_hit? target, with: item }
  end

  alias make_targets_original make_targets

  def make_targets
    make_targets_original.compact
  end

  private

  def add_if_piercing
    targets = yield
    if item.piercing?
      targets.compact.map { |target| [target] + target.battlers_behind }.flatten
    else
      targets
    end
  end

  def until_reachable
    count = 0
    while (target = yield) && (count < 100)
      count += 1
      break target if subject.can_hit? target, with: item
    end
  end

  def if_reachable
    target = yield
    target if subject.can_hit? target, with: item
  end
end
#patches/game_action_result_patch.rb
class Game_ActionResult
  attr_accessor :transfered_damage, :transfered_damage_target

  alias clear_damage_values_original clear_damage_values
  def clear_damage_values
    clear_damage_values_original
    @transfered_damage = 0
    @transfered_damage_target = nil
  end
end
#patches/game_actor_patch.rb
class Game_Actor

  def rpg_object
    actor
  end

  def use_sprite?
    true
  end

  def sprite_name
    actor.sprite_name
  end

  def perform_damage_effect
    @sprite_effect_type = :blink
    Sound.play_actor_damage
  end

  def skill_atype_ok?(skill)
    atype_id1 = skill.required_atype_id1
    atype_id2 = skill.required_atype_id2
    return true if atype_id1 == 0 && atype_id2 == 0
    return true if atype_id1 > 0 && atype_equipped?(atype_id1)
    return true if atype_id2 > 0 && atype_equipped?(atype_id2)
    false
  end

  def atype_equipped?(atype_id)
    armors.any? {|armor| armor.atype_id == atype_id }
  end

  def check_damage_value(user, item)
    value = item.damage.eval(user, self, $game_variables)
    value *= item_element_rate(user, item)
    value *= pdr if item.physical?
    value *= mdr if item.magical?
    value *= rec if item.damage.recover?
    apply_guard(value)
  end
end
#patches/game_battler_base_patch.rb
class Game_BattlerBase
  def skill_atype_ok?(skill)
    true
  end

  alias skill_conditions_met_original skill_conditions_met?
  def skill_conditions_met?(skill)
    skill_conditions_met_original(skill) && skill_atype_ok?(skill)
  end

  alias clear_states_original clear_states
  def clear_states
    clear_states_original
    @states_casters = {}
  end

  alias erase_state_original erase_state
  def erase_state(state_id)
    erase_state_original state_id
    @states_casters.delete state_id
  end

  def state_caster(state_id)
    state_id = state_id.is_a?(Integer) ? state_id : state_id.id
    @states_casters[state_id]
  end

  def attack_skill_id
    skill = skills.find { |skill| skill.tag? :attack_skill }
    (skill && skill.id) || 1
  end
end
#patches/game_battler_patch.rb
class Game_Battler
  EFFECT_TO_METHOD = {
    EFFECT_RECOVER_HP    => :item_effect_recover_hp,
    EFFECT_RECOVER_MP    => :item_effect_recover_mp,
    EFFECT_GAIN_TP       => :item_effect_gain_tp,
    EFFECT_ADD_STATE     => :item_effect_add_state,
    EFFECT_REMOVE_STATE  => :item_effect_remove_state,
    EFFECT_ADD_BUFF      => :item_effect_add_buff,
    EFFECT_ADD_DEBUFF    => :item_effect_add_debuff,
    EFFECT_REMOVE_BUFF   => :item_effect_remove_buff,
    EFFECT_REMOVE_DEBUFF => :item_effect_remove_debuff,
    EFFECT_SPECIAL       => :item_effect_special,
    EFFECT_GROW          => :item_effect_grow,
    EFFECT_LEARN_SKILL   => :item_effect_learn_skill,
    EFFECT_COMMON_EVENT  => :item_effect_common_event,
  }

  attr_writer :pose

  def effective_hp(kind, element_id = nil)
    kind.to_s =~ /magic/i ? hp * mdf : hp * self.def
  end

  def damage_messages
    @damage_messages ||= []
  end

  def can_hit?(target, args)
    TargetManager.new(self, target, args[:with]).can_hit?
  end

  def distance_to(target)
    Line::Distance.new(self, target).distance
  end

  def line_set
    friends_unit.line_set
  end

  def line
    line_set.line_with self
  end

  def transparent?
    states.include?(RPG::State.transparent) || dead?
  end

  def observable?
    states.include? RPG::State.observable
  end

  def visible?
    !transparent?
  end

  alias original_on_turn_end on_turn_end
  def on_turn_end
    original_on_turn_end
    set_nonaction_pose
  end

  alias original_on_action_end on_action_end
  def on_action_end
    original_on_action_end
    set_nonaction_pose
  end

  alias on_battle_start_original on_battle_start
  def on_battle_start
    Ticker.delay 30 do
      switch_pose :battle_start
    end
    on_battle_start_original
  end

  def item_effect_apply(user, item, effect)
    actor, subject = effect.direct? ? [self, user] : [user, self]

    if method_name = effect_to_method(effect.code)
      actor.__send__ method_name, subject, item, effect
    end
  end

  def effect_to_method(code)
    EFFECT_TO_METHOD.merge(@additional_effect_methods || {})[code]
  end

  def battlers_behind
    if friends_unit.line_set
      friends_unit.line_set.battlers_behind self
    else
      []
    end
  end

  def item_effect_add_state_normal(user, item, effect)
    chance = effect.value1
    chance *= state_rate(effect.data_id) if opposite?(user)
    chance *= luk_effect_rate(user)      if opposite?(user)
    if rand < chance
      add_state effect.data_id, user
      @result.success = true
    end
  end

  def add_state(state_id, caster = nil)
    if state_addable?(state_id)
      add_new_state(state_id, caster) unless state?(state_id)
      reset_state_counts(state_id)
      @result.added_states.push(state_id).uniq!
    end
  end

  def add_new_state(state_id, caster = nil)
    die if state_id == death_state_id
    @states.push(state_id)
    @states_casters[state_id] = caster
    on_restrict if restriction > 0
    sort_states
    refresh
  end

  alias original_refresh_for_poses refresh
  def refresh
    original_refresh_for_poses
    set_nonaction_pose
  end

  def make_damage_value(user, item)
    value = item.damage.eval(user, self, $game_variables)
    value *= item_element_rate(user, item)
    value *= pdr if item.physical?
    value *= mdr if item.magical?
    value *= rec if item.damage.recover?
    value = apply_critical(value) if @result.critical
    value = apply_variance(value, item.damage.variance)
    value = apply_guard(value)
    value, @result.transfered_damage, @result.transfered_damage_target = apply_watch(value)
    @result.make_damage(value.to_i, item)
  end

  def apply_watch(damage)
    if damage > 0 && states.include?(RPG::State.watched) && state_caster(RPG::State.watched).alive?
      [damage * 0.7, (damage * 0.3).round, state_caster(RPG::State.watched)]
    else
      [damage, nil, nil]
    end
  end

  def apply_guard(damage)
    damage / (damage > 0 && guard? ? 1.2 * grd : 1)
  end

  alias execute_damage_original execute_damage
  def execute_damage(user)
    execute_damage_original user
    if @result.hp_damage > 0
      animate_damage
    else
      set_nonaction_pose
    end
    if @result.transfered_damage && @result.transfered_damage_target
      @result.transfered_damage_target.hp -= @result.transfered_damage
      @result.transfered_damage_target.set_nonaction_pose if @result.transfered_damage_target.dead?
    end
  end

  def pose
    @pose || :iddle
  end

  def low_hp?
    hp < mhp * 0.3
  end

  def animate_damage
    switch_pose(:taking_damage) unless dead?
  end

  def recover_all
    super
    set_nonaction_pose
  end

  def set_nonaction_pose
    @pose = if dead?
      :dead
    elsif guard?
      :guard
    elsif low_hp?
      :dying
    else
      :iddle
    end
  end

  def switch_pose(pose)
    @pose = pose
    Ticker.delay 30 do
      set_nonaction_pose
    end
  end

  alias original_die_for_patch die 
  def die
    original_die_for_patch
    set_nonaction_pose
  end
end
#patches/game_enemy_patch.rb
class Game_Enemy
  attr_accessor :current_role
  attr_accessor :index

  %w(skills sprite_name lines_priority).each do |method_name|
    define_method method_name do |*args|
      enemy.public_send(method_name, *args)
    end
  end

  (Strategy::Role.roles.keys + %w(range melee magical martial)).each do |key|
    define_method "#{key}?" do
      tag? key
    end
  end

  def rpg_object
    enemy
  end

  def melee_attack?
    $data_skills[attack_skill_id].melee?
  end

  def make_actions
    super
    return if @actions.empty?
    @actions.each do |action|
      action.set_strategy_action($game_troop.current_strategy.action_for(self))
    end
  end
end
#patches/game_followers_patch.rb
class Game_Followers
  def visible_followers
    visible_folloers
  end
end
#patches/game_troop_patch.rb
class Game_Troop
  def current_strategy
    @current_strategy ||= Strategy.best
  end

  def make_actions
    current_strategy.set_roles
    super
  end

  def lines_matrix
    current_strategy.lines_matrix
  end

  def clear_strategy
    @current_strategy = nil
    @line_set = nil
  end

  def sort_by_priority!
    new_enemies = @enemies.sort do |l, r|
      if l.lines_priority[0] != r.lines_priority[0]
        l.lines_priority[0] <=> r.lines_priority[0]
      elsif l.lines_priority[1] != r.lines_priority[1]
        l.lines_priority[1] <=> r.lines_priority[1]
      else
        l.lines_priority[2] <=> r.lines_priority[2]
      end
    end

    unless new_enemies == @enemies
      @enemies = new_enemies
      @enemies.each.with_index { |e, index| e.index = index }
    end
  end
end
#patches/game_unit_patch.rb
class Game_Unit
  def init_lines_matrix(matrix)
    @lines_matrix = matrix
    @line_set = nil
  end

  def line_set
    @line_set ||= init_line_set
  end

  def get_line_set
    @line_set
  end

  def smooth_target(index)
    member = members[index]
    (member && member.alive? && !member.transparent?) ? member : first_visible
  end

  def first_visible
    flatten_matrix = lines_matrix.flatten
    alive_members.reject(&:transparent?).sort do |b1, b2|
      flatten_matrix.index(b1.index) <=> flatten_matrix.index(b2.index)
    end.first
  end

  private

  def init_line_set
    LineSet.new(half).tap do |ls|
      lines_matrix.each.with_index do |arr, line_index|
        arr.each { |i| ls.lines[line_index].add members[i] if members[i] }
      end
    end
  end

  def lines_matrix
    @lines_matrix || [[0, 1], [2], [3]]
  end

  def half
    if set = $game_troop.get_line_set || $game_party.get_line_set
      set.half == 1 ? 2 : 1
    else
      (1..2).to_a.sample
    end
  end
end
#patches/object_patch.rb
class Object
  def try(*args, &block)
    public_send(*args, &block) if respond_to?(args[0])
  end
end
#patches/pose_patch.rb
class RPG::Skill
  attr_writer :pose

  def pose
  	@pose || default_pose
  end

  private

  def default_pose
  	if stype_id == 2
  	  :casting
  	elsif id == 1
  	  :attacking
    elsif id == 2
      :guard
  	else
  	  :skill
  	end
  end
end

class RPG::Item
  attr_writer :pose

  def pose
  	@pose || :casting
  end
end
#patches/rpg_actor_patch.rb
class RPG::Actor
  attr_accessor :sprite_name
  include Nottable
end
#patches/rpg_enemy_patch.rb
class RPG::Enemy
  attr_writer :skills
  attr_accessor :sprite_name

  def skills
    @skills ||= []
  end

  def lines_priority=(*lines)
  	@line_priority = lines.flatten
  end

  def lines_priority
  	@line_priority || [1, 2, 3]
  end
end
#patches/scene_battle_patch.rb
class Scene_Battle

  alias original_terminate_for_patch terminate
  def terminate
    $game_troop.clear_strategy
    original_terminate_for_patch
  end

  alias original_battle_start_for_patch battle_start 
  def battle_start
    $game_troop.clear_strategy
    original_battle_start_for_patch
  end

  def command_attack
    BattleManager.actor.input.set_attack
    @enemy_window.current_item = $data_skills[BattleManager.actor.attack_skill_id]
    select_enemy_selection
  end

  alias original_on_skill_ok on_skill_ok
  def on_skill_ok
    @actor_window.current_item = @enemy_window.current_item = @skill_window.item
    @skill_window.hide
    original_on_skill_ok
  end

  alias original_on_item_ok on_item_ok
  def on_item_ok
    @actor_window.current_item = @enemy_window.current_item = @item_window.item
    original_on_item_ok
  end

  alias original_invoke_counter_attack invoke_counter_attack
  def invoke_counter_attack(target, item)
    target.switch_pose :counter_attack
    original_invoke_counter_attack target, item
  end

  alias original_invoke_magic_reflection invoke_magic_reflection
  def invoke_magic_reflection(target, item)
    target.switch_pose :magic_reflection
    original_invoke_magic_reflection target, item
  end

  def with_item_pose(item)
    if item.is_a?(RPG::Skill) && item.id == 2
      yield
    else
      @subject.pose = item.pose
      yield
      @subject.set_nonaction_pose
    end
  end

  def use_item
    item = @subject.current_action.item
    @log_window.display_use_item(@subject, item)
    @subject.use_item(item)
    refresh_status
    targets = @subject.current_action.make_targets.compact

    if item.animations.any?
      with_item_pose item do
        show_animation [@subject], item.animations[:caster] if item.animations[:caster]
      end
      show_animation targets, item.animations[:target] if item.animations[:target]
    else
      with_item_pose(item) { show_animation(targets, item.animation_id) }
    end
    targets.each {|target| item.repeats.times { invoke_item(target, item) } }
  end

  def show_animation(targets, animation_id)
    if animation_id < 0
      show_attack_animation(targets)
    else
      show_normal_animation(targets, animation_id)
    end
    wait_for_animation
  end

  def show_normal_animation(targets, animation_id, mirror = false)
    animation = $data_animations[animation_id]
    if animation
      targets.each do |target|
        target.animation_id = animation_id
        target.animation_mirror = mirror
      end
    end
    abs_wait_short
  end

  def invoke_item(target, item)
    if rand < target.item_cnt(@subject, item)
      invoke_counter_attack(target, item)
    elsif rand < target.item_mrf(@subject, item)
      invoke_magic_reflection(target, item)
    elsif item.for_opponent? && target.states.include?(RPG::State.shield_block) && rand(10) == 0
      target.switch_pose :block
      @log_window.display_shield_block target
    else
      apply_item_effects(apply_substitute(target, item), item)
    end
    @subject.last_target_index = target.index
  end

  def show_attack_animation(targets)
    Sound.play_enemy_attack
    abs_wait_short
  end

  alias original_process_action_end process_action_end
  def process_action_end
    original_process_action_end
    wait 60
  end
end
#patches/screen_coordinatest_patch.rb
[Game_Actor, Game_Enemy].each do |klass|
  klass.class_eval do
    def screen_y
      line_set.slot_with(self).y
    end

    def screen_x
      line_set.slot_with(self).x
    end

    def screen_z
      friends_unit.members.size - index.to_i   
    end
  end
end
#patches/skill_and_item_patch.rb
module MeleePatch
  def melee?
    tags.include?(:melee) || !range?
  end

  def range?
    tags.include? :range
  end
end

module PiercingPatch
  def piercing?
    tags.include? :piercing
  end
end

module RequiredAtypes
  attr_writer :required_atype_id1, :required_atype_id2

  def required_atype_id1
    @required_atype_id1.to_i
  end

  def required_atype_id2
    @required_atype_id2.to_i
  end
end

module ItemStates
  def states
    effects.select { |effect| effect.code == 21 }.map do  |effect|
      $data_states[effect.data_id]
    end
  end
end

module AnimationsPatch
  def animations
    @animations ||= {}
  end
end

[RPG::Skill, RPG::Item].each do |klass|
  [MeleePatch, RequiredAtypes, PiercingPatch, ItemStates, AnimationsPatch].each do |patch|
    klass.__send__ :include, patch
  end
end

RPG::Class.__send__ :include, MeleePatch
#patches/spriteset_battle_patch.rb
class Spriteset_Battle
  def self.cursors
    @cursors ||= {}
  end

  def cursors
    self.class.cursors
  end

  alias original_initialize initialize
  def initialize(*args)
    @cursors = {}
    create_cursor :red
    create_cursor :green
    create_cursor :blue
    original_initialize(*args)
  end

  def create_enemies
    @enemy_sprites = create_animated_sprites $game_troop
  end

  def create_actors
    @actor_sprites = create_animated_sprites $game_party
  end

  def update_actors
    if $game_party.members.size != @actor_sprites.size
      dispose_actors
      dispose_hp_bars
      create_hp_bars
      create_actors
    end
    @actor_sprites.each { |sprite| sprite.update }
  end

  alias original_dispose dispose
  def dispose
    dispose_cursor :red
    dispose_cursor :green
    dispose_cursor :blue
    original_dispose
  end

  alias original_update update
  def update
    update_cursor :red
    update_cursor :green
    update_cursor :blue
    original_update
  end

  private

  def create_cursor(color)
    cursors[color] = Cursor.new @viewport2, color
  end

  def update_cursor(color)
    cursors[color].update
  end

  def dispose_cursor(color)
    cursors[color].dispose
  end

  def battlers
    $game_troop.members + $game_party.members
  end

  def create_animated_sprites(game_unit)
    game_unit.members.reverse.map { |actor| Sprite_AnimatedBattler.new @viewport2, actor }
  end
end
#patches/sprite_battler_patch.rb
class Sprite_Battler
  def update
    super
  end
end
#patches/state_patch.rb
class RPG::State
  class << self
    { transparent: 26, watched: 28, shield_block: 27, observable: 29 }.each do |state_name, data_id|
      define_method(state_name) { $data_states[data_id] }
    end
  end
end
#patches/taggable_patch.rb
module Taggable
  def self.included(klass)
    klass.class_eval { attr_writer :tags }
  end

  def tags
    @tags ||= []
  end

  def tag?(*syms)
    (to_tags(syms) - tags).empty?
  end
  alias tags? tag?

  def any_tag?(*syms)
    (tags & to_tags(syms)).any?
  end

  def to_tags(strings)
    strings.map(&:to_sym)
  end
end

[
  RPG::Enemy, RPG::Skill,
  RPG::Item, RPG::Class,
  RPG::Actor
].each { |klass| klass.__send__ :include, Taggable }

[Game_Actor, Game_Enemy].each do |klass|
  klass.class_eval do
    %w(tags tag? any_tag?).each do |method_name|
      define_method method_name do |*args|
        rpg_object.public_send(method_name, *args)
      end
    end
  end
end
#patches/window_actor_command_patch.rb
class Window_ActorCommand
  alias original_setup setup
  def setup(actor)
  	cursor.mark actor
  	cursor.show
  	original_setup actor
  end

  alias original_close close
  def close
  	original_close
  	cursor.hide
  end

  private

  def cursor
  	Spriteset_Battle.cursors[:green]
  end
end
#patches/window_base_patch.rb
class Window_Base
  def draw_actor_name(actor, x, y, width = 112, enabled = true)
    change_color(hp_color(actor), enabled)
    draw_text(x, y, width, line_height, actor.name)
  end

  def update_tone
  end
end
#patches/window_battle_actor_patch.rb
class Window_BattleActor
  attr_accessor :current_item

  def friends
    $game_party.battle_members
  end

  def friend
    @index && friends[@index]
  end

  def item_max
    friends.size
  end

  def current_item_enabled?
    friend && enable?(friend)
  end

  def enable?(friend)
    if current_actor = BattleManager.actor
      current_actor.can_hit? friend, with: current_item
    else
      false
    end
  end

  def select(index)
    cursor.mark $game_party.members[index]
    cursor.show
    super
  end

  alias original_hide hide
  def hide
    cursor.hide
    original_hide
  end

  def draw_item(index)
    friend = friends[index]
    draw_basic_area basic_area_rect(index), friend, enable?(friend)
    draw_gauge_area gauge_area_rect(index), friend
  end

  def cursor
    Spriteset_Battle.cursors[:blue]
  end
end
#patches/window_battle_enemy_patch.rb
class Window_BattleEnemy
  attr_accessor :current_item

  def enemies
    $game_troop.alive_members
  end

  def enemy
    @index && enemies[@index]
  end

  def item_max
    enemies.size
  end

  def current_item_enabled?
    enemy && enable?(enemy)
  end

  def enable?(enemy)
    if current_enemy = BattleManager.actor
      current_enemy.can_hit? enemy, with: current_item
    else
      false
    end
  end

  def draw_item(index)
    enemy = enemies[index]
    change_color normal_color, enable?(enemy)
    draw_text item_rect_for_text(index), enemy.name
  end

  def select(index)
    cursor.mark enemies[index]
    cursor.show
    super
  end

  alias original_hide hide
  def hide
    cursor.hide
    original_hide
  end

  def cursor
    Spriteset_Battle.cursors[:red]
  end
end
#patches/window_battle_skill_patch.rb
class Window_BattleSkill
  def activate
    show
    super
  end
end
#patches/window_battle_status_patch.rb
class Window_BattleStatus
  def draw_basic_area(rect, actor, enabled = true)
    draw_actor_name(actor, rect.x + 0, rect.y, 100, enabled)
    draw_actor_icons(actor, rect.x + 104, rect.y, rect.width - 104)
  end
end
#patches/window_skill_list_patch.rb
class Window_SkillList
  alias original_include? include?
  def include?(item)
    original_include?(item) && item.id != @actor.attack_skill_id
  end
end
#dsls/skills/archer_skills.rb
#encoding=utf-8

SkillDsl.define 'archer' do
  skill 'Выстрел' do
    tp_gain 5
    id 147
    description 'Базовый выстрел'
    shared 'shot' do
      icon_index 133
      message 'shoots'
      hit_type physical
      shared 'range'
      scope enemy
      required_weapon bow
      required_weapon gun
      damage do
        critical true
        type to_hp
        element common
        formula 'a.atk * 2 + a.agi * 2 - b.def * 2'
      end
      tag :solo
      pose :attack
    end
    tags :tp_gain, :low_damage, :attack_skill
  end

  skill 'Прицельный' do
    description 'Прицельный выстрел. Наносит средний урон.'
    message 'shoots carefully'
    tp_cost 0
    id 153
    tags :normal_damage, :free
    shared 'shot'
    damage do
      formula 'a.atk * 2 + a.agi * 2 - b.def'
    end
  end

  skill 'Бронебой' do
    message 'shoots through amror'
    description 'Бронебойный выстрел. Наносит существенный урон.'
    id 149
    tp_cost 10
    shared 'shot'
    damage do
      formula 'a.atk * 2 + a.agi * 2'
    end
    tag :high_damage
    pose :skill
  end

  skill 'Blinding Shot' do
    message 'blinds with arrow'
    id 150
    tp_cost 20
    shared 'shot'
    effect do
      effect add_state
      data_id 3
      value1 1
    end
    tags :low_damage, :controll
  end

  skill 'Пронзатель' do
    message 'pierce with arrow'
    description <<-TEXT
Пробивает цель насквозь и поражает цель,
стоящую позади основной цели.
    TEXT
    shared 'shot'
    tp_cost 10
    id 151
    damage do
      formula 'a.atk * 2 + a.agi * 3 - b.def * 2'
    end
    tags :normal_damage, :piercing
    remove_tag :solo
    pose :skill
  end

  skill 'Burst Shot' do
    message 'burst with arrow'
    shared 'shot'
    tp_cost 10
    id 152
    scope all_enemies
    damage do
      formula 'a.atk + a.agi - b.def'
    end
    tags :low_damage, :area
    remove_tag :solo
    pose :skill
  end
end
#dsls/skills/mage_skills.rb
#encoding=utf-8

SkillDsl.define 'mage' do
  skill 'Водяная бомба' do
    message 'throws water bomb'
    id 167
    icon_index 99
    description 'Наносит средний урон по площади.'

    shared 'mage skill' do
      animation :caster, 112
      mp_cost 12
      icon_index 97
      shared 'range'
      shared 'magical'
      scope all_enemies

      damage do
        type to_hp
        element ice
        formula '4 * a.mat - 1.5 * b.mdf'
        critical true
      end

      tag :area
    end
    animation :caster, 113
    animation :target, 116

    tag :normal_damage

    damage do
      element water
    end
  end

  skill 'Метель' do
    message 'calls for blizzard'
    id 168
    icon_index 3
    description 'Наносит слабый урон по площади, ослепляет цели.'

    shared 'mage skill'
    shared 'mage controll' do
      damage do
        formula '3 * a.mat - b.mdf * 2'
      end
      tags :low_damage, :controll
    end

    effect do
      effect add_state
      data_id 3
      value1 0.5
    end

    pose :skill

    animation :target, 117
  end

  skill 'Ледяная бомба' do
    message 'throws ice bomb'
    description 'Наносит существенный урон по площади.'
    animation :target, 118
    id 169

    shared 'mage skill'
    mp_cost 60
    damage do
      formula '6 * a.mat - b.mdf'
    end
    pose :skill
    tag :high_damage
  end

  skill 'Град' do
    message 'attacks with heavy ice'
    id 170    
    animation :target, 119
    description 'Наносит слабый урон по площади, оглушает цели.'

    shared 'mage skill'
    shared 'stun effect'
    shared 'mage controll'
    expand_effect do
      value1 0.33
    end
    pose :skill

    mp_cost 18
  end
end
#dsls/skills/prepare.rb
#encoding=utf-8
SkillDsl.prepare do
  #speed
  #success_rate
  #repeats
  #variance

  #effects
  #code
  #data_id
  #value1
  #value2
  all do
    occasion only_in_battle
    damage do
      variance 20
    end
  end

  shared 'range' do
    tag :range
    remove_tag :melee
  end

  shared 'melee' do
    tag :melee
    remove_tag :range
    hit_type physical
  end

  shared 'magical' do
    stype_id magic
    hit_type magical
  end

  shared 'stun effect' do
    effect do
      effect add_state
      data_id 8
      value1 0.66
    end
  end

  shared 'perception' do
    description 'Обнаруживает скрытые цели'
    icon_index 121
    scope all_enemies

    tag :perception
    animation :caster, 122

    effect do
      effect remove_state
      data_id RPG::State.transparent.id
      value1 0.5
    end

    effect do
      effect add_state 
      data_id RPG::State.observable.id 
      value1 0.5
    end
  end

  #skill 'Strike' do
  #  shared 'melee'
  #  scope enemy
  #  tp_gain 5
  #  required_wtype_id1 hammer
  #  required_wtype_id2 gun
  #  damage do
  #    type to_hp
  #    element common
  #    formula 'a.atk * 4 - b.def * 2'
  #  end
  #end

  #skill 'Inferno' do
  #  shared 'range'
  #  shared 'magical'
  #  scope all_enemies
  #  mp_cost 20
  #  damage do
  #    type to_hp
  #    element fire
  #    formula 'a.mat * 3 - b.def * 2 + 100'
  #    critical true
  #  end
  #end
end
#dsls/skills/priest_skills.rb
#encoding=utf-8
SkillDsl.define 'priest' do

  skill 'Регенерация' do
    message 'call for regeneration power'
    description "Повышает скорость регенерации здоровья у союзника."
    mp_cost 12
    id 158
    shared 'regeneration' do
      icon_index 112
      shared 'range'
      scope ally
      stype magic
      animation :caster, 114
      animation :target, 120
      effect do
        effect add_state
        data_id 14
        value1 1
      end
      tag :hot
    end
    tag :solo
  end

  skill 'Общая регенерация' do
    description <<-TEXT
Повышает скорость регенерация здоровья у всех
союзников.
    TEXT
    id 162
    mp_cost 24
    shared 'regeneration'
    scope all_allies
    tag :area
  end

  skill 'Исцеление' do
    description 'Исцеляет союзника.'
    mp_cost 20
    id 157
    shared 'heal' do
      icon_index 112
      message 'healing'
      shared 'range'
      scope ally
      stype magic
      damage do
        type heal_hp
        formula 'b.mhp / 2.5'
      end
      tag :heal

      animation :target, 115
      animation :caster, 114
    end
    tag :solo
  end

  skill 'Общее исцеление' do
    description 'Исцеляет всех союзников.'
    id 160
    mp_cost 60
    shared 'group heal' do
      shared 'heal'
      scope all_allies
      damage do
        formula 'b.mhp / 2.5'
      end
    end
    tag :area
  end

  skill 'Очищение' do
    description 'Накладывает эффект регенерации и исцеляет союзника'
    id 159
    mp_cost 45
    shared 'regeneration'
    shared 'heal'
    tag :solo
  end

  skill 'Общее очищение' do
    description 'Накладывает эффект регенерации и исцеляет всех союзников'
    id 161
    mp_cost 100
    shared 'regeneration'
    shared 'group heal'
    tag :area
  end
end
#dsls/skills/rogue_skills.rb
#encoding=utf-8
SkillDsl.define 'rogue' do
  skill 'Зрение' do
    shared 'perception'
    energy_cost 10
    id 143
  end

  skill 'Стелс' do    
    description <<-TEXT
Скрывает вас в тени. Скрытую цель невозможно
атаковать. Полученный урон прерывает эффект.
    TEXT
    scope my_self
    id 137
    speed 100
    icon_index 12
    shared 'transparent effect' do
      effect do
        effect add_state
        data_id RPG::State.transparent.id
        value1 1
      end
      tag :stealth
    end
    tag :no_damage
    pose :skill
  end

  skill 'Вендета' do
    description 'Средний урон, средняя цена'
    message 'Hits Carefully'
    shared 'melee'
    id 142
    shared 'rogue hit with damage' do
      icon_index 131
      hit_type physical
      energy_cost Energy::Patch::RechargeRate::DEFAULT_RATE
      scope enemy
      required_weapon dagger
      damage do
        critical true
        type to_hp
        formula 'a.atk  + a.luk * 2'
      end
    end
    pose :attack
    tags :normal_damage, :free
  end

  skill 'Поцелуй аспида' do
    message 'kiss with poison'
    description 'Отравляет цель.'
    id 138
    damage do
      type debuff
      element none
    end
    energy_cost 5
    hit_type certain
    shared 'aspid kiss' do
      shared 'melee'
      scope enemy
      effect do
        effect add_state
        data_id 2
        value1 1
      end
      tag :poison
    end
    pose :attack
    tag :no_damage
  end

  skill 'Удар в спину' do
    id 139
    description 'Удар в спину, наносящй огромный урон.'
    shared 'backstab' do
      shared 'rogue hit with damage'
      message 'attacks behind'
      energy_cost 30
      damage do
        formula 'a.atk * 2 + a.luk * 3'
      end
      tag :high_damage
    end
    pose :attack
  end

  skill 'Шаг в тень' do
    shared 'backstab'
    description 'Удар в спину, который прячет вас в тень.'
    id 140
    energy_cost 35
    shared 'transparent effect'
    expand_effect do
      direct false
    end
    pose :skill
  end

  skill 'Отравленный удар' do
    shared 'aspid kiss'
    shared 'backstab'
    description 'Удар в спину, отравляющий цель'
    id 141
    energy_cost 35
    pose :attack
  end
end
#dsls/skills/warrior_skills.rb
#encoding=utf-8
SkillDsl.define 'warrior' do
  skill 'Зрение' do
    shared 'perception'
    id 133
  end

  skill 'Защитный удар' do
    message 'slams with shield'
    description "Атака и защита однавременно."
    speed 50
    tp_cost 0
    id 127

    shared 'shield strike ' do
      icon_index 127
      scope enemy
      shared 'melee'
      required_armor small_shield
      required_armor big_shield
      damage do
        type to_hp
        element common
        formula 'a.atk * 3 - b.def * 2'
        critical true
      end
      tags :low_damage, :solo
      pose :attack
    end

    shared 'protective effect' do
      effect do
        effect add_state
        data_id 9
        value1 1
        direct false
      end
      tags :protective, :short_time, :self
      pose :guard
    end
  end

  skill 'Оглушающий удар' do
    message 'bash with shield '
    description 'Наносит небольшой урон и оглушает врага'
    tp_cost 10
    id 128
    shared 'shield strike'
    shared 'stun effect'
    tag :controll
    pose :attack
  end

  skill 'Warrior Stump' do
    message 'stumps!'
    tp_cost 20
    id 129
    scope all_enemies
    damage do
      type to_hp
      formula 'a.atk - 0.5 * b.def'
    end
    shared 'stun effect'
    tags :controll, :area
    pose :skill
  end

  skill 'Второе дыхание' do
    message 'became stronger '
    description <<-TEXT
Позволяет увеличить запас здоровья и
востановить некоторую его часть
    TEXT
    tp_cost 20
    id 130
    icon_index 32
    scope me

    damage do
      type heal_hp
      formula "a.param_base(#{mhp}) * 0.25"
    end

    effect do
      effect add_buff
      value1 4
      data mhp
    end
    tags :protective, :self, :extrimal
    pose :skill
  end

  skill 'Забота' do
    message 'protects ally '
    description <<-TEXT
Переносит треть урона, направленного на союзников,
на применившего этот навык.
    TEXT
    tp_cost 0
    id 131
    scope all_allies
    speed 50
    icon_index 13

    animation :target, 121
    animation :caster, 122

    effect do
      effect add_state
      value1 1
      data RPG::State.watched.id
    end

    tags :protective, :allies, :long_time
    pose :skill
  end

  skill 'Блокирование' do
    id 132
    message 'raise shield '
    description '10% шанс заблокировать любой урон'
    icon_index 160
    tp_cost 0
    tp_gain 4
    scope me
    speed 100
    effect do
      effect add_state
      value1 1
      data RPG::State.shield_block.id
    end
    tags :protective, :self, :long_time
    pose :skill
  end
end
#dsls/enemies/bosses.rb
EnemyDsl.define do
  enemy 'Den Frosty' do
    id 1

    exp 500
    gold 500

    copy_skills $data_classes[16], 10
    copy_params $data_classes[16], 10

    lines_priority 3, 2, 1

    drop_item do
      kind stuff
      data 1
      denominator 1
    end

    expand_feature do
      code atk_element
      data ice
    end

    feature do
      code element_rate
      data ice
      value 2
    end

    feature do
      code element_rate
      data fire
      value 0
    end

    feature do
      code sparam
      data deal_mag_dmg
      value 1.5
    end

    feature do
      code param
      data mmp
      value 1.2
    end
  end
end
#dsls/enemies/monsters.rb
#encoding=utf-8

EnemyDsl.define do
  (1..10).to_a.each do |lvl|
    enemy "Бихолд" do
      id  2 + lvl
      sprite 'behold'
      battler_name 'gayzer'
      copy_params $data_classes[14], lvl
      copy_skills $data_classes[14], lvl

      exp 20 * lvl
      gold 0

      tags :martial, :solo_ongoiner, :range
      tag  :solo_burster if lvl >= 3

      lines_priority 2, 1, 3

      new_feature do
        code xparam
        data acu
        value 1
      end

      feature do
        code xparam
        data critical
        value 0.1
      end

      feature do
        code param
        data agi
        value 1.1
      end

      feature do
        code param
        data atk
        value 1.1
      end
    end

    enemy "Лич" do
      id 12 + lvl
      sprite 'lich'
      battler_name 'Wizard_f'

      copy_skills $data_classes[16], lvl
      copy_params $data_classes[16], lvl

      exp 20 * lvl
      gold 0

      lines_priority 3, 2, 1

      new_feature do
        code param
        data mmp
        value 1.2
      end

      feature do
        code xparam
        data acu
        value 0.8
      end

      feature do
        code sparam
        data deal_mag_dmg
        value 1.2
      end

      tags :magical, :range, :area_ongoiner
      tag :area_burster if lvl >= 3
      tag :area_controller if lvl >= 2
    end


    enemy "Летучая мышь" do
      id 22 + lvl
      sprite 'bat'
      battler_name 'Cleric_f'

      copy_skills $data_classes[15], lvl
      copy_params $data_classes[15], lvl

      lines_priority 3, 2, 1

      new_feature do
        code xparam
        data acu
        value 0.8
      end

      exp 30 * lvl
      gold 0

      tags :magical, :range, :healer
    end


    enemy "Скорпион" do
      id 32 + lvl
      sprite 'sand_king'
      battler_name 'Paladin_m'
      copy_params $data_classes[12], lvl
      copy_skills $data_classes[12], lvl
      exp 30 * lvl
      gold 0
      tags :martial, :tank, :melee
      lines_priority 1, 2, 3

      new_feature do
        code element_rate
        data martial
        value 0.9
      end

      feature do
        code xparam
        data acu
        value 0.95
      end

      [drain, fire, ice, lighting, water, earth, air, holly, shadow].each do |elem|
        feature do
          code element_rate
          data elem
          value 0.9
        end
      end

      feature do
        code param
        data mhp
        value 1.1
      end

      feature do
        code param
        data dff
        value 1.1
      end

      feature do
        code param
        data mdf
        value 1.1
      end

      feature do
        code param
        data atk
        value 0.8
      end

      feature do
        code xparam
        data eva
        value 0.05
      end
    end

    enemy "Призрак" do
      id 42 + lvl
      sprite 'ghost'
      battler_name 'Thief_m'
      copy_params $data_classes[13], lvl
      copy_skills $data_classes[13], lvl
      exp 30 * lvl
      gold 0
      tags :martial, :rogue, :melee, :stealther, :rogue_ongoiner
      tag :rogue_burster if lvl >= 3
      lines_priority 1, 2, 3
      energy_user true

      new_feature do
        code param
        data atk
        value 1.1
      end

      feature do
        code xparam
        data acu
        value 0.95
      end

      feature do
        code param
        data luk
        value 1.1
      end

      feature do
        code xparam
        data critical
        value 0.3
      end
    end

    enemy "Синий слизень" do 
      id 52 + lvl 
      sprite 'blue_slime'
      battler_name 'Slime'
      copy_params $data_classes[17], lvl
      copy_skills $data_classes[17], lvl

      exp 10 * lvl
      gold 0

      tags :martial, :minion, :melee, :solo_ongoiner

      lines_priority 1, 2, 3

      new_feature do
        code xparam
        data acu
        value 0.95
      end
    end

    enemy "Красный слизень" do 
      id 62 + lvl 
      sprite 'red_slime'
      battler_name 'Slime'
      copy_params $data_classes[18], lvl
      copy_skills $data_classes[18], lvl

      exp 10 * lvl
      gold 0

      tags :martial, :minion, :range, :solo_ongoiner

      lines_priority 2, 1, 3

      new_feature do
        code xparam
        data acu
        value 0.95
      end
    end
  end
end
