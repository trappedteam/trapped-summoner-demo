=begin
==============================================================
Title         : CC_PMS_VXA_v4.1
Author        : CC (http://yellow-mantaray.blogspot.jp/)
Last Modified : 2014/07/30

--------------------------------------------------------------
 Features
--------------------------------------------------------------
Parallax Mapping System(PMS). 
You can show multiple layers on the map screen.
As many layers as you want.
Each layer can have a different scroll speed.
Flip animation.

--------------------------------------------------------------
 Terms of Use (2014/07/28)
--------------------------------------------------------------
Creative Commons 4.0
 This work is licensed under the Creative Commons Attribution 4.0 International License.
 To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

 In Short:
  As long as you give credits, you are free to use this script in Commercial/Non-Commercial Projects.

We appreciate your donation.
 If you find this script useful, please consider to make a donation.
 Pledgie URL: https://pledgie.com/campaigns/26257

--------------------------------------------------------------
 How to use
--------------------------------------------------------------

[1] Settings
  Edit CC::PMS.
  CC::PMS::LAYERS decide how many layer it use and how does it appears.
  LAYERS[6] is Array for the animation. Set [] if you don't need the animation.
  
[2] Prepare Images
  In default, file directory is ./Graphics/ParallaxMaps/
  
  Filename format is;
  
  (1)without animation
  map[@map_id]_[name][type]
    ex1. 'map1_gnd0'
  
  (2)with animation
  If LAYERS[6] is Array of Numeric, format will be;
  map[@map_id]_[name][type]_[anim_position]
    ex2. [1,2,3,2] and "map1_water0_1" -> "map1_water0_2" -> "map1_water0_3" -> "map1_water0_2"
  
  The script loads images automatically.
  and if required image is not found, nothing happens.
  
[3] Cache
  This script uses cache for speed. It can be heavy load for memory.
  Call "Cache.clear_parallax_map()" to clear cache manually.
  
[4] Remarks
  This script check files only when you enter the map and not again.
  So, even if you change some variables in the map, PMS don't react and change images.

--------------------------------------------------------------
 References
--------------------------------------------------------------
Yami, "Overlay Mapping", https://yamiworld.wordpress.com/
DasMoony, "DMO - LIQUID & PARALLAX SCRIPT", http://www.rpgmakervxace.net/topic/676-dmo-liquid-parallax-script/

==============================================================
=end

$cc_imported ||= {}
$cc_imported["CC_PMS"] = true

#==============================================================================
## CC::PMSモジュール

module CC
module PMS
	DIR = "Graphics/ParallaxMaps/"
	
	# off_sw_id (osi)      : When Switch[osi] is true, the layer is invisible.
	# changer_var_id (cvi) : When Variables[cvi] > 0, the file 'map(mapid)_(name)(value of Variables[cvi])' is called.
	#                        Othewise, 0 is used as last character of filename.
	# blend_type : 0 = normal, 1 = add, 2 = sub
	
	LAYERS = [ # name, z, opacity, blend_type, off_sw_id, changer_var_id, animate_arr, scroll_ratio
		["gnd", 1, 255, 0, 22, 22, [], nil, ],
		["high", 250, 255, 0, 23, 23, [], nil, ],
		["shd", 251, 85, 2, 25, 25, [], nil, ],
		["anim", 1, 255, 0, 26, 26, [0,1,2,1], nil,  ],
		["rati", 250, 255, 0, 27, 27, [], [0.5, 1], ],
		#["water", 50, 255, 0, 24, 24, [0,1,2,1], nil, ],
	] #
	OFF_SW		= 21		# If $game_variables[OFF_SW] is true, entire PMS will stop.
	
	# Interval to change images.
	ANIM_INTERVAL = 60
	
end # of PMS
end # of CC


#==============================================================
## キャッシュ

module Cache
	def self.parallax_map(filename)
		self.load_bitmap(CC::PMS::DIR, filename)
	end
	
	def self.clear_parallax_map
		# パララックスはメモリを食うので、手動解放を用意しておく
		flag = false
		@cache.each{|key, value|
			if key.include?(CC::PMS::DIR)
				@cache.delete(key)
				flag = true
			end
		}
		GC.start if flag
	end
end

#==============================================================================
## 組み込み

class Spriteset_Map
  alias _pmscc_create_parallax create_parallax
  def create_parallax
    _pmscc_create_parallax
    create_pms
  end
  
  alias _pmscc_dispose_parallax dispose_parallax
  def dispose_parallax
  	dispose_pms
    _pmscc_dispose_parallax
  end
  
  alias _pmscc_update_parallax update_parallax
  def update_parallax
  	_pmscc_update_parallax
  	update_pms
  end
  
  def create_pms
    @cc_pms = []
    @cc_pms_mapid = $game_map.map_id
    CC::PMS::LAYERS.each {|params| @cc_pms.push( CC_pms.new(params, @viewport1) ) }
  end
  
  def dispose_pms
		@cc_pms.each {|layer| layer.dispose }
  end
  
  def update_pms
  	if @cc_pms_mapid != $game_map.map_id
	    @cc_pms_mapid = $game_map.map_id
  		refresh_pms
  	end
  	@cc_pms.each {|layer| layer.update }
  end
  
  def refresh_pms
  	dispose_pms
  	create_pms
  end
end # Spriteset_Map


#==============================================================================
## CC_pms クラス

class CC_pms
	attr_reader :name
	def initialize(params, viewport)
		@name = params[0]
		
		@sprite = Sprite.new
		@sprite.viewport = viewport
		@sprite.z = params[1]
		@sprite.opacity = params[2]
		@sprite.blend_type = params[3]
		
		@off_switch_id = params[4]
		changer_var_id = params[5]
		animation_array = params[6]
		
		@scroll_ratio_x = 1
		@scroll_ratio_y = 1
		if params[7] != nil
			@scroll_ratio_x = params[7][0]
			@scroll_ratio_y = params[7][1]
		end
		
		# アニメ処理
		@filename_itr = 0 # 何番目のファイルネームを使うか
		@frame_count = CC::PMS::ANIM_INTERVAL # ファイルネーム交換のフレーム周期
		
		# ファイル名の配列を作る
		filename = 'map' << $game_map.map_id.to_s << '_' << @name
		filename << ( ( changer_var_id > 0 )?( $game_variables[changer_var_id].to_s ):( '0' ) )
		@filename_arr = []
		if animation_array == []
			@filename_arr.push(filename)
		else
			animation_array.each {|num| @filename_arr.push( filename + '_' + num.to_s )}
		end
		
		reset_image
		update
	end
	
	#-------------------------------------
	def dispose
		@sprite.dispose
	end
	
	#-------------------------------------
	def reset_image
		# 画像のロードを試みてロード失敗したら空にする
		begin
			@sprite.bitmap = Cache.parallax_map( @filename_arr[@filename_itr] )
		rescue
			@sprite.bitmap = nil
			return
		end
		#p @filename_arr[@filename_itr]
	end # of reset_image
	
	#-------------------------------------
  def update
  	# マップ変更のチェック
  	
  	# 画像ないならリターン
  	return if @sprite.bitmap.nil?
  	
  	# オフスイッチが入ってるならvisibleをオフにしてリターン
  	if $game_switches[@off_switch_id] || $game_switches[CC::PMS::OFF_SW]
  		@sprite.visible = false
  		return
  	else
  		@sprite.visible = true
  	end
  	
  	# アニメーションのカウントを進める
  	if @filename_arr.size > 1
			@frame_count -= 1
			if @frame_count < 1
				@frame_count = CC::PMS::ANIM_INTERVAL
				@filename_itr += 1
				@filename_itr = 0 if @filename_itr >= @filename_arr.size
				reset_image
			end
  	end
  	
		# 表示位置を更新
		@sprite.ox = $game_map.display_x * 32 * @scroll_ratio_x
		@sprite.oy = $game_map.display_y * 32 * @scroll_ratio_y
  end # of update
end

